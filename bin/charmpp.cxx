#include "TROOT.h"
#include "TString.h"
#include "WDTruthLoop.h"
#include "WFakeRateMeasurement.h"
#include "WMETTemplate.h"
#include "WplusDLoop.h"
#include "WPTTemplate.h"
#include "ZCharmLoop.h"

int main(int argc, char **argv)
{
    // file name
    TString file_name = argv[1];
    std::cout << "input file " << file_name << std::endl;

    // out path
    TString out_path = argv[2];

    // analysis type
    TString analysis_type = argv[3];

    // config name
    std::string sys_config_name = "default";
    if (argc > 4)
    {
        sys_config_name = argv[4];
    }

    // analysis config name
    std::string config_name = "default";
    if (argc > 5)
    {
        config_name = argv[5];
    }

    long long start_event = 0;
    long long end_event = -1;
    if (argc > 6)
    {
        start_event = atoll(argv[6]);
        end_event = atoll(argv[7]);
        std::cout << "start_event " << start_event << " end_event " << end_event << std::endl;
    }

    // event loop
    Charm::EventLoopBase *el = nullptr;

    // implicit MT
    // ROOT::EnableImplicitMT(2);

    if (analysis_type == "ZCharm")
    {
        std::cout << "running the ZCharm analysis" << std::endl;
        el = new Charm::ZCharmLoop(file_name, out_path);
    }
    else if (analysis_type.Contains("WDCharm"))
    {
        std::cout << "running the W+D Charm analysis" << std::endl;
        el = new Charm::WplusDLoop(file_name, out_path);
    }
    else if (analysis_type == "WMETTemplate")
    {
        std::cout << "running the W MET Template analysis" << std::endl;
        el = new Charm::WMETTemplate(file_name, out_path);
    }
    else if (analysis_type == "WPTTemplate")
    {
        std::cout << "running the W PT Template analysis" << std::endl;
        el = new Charm::WPTTemplate(file_name, out_path);
    }
    else if (analysis_type == "WFakeRate")
    {
        std::cout << "running the W Fake Rate Measurement" << std::endl;
        el = new Charm::WFakeRateMeasurement(file_name, out_path);
    }
    else if (analysis_type == "WDTruthLoop")
    {
        std::cout << "running the W+D truth analysis" << std::endl;
        el = new Charm::WDTruthLoop(file_name, out_path, "TruthTree");
    }
    else
    {
        std::cout << "invalid analysis type specified... exiting." << std::endl;
    }

    el->set_config(config_name);
    el->set_sys_config(sys_config_name);
    el->run(start_event, end_event);
}
