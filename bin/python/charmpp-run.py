#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)

import sys, os
import subprocess

import utils

def launch_wrapper(args):
    FNULL = open(os.devnull, 'w')
    p = subprocess.Popen(["charmpp"] + args,
        stdout=FNULL, stderr=subprocess.STDOUT)
    p.communicate()
    return

def launch_wrapper_verbose(args):
    p = subprocess.Popen(["charmpp"] + args,
        stderr=subprocess.STDOUT)
    p.communicate()
    return

def run(options, args):

    threads = int(options.threads)
    analysis_type = options.analysis_type
    verbose = options.verbose

    jobs_file = os.path.join(os.environ['CHARMPP_NTUPLE_PATH'], 'sum_of_weights.txt')
    if not os.path.isfile(jobs_file):
        print ("no sum_of_weights.txt file found in %s" % os.environ['CHARMPP_NTUPLE_PATH'])
        print ("call charmpp-prepare first")
        sys.exit(1000)

    print ("Identifiers: ", args)

    concurrnet_jobs = utils.get_jobs(jobs_file, options, args)

    print ("number of jobs: ", len(concurrnet_jobs))

    ## check if tqdm is available
    import imp
    try:
        imp.find_module('tqdm')
        tqdm_found = True
    except ImportError:
        print ("tqdm module not found so there will be no progress bar. Install tqdm to get a progress bar with:\npip install tqdm --user")
        tqdm_found = False

    wrapper = launch_wrapper_verbose if verbose else launch_wrapper

    from multiprocessing import Pool
    p = Pool(threads)
    if tqdm_found:
        import tqdm
        for _ in tqdm.tqdm(p.imap_unordered(wrapper, concurrnet_jobs), total=len(concurrnet_jobs)):
            pass
    else:
        for i, _ in enumerate(p.imap_unordered(wrapper, concurrnet_jobs)):
            print ("done processing job %s/%s" % (i+1, len(concurrnet_jobs)))

if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser(usage=globals()['__doc__'])

    # ----------------------------------------------------
    # required
    # ----------------------------------------------------
    parser.add_option('-t', '--threads',
                      action="store", dest="threads",
                      help="number of threads to run on",
                      default=1)
    parser.add_option('-m', '--max-events',
                      action="store", dest="max_events",
                      help="maximum number of events per job",
                      default=-1)
    parser.add_option('-a', '--analysis-type',
                      action="store", dest="analysis_type",
                      help="analysis type (options: WCharm, ZCharm, WDCharm)")
    parser.add_option('-v', '--verbose',
                      action="store_true", dest="verbose",
                      help="print output of jobs")
    parser.add_option('-f', '--force',
                      action="store_true", dest="force",
                      help="override if existing output file",
                      default=False)

    # parse input arguments
    options, args = parser.parse_args()

    run(options, args)
