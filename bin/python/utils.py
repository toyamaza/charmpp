import os

split = {
    'Top': {
        'Top_ttbar': ['410470'],
        'Top_single_top': ['410644', '410645', '410646', '410647', '410658', '410659'],
        'Top_ttx': ['410155', '410156', '410157', '410218', '410219', '410220'],
    },
    'Top_sys': {
        'Top_ttbar_HS': ['410464', '410465'],
        'Top_ttbar_ISR': ['410480', '410482'],
    },
    'Top_sys_Herwig': {
        'Top_ttbar_Shower': ['410557', '410558'],
    },
    'Powheg_Wjets': {
        'Ph_Wjets_emu': ['361100', '361101', '361103', '361104'],
        'Ph_Wjets_tau': ['361102', '361105'],
    },
    'Powheg_Zjets': {
        'Ph_Zjets_emu': ['361106', '361107'],
        'Ph_Zjets_tau': ['361108'],
    },
    'Sherpa_Zjets': {
        'Sh_Zjets_light_emu': ['364100', '364103', '364106', '364109', '364114', '364117', '364120', '364123'],
        'Sh_Zjets_cjets_emu': ['364101', '364104', '364107', '364110', '364115', '364118', '364121', '364124'],
        'Sh_Zjets_bjets_emu': ['364102', '364105', '364108', '364111', '364116', '364119', '364122', '364125'],
        'Sh_Zjets_highpt_emu': ['364112', '364113', '364126', '364127'],
        'Sh_Zjets_lowmass_emu': ['364198', '364199', '364200', '364201', '364202', '364203', '364204', '364205', '364206', '364207', '364208', '364209'],
        'Sh_Zjets_tau': ['364128', '364129', '364130', '364131', '364132', '364133', '364134', '364135', '364136', '364137', '364138', '364139', '364140', '364141', '364210', '364211', '364212', '364213', '364214', '364215'],
    },
    'Sherpa_Wjets': {
        'Sh_Wjets_light_emu': ['364156', '364159', '364162', '364165', '364170', '364173', '364176', '364179'],
        'Sh_Wjets_cjets_emu': ['364157', '364160', '364163', '364166', '364171', '364174', '364177', '364180'],
        'Sh_Wjets_bjets_emu': ['364158', '364161', '364164', '364167', '364172', '364175', '364178', '364181'],
        'Sh_Wjets_highpt_emu': ['364168', '364169', '364182', '364183'],
        'Sh_Wjets_tau': ['364184', '364187', '364190', '364193', '364185', '364188', '364191', '364194', '364186', '364189', '364192', '364195', '364196', '364197'],
    },
    'MGPy8EG_Zjets': {
        'MG_Zjets_light_emu': ['363123', '363126', '363129', '363132', '363135', '363138', '363141', '363144', '363147', '363150', '363153', '363156', '363159', '363162', '363165', '363168'],
        'MG_Zjets_cjets_emu': ['363124', '363127', '363130', '363133', '363136', '363139', '363142', '363145', '363148', '363151', '363154', '363157', '363160', '363163', '363166', '363169'],
        'MG_Zjets_bjets_emu': ['363125', '363128', '363131', '363134', '363137', '363140', '363143', '363146', '363149', '363152', '363155', '363158', '363161', '363164', '363167', '363170'],
        'MG_Zjets_tau': ['361510', '361511', '361512', '361513', '361514'],
    },
    'MGPy8EG_Wjets': {
        'MG_Wjets_light_emu': ['363600', '363603', '363606', '363609', '363612', '363615', '363618', '363621', '363624', '363627', '363630', '363633', '363636', '363639', '363642', '363645'],
        'MG_Wjets_cjets_emu': ['363601', '363604', '363607', '363610', '363613', '363616', '363619', '363622', '363625', '363628', '363631', '363634', '363637', '363640', '363643', '363646'],
        'MG_Wjets_bjets_emu': ['363602', '363605', '363608', '363611', '363614', '363617', '363620', '363623', '363626', '363629', '363632', '363635', '363638', '363641', '363644', '363647'],
        'MG_Wjets_tau': ['363648', '363651', '363654', '363657', '363660', '363663', '363666', '363669', '363649', '363652', '363655', '363658', '363661', '363664', '363667', '363670', '363650', '363653', '363656', '363659', '363662', '363665', '363668', '363671'],
    },
    'MGPy8EG_FxFx_Wjets': {
        'MG_FxFx_Wjets_light_emu': ['508981', '508984'],
        'MG_FxFx_Wjets_cjets_emu': ['508980', '508983'],
        'MG_FxFx_Wjets_bjets_emu': ['508979', '508982'],
    },
    'Sherpa2211_Wjets': {
        'Sh_2211_Wjets_light_emu': ['700340', '700343'],
        'Sh_2211_Wjets_cjets_emu': ['700339', '700342'],
        'Sh_2211_Wjets_bjets_emu': ['700338', '700341'],
        'Sh_2211_Wjets_tau': ['700344','700345','700346','700347','700348','700349'],
    },
    'Sherpa2211_Zjets': {
        'Sh_2211_Zjets_light_emu': ['700322', '700325'],
        'Sh_2211_Zjets_cjets_emu': ['700321', '700324'],
        'Sh_2211_Zjets_bjets_emu': ['700320', '700323'],
        'Sh_2211_Zjets_tau': ['700326','700327','700328','700329','700330','700331','700332','700333','700334','700335','700336','700337'],
    },
    'Sherpa2211_WplusD': {
        'Sh_2211_WplusD': ['700366', '700367'],
    },
    'MGPy8EG_NLO_WplusD': {
        'MG_NLO_WplusD': ['507704', '507705'],
    },
    'MGPy8EG_FxFx_WplusD': {
        'MG_FxFx_WplusD': ['501716', '501717'],
    },
    'SPG': {
        'SPG_Dminus': ['999914'],
        'SPG_Dsminus': ['999912'],
        'SPG_Dstar_plus': ['999928'],
        'SPG_Dstar_minus': ['999918'],
        'SPG_D0': ['999921'],
        'SPG_D0bar': ['999911'],
        'SPG_Baryonbar': ['999913', '999915', '999916', '999917'],
        'SPG_Dplus': ['999924'],
        'SPG_Ds': ['999922'],
        'SPG_Baryon': ['999923', '999925', '999926', '999927'],
    },
}

def get_jobs(jobs_file, options, args):
    concurrnet_jobs = {}

    output_folder = f"output_{options.sys_config_name}"

    with open(jobs_file, 'r') as f:

        for l in f:
            line = l.split()

            input_file = os.path.join(os.environ['CHARMPP_NTUPLE_PATH'], line[0])

            process = False
            if len(args) > 0:
                for arg in args:
                    if arg in input_file:
                        process = True
            else:
                process = True

            if not process:
                continue

            concurrnet_jobs[line[0]] = []

            if len(line) > 3:
                # new ntuples have two trees with different number of events
                if options.truth_tree:
                    n_events = int(line[-1])
                else:
                    n_events = int(line[-2])
            else:
                # old (or non-signal) ntuples have only one tree
                n_events = int(line[-1])

            if int(options.max_events) > 0 and n_events > int(options.max_events):
                for start in range(0, n_events+1, int(options.max_events)):

                    out_name = line[0].replace(".root", "") + ".%s.%s.%s.root" % (options.sys_config_name, start, start+int(options.max_events))
                    out_name = os.path.join(output_folder, out_name)
                    print (out_name)
                    if os.path.isfile(out_name) and not options.force:
                        print ("file %s already exists" % (out_name))
                    else:
                        concurrnet_jobs[line[0]] += [[
                            input_file, ".", options.analysis_type, options.sys_config_name, options.config_name,
                            str(start), str(start+int(options.max_events))
                        ]]
            else:
                if os.path.isfile(os.path.join(output_folder, line[0].replace(".root", "") + ".%s.root" % (options.sys_config_name))) and not options.force:
                    print ("file %s already exists" % (line[0]))
                else:
                    concurrnet_jobs[line[0]] += [[input_file, ".", options.analysis_type, options.sys_config_name, options.config_name]]

    tasks = []
    at_least_one_key = True
    while at_least_one_key:
        at_least_one_key = False
        for key in concurrnet_jobs:
            if not concurrnet_jobs[key]:
                continue
            tasks += [concurrnet_jobs[key].pop()]
            at_least_one_key = True

        if not at_least_one_key:
            break

    return tasks
