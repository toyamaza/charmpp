#!/usr/bin/env python

import sys, os
import utils

CHARMPP_BUILD_PATH = os.environ['CHARMPP_BUILD_PATH']
CHARMPP_NTUPLE_PATH = os.environ['CHARMPP_NTUPLE_PATH']
CHARMPP_RUN_PATH = os.environ['CHARMPP_RUN_PATH']

def run(options, args):

    # input files
    jobs_file = os.path.join(CHARMPP_NTUPLE_PATH, 'sum_of_weights.txt')
    if not os.path.isfile(jobs_file):
        print ("no sum_of_weights.txt file found in %s" % CHARMPP_NTUPLE_PATH)
        print ("call charmpp-prepare first")
        sys.exit(1000)

    # define input tasks
    tasks = utils.get_jobs(jobs_file, options, args)
    print ("number of jobs: ", len(tasks))

    # save tasks to file and exit
    with open('task_list.txt', 'a') as f:
        for task in tasks:
            f.write(f"charmpp {' '.join(task)}\n")

if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser(usage=globals()['__doc__'])

    # ----------------------------------------------------
    # arguments
    # ----------------------------------------------------
    parser.add_option('-a', '--analysis-type',
                      action="store", dest="analysis_type",
                      help="analysis type (options: WCharm, ZCharm, WDCharm)")
    parser.add_option('-m', '--max-events',
                      action="store", dest="max_events",
                      help="maximum number of events per job",
                      default=-1)
    parser.add_option('-c', '--config-name',
                      action="store", dest="config_name",
                      help="name of the config file",
                      default="default")
    parser.add_option('-s', '--sys-config-name',
                      action="store", dest="sys_config_name",
                      help="name of the systematics config file",
                      default="default")
    parser.add_option('--truth-tree',
                      action="store_true", dest="truth_tree",
                      help="running on the TruthTree instead of the CharmAnalysis tree")

    # parse input arguments
    options, args = parser.parse_args()

    run(options, args)
