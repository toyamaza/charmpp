#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)

import os, sys
import subprocess

path = os.environ['CHARMPP_NTUPLE_PATH']

merge_path = os.path.join(os.environ['CHARMPP_NTUPLE_PATH'], "merged")
if not os.path.exists(merge_path):
    os.makedirs(merge_path)

datasets = {}

def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def wrapper(args):
    # launch hadd
    p = subprocess.Popen(["hadd"] + [args[0]] + args[1],
        stderr=subprocess.STDOUT)
    p.communicate()
    return

# r=root, d=directories, f=files
for r, d, f in os.walk(path):
    if "merged" in r:
        continue
    for file in f:
        if '.root' in file:

            sample = os.path.basename(r).split(".")[2]
            sample += "." + os.path.basename(r).split(".")[3]

            # process only specified samples
            process = False
            if len(sys.argv) > 1:
                for arg in sys.argv[1:]:
                    if arg in r or arg in file:
                        process = True
            else:
                process = True
            if not process:
                continue

            if not sample in datasets.keys():
                datasets[sample] = []
            

            datasets[sample] += [os.path.join(r, file)]

subdatasets = {}

for sample in datasets:
    subdatasets[sample] = {}
    for f in datasets[sample]:
        dsid = os.path.basename(f).split(".")[2]
        short = os.path.basename(f).split(".")[3]

        if not dsid in subdatasets[sample].keys():
            subdatasets[sample][dsid] = []

        subdatasets[sample][dsid] += [f]

reduced_samples = {}

# reduce size
for sample in subdatasets:
    sample_reduced = {}
    for subdataset in subdatasets[sample]:
        subdataset_size = 0
        reduced_subdataset = []
        for f in subdatasets[sample][subdataset]:
            b = os.path.getsize(f)
            subdataset_size += b
            if subdataset_size > 50000000000.:
                reduced_samples[sample] = {}
                if subdataset in sample_reduced:
                    sample_reduced[subdataset] += [reduced_subdataset]
                else:
                    sample_reduced[subdataset] = [reduced_subdataset]
                reduced_subdataset = [f]
                subdataset_size = 0
            else:
                reduced_subdataset += [f]
        if subdataset in sample_reduced:
            sample_reduced[subdataset] += [reduced_subdataset]

    for s in sample_reduced:
        # no dupes
        reduced = sample_reduced[s]
        l = sum(reduced, [])
        assert set([x for x in l if l.count(x) > 1])==set([])
        reduced_samples[sample][s] = reduced

# replace
for sample in reduced_samples:
    for subdataset in reduced_samples[sample]:
        del subdatasets[sample][subdataset]
        sample_reduced = reduced_samples[sample][subdataset]
        for i, x in enumerate(sample_reduced):
            subdatasets[sample][subdataset + ".%s" % i] = x

jobs = []

for sample in subdatasets:
    for subdataset in subdatasets[sample]:
        merge_file = os.path.join(merge_path, sample + "." + subdataset + ".root")
        jobs += [[merge_file, subdatasets[sample][subdataset]]]

## check if tqdm is available
import imp
try:
    imp.find_module('tqdm')
    tqdm_found = True
except ImportError:
    print ("tqdm module not found so there will be no progress bar. Install tqdm to get a progress bar with:\npip install tqdm --user")
    tqdm_found = False

from multiprocessing import Pool
p = Pool(6)
if tqdm_found:
    import tqdm
    for _ in tqdm.tqdm(p.imap_unordered(wrapper, jobs), total=len(jobs)):
        pass
else:
    for i, _ in enumerate(p.imap_unordered(wrapper, jobs)):
        print ("done processing job %s/%s" % (i+1, len(jobs)))
