import ROOT

import re
import os, sys
import subprocess

def cutflow(options):
     
    # Put together all cutflow histograms
    f = ROOT.TFile(str(options.input), "READ")
    c = ROOT.TCanvas("c", "c", 1200, 1200)

    list_of_algs = ['GRLSelectorAlg','PrimaryVertexSelectorAlg','EventFlagSelectorAlg', 'TrigEventSelectorAlg', 'BadMuonVetoAlg', 'EventPreselectionAlg'] 

    hout_size = 0
    # Set the size of the cutflow histogram
    for key in f.GetListOfKeys():
        name = key.GetName()
        if (options.full_cutflow):
            if re.search('.+._.+', name):
                hout_size += 1
        else:
            for algs in list_of_algs:
                if re.search(algs + '_.+', name):
                    hout_size += 1

    hout = ROOT.TH1I('cutflow', 'Histogram of Cutflow Algorithms', hout_size, 0, hout_size - 1)

    # hout bin iterator    
    i = 1
 
    '''
    Go through root file and get individual cutflow histograms
    Save them into one final output histogram
    '''
    for key in f.GetListOfKeys():
        name = key.GetName()
        if (options.full_cutflow):
            if re.search('.+_.+', name):
                h = f.Get(name)
                hout.AddBinContent(i, h.GetBinContent(1))
                hout.GetXaxis().SetBinLabel(i, name)
                i += 1
        else:
            for algs in list_of_algs:
                if re.search(algs + '_.+', name):
                    h = f.Get(name)
                    hout.AddBinContent(i, h.GetBinContent(1))
                    hout.GetXaxis().SetBinLabel(i, name)
                    i += 1

    hout.LabelsOption("v", "X")
    fout = ROOT.TFile("cutflow.root", "UPDATE")
    hout.Write()
    fout.Close()

def run(options, args):
    # outdir
    out_path = os.getcwd()
    cutflow(options)


if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser()

    # ----------------------------------------------------
    # required
    # ----------------------------------------------------

    parser.add_option('-i', '--input',
                      action="store", dest="input",
                      help="path to the input file")
    parser.add_option('-o', '--output',
                      action="store", dest="output_dir",
                      help="output directory")
    parser.add_option('-f', '--fullcutflow',
                      action="store_true", dest="full_cutflow",
                      help="run full cutflow histogramming",
                      default=False)

    # parse input arguments
    options, args = parser.parse_args()

    f = ROOT.TFile("cutflow.root", "RECREATE")
    f.Close()

    run(options, args)
