#!/bin/bash

export CHARMPP_BUILD_PATH=/global/homes/${USER:0:1}/$USER/build/charmpp

export CHARMPP_RUN_PATH=/global/cscratch1/sd/$USER/run/charmpp

export CHARMPP_NTUPLE_PATH=/global/cfs/cdirs/atlas/wcharm/v9/merged

export CHARMPP_DATA_PATH=/global/cfs/cdirs/atlas/wcharm/charmpp_data

export CHARMPP_SOURCE_PATH=`pwd`

mkdir -p $CHARMPP_BUILD_PATH
mkdir -p $CHARMPP_RUN_PATH

export PATH=${CHARMPP_BUILD_PATH}/bin:$PATH
