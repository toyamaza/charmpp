# champp

Histograming code for LBNL Charm project

## Contact Details

| Contact Person | Contact E-mail        |
|----------------|-----------------------|
| Miha Muskinja  | miha.muskinja@cern.ch |

## Setup

Clone the charmpp project:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/berkeleylab/CharmPhysics/charmpp.git
```

Copy the `example.sh` file and specify the following paths as you prefer:

```
export CHARMPP_BUILD_PATH=<SOME_PATH>

export CHARMPP_RUN_PATH=<SOME_OTHER_PATH>

export CHARMPP_NTUPLE_PATH=<PATH_TO_NTUPLES>

export CHARMPP_DATA_PATH=/global/cfs/cdirs/atlas/wcharm/charmpp_data
```

`CHARMPP_BUILD_PATH` is the directory where the charmpp project will be built, for example a path under `$HOME`. `CHARMPP_RUN_PATH` is the path where the output histograms from the charmpp execution will appear. The recommended path for `CHARMPP_RUN_PATH` is a user folder under `$CSCRATCH`.

Source the setup script from the root folder (i.e. where the charmpp project is cloned):

```
source your_script_name.sh
```

### Recommended ntuple paths

Standard MC16a/d/e MC samples:
```
export CHARMPP_NTUPLE_PATH=/global/cfs/cdirs/atlas/wcharm/v9/merged
```

Single Particle Gun samples:
```
CHARMPP_NTUPLE_PATH=/global/cfs/cdirs/atlas/wcharm/v9_spg/merged
```

## Environment Setup (recommended on Cori)

To build the project on Cori please follow the instructions below.

```
module load python3/3.9-anaconda-2021.11
module load root
pip install 'ray[default]'
```

This loads the proper version of python to work with root/pyroot and installs ray for distributed processing. To build the project, make a 'build' folder that is linked to the 'CHARMPP_BUILD_PATH' variable and perform the cmake build from that folder:

```
cd $CHARMPP_BUILD_PATH
cmake $CHARMPP_SOURCE_PATH
make -j8
```

## Running charmpp

If $CHARMPP_BUILD_PATH is set correctly, a binary called 'charmpp' should be available in your path. It can be run directly, e.g.:

```
charmpp $CHARMPP_NTUPLE_PATH/MGPy8EG_Wjets.MC16e.363601.root . WDCharm default default 0 100000
```

Or via the helper python script that will run over all input files in $CHARMPP_NTUPLE_PATH that match the identifier (`MGPy8EG_Wjets.MC16e.363601` in the example below):

```
charmpp-ray -a WDCharm -m 1000000 MGPy8EG_Wjets.MC16e.363601 -l -p 8
```

The `-l -p 8` flags instruct the script to run the jobs locally with 8 processes. Run `charmpp-histogram-merge` to merge the outputs afterwards.
