#include "WDTruthLoop.h"

namespace Charm
{
void WDTruthLoop::initialize()
{
    std::cout << "WDTruthLoop::initiaize" << std::endl;

    // configure
    configure();

    // systematics
    if (m_do_systematics)
    {
        // GEN
        m_sys_branches_GEN.init(get_systematics_from_group("systematics_GEN"));
    }
}

float WDTruthLoop::dR_min(std::vector<unsigned int> *selected, bool OS)
{
    float dR_min = 999;
    for (unsigned int i = 0; i < selected->size(); i++)
    {
        for (unsigned int j = i + 1; j < selected->size(); j++)
        {
            unsigned int index1 = selected->at(i);
            unsigned int index2 = selected->at(j);
            if (OS && (m_TruthParticles_Selected_pdgId->at(index1) * m_TruthParticles_Selected_pdgId->at(index2) > 0.0))
            {
                continue;
            }
            float deta = m_TruthParticles_Selected_eta->at(index1) - m_TruthParticles_Selected_eta->at(index2);
            float dphi = TVector2::Phi_mpi_pi(m_TruthParticles_Selected_phi->at(index1) - m_TruthParticles_Selected_phi->at(index2));
            float dR = TMath::Sqrt(deta * deta + dphi * dphi);
            if (dR < dR_min)
            {
                dR_min = dR;
            }
        }
    }
    return dR_min;
}

void WDTruthLoop::fill_Dplus_histograms(std::string channel, bool requireDecay)
{
    // number of D, dR
    std::vector<unsigned int> selected = {};

    // truth D selection
    for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size(); i++)
    {
        // require PDG ID
        if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 411)
        {
            continue;
        }
        // truth fiducial pT cut
        if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0)
        {
            continue;
        }
        // truth fiducial eta cut
        if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2)
        {
            continue;
        }
        // require the D+ -> K pi pi decay if required
        std::string mode = "";
        if (requireDecay)
        {
            mode = get_decay_mode(i);
            if (mode == "")
            {
                continue;
            }
        }
        // number of D, dR
        selected.push_back(i);

        // channel name
        std::string name = get_charge_prefix(m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
        if (channel != "")
        {
            name += "_" + channel;
        }
        if (requireDecay)
        {
            name += "_" + mode;
        }
        fill_D_histograms("Dplus" + name, i);
    }
    // overall plots
    std::string channel_name = !channel.empty() ? "_" + channel : "";
    auto channels = std::vector<std::string>(m_systematics.size() + 1, channel_name);
    if (!requireDecay)
    {
        add_fill_histogram_sys("N_Dplus", channels, selected.size(), 10, -0.5, 9.5, false);
        add_fill_histogram_sys("dR_min_Dplus", channels, dR_min(&selected), 240, 0.0, 6.0, false);
        add_fill_histogram_sys("dR_min_OS_Dplus", channels, dR_min(&selected, true), 240, 0.0, 6.0, false);
    }
}

void WDTruthLoop::fill_Dstar_histograms(std::string channel, bool requireDecay)
{
    // truth D selection
    for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size(); i++)
    {
        // require PDG ID
        if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 413)
        {
            continue;
        }
        // truth fiducial pT cut
        if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0)
        {
            continue;
        }
        // truth fiducial eta cut
        if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2)
        {
            continue;
        }
        // require the D+ -> K pi pi decay if required
        std::string mode = "";
        if (requireDecay)
        {
            mode = get_decay_mode(i);
            if (mode == "")
            {
                continue;
            }
        }
        // channel name
        std::string name = get_charge_prefix(m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
        if (channel != "")
        {
            name += "_" + channel;
        }
        if (requireDecay)
        {
            name += "_" + mode;
        }
        fill_D_histograms("Dstar" + name, i);
    }
}

void WDTruthLoop::fill_Dzero_histograms(std::string channel, bool requireDecay)
{
    // number of D, dR
    std::vector<unsigned int> selected = {};

    // truth D selection
    for (unsigned int i = 0; i < m_TruthParticles_Selected_decayMode->size(); i++)
    {
        // require PDG ID
        if (fabs(m_TruthParticles_Selected_pdgId->at(i)) != 421)
        {
            continue;
        }
        // truth fiducial pT cut
        if ((m_TruthParticles_Selected_pt->at(i) * Charm::GeV) < 8.0)
        {
            continue;
        }
        // truth fiducial eta cut
        if (fabs(m_TruthParticles_Selected_eta->at(i)) > 2.2)
        {
            continue;
        }
        // require the D0 -> K pi pi pi decay if required
        std::string mode = "";
        if (requireDecay)
        {
            mode = get_decay_mode(i);
            if (mode == "")
            {
                continue;
            }
        }
        // number of D, dR
        selected.push_back(i);

        // channel name
        std::string name = get_charge_prefix(m_truth_lep1_charge, m_TruthParticles_Selected_pdgId->at(i));
        if (channel != "")
        {
            name += "_" + channel;
        }
        if (requireDecay)
        {
            name += "_" + mode;
        }
        fill_D_histograms("Dzero" + name, i);
    }
    // overall plots
    std::string channel_name = !channel.empty() ? "_" + channel : "";
    auto channels = std::vector<std::string>(m_systematics.size() + 1, channel_name);
    if (!requireDecay)
    {
        add_fill_histogram_sys("N_Dzero", channels, selected.size(), 10, -0.5, 9.5, false);
        add_fill_histogram_sys("dR_min_Dzero", channels, dR_min(&selected), 240, 0.0, 6.0, false);
        add_fill_histogram_sys("dR_min_OS_Dzero", channels, dR_min(&selected, true), 240, 0.0, 6.0, false);
    }
}

bool WDTruthLoop::preselection()
{
    // Weighted by the MC16a/d/e lumi fraction
    multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_branches_GEN, true, true, (m_mc_shower == "sherpa"));

    // decipher wjets truth
    if (!get_truth_wjets())
    {
        return false;
    }

    // channel name
    auto channels = std::vector<std::string>(m_systematics.size() + 1, m_truth_channel);
    m_channel_sys = channels;

    return true;
}

int WDTruthLoop::execute()
{
    // 1. charged lepton pT > 30 GeV
    if (m_truth_lep1.Pt() < 30)
    {
        return 1;
    }

    // 2. charged lepton |eta| < 2.5
    if (fabs(m_truth_lep1.Eta()) > 2.5)
    {
        return 1;
    }

    // reweight
    if (m_do_prod_fraction_rw)
    {
        reweight_production_fractions(this);
    }
    if (m_do_wjets_rw)
    {
        reweight_wjets(this);
    }

    // inclusive histograms
    fill_histograms();

    // truth D+ selection
    fill_Dplus_histograms();
    fill_Dplus_histograms("", true);

    // truth D* selection
    fill_Dstar_histograms();
    fill_Dstar_histograms("", true);

    // truth D0 selection
    fill_Dzero_histograms();
    fill_Dzero_histograms("", true);

    // fiducial MET and mT cuts
    if (!m_do_fiducial_met)
    {
        return 1;
    }

    // 3. MET > 30 GeV
    if (m_truth_lep2.Pt() < 30)
    {
        return 1;
    }

    // 4. mT > 60 GeV
    if (m_truth_mt < 60)
    {
        return 1;
    }

    // fill fiducial histograms
    fill_histograms("fid");

    // truth D+ selection
    fill_Dplus_histograms("fid");
    fill_Dplus_histograms("fid", true);

    // truth D* selection
    fill_Dstar_histograms("fid");
    fill_Dstar_histograms("fid", true);


    // truth D0 selection
    fill_Dzero_histograms("fid");
    fill_Dzero_histograms("fid", true);

    return 1;
}

void WDTruthLoop::fill_D_histograms(std::string channel, unsigned int i)
{
    std::vector<float> diphotons = get_diphoton_masses(i);
    std::string channel_name = !channel.empty() ? "_" + channel : "";
    auto channels = std::vector<std::string>(m_systematics.size() + 1, channel_name);
    add_fill_histogram_sys("D_pt"             , channels, m_TruthParticles_Selected_pt->at(i) * Charm::GeV, 150, 0, 150, true);
    add_fill_histogram_sys("D_eta"            , channels, m_TruthParticles_Selected_eta->at(i), 100, -3.0, 3.0, false);
    // add_fill_histogram_sys("D_pt_eta"         , channels, m_TruthParticles_Selected_pt->at(i) * Charm::GeV, m_TruthParticles_Selected_eta->at(i), 200, 0, 200, 440, -2.2, 2.2, false);
    add_fill_histogram_sys("D_phi"            , channels, m_TruthParticles_Selected_phi->at(i), 100, -M_PI, M_PI, false);
    add_fill_histogram_sys("D_m"              , channels, m_TruthParticles_Selected_m->at(i) * Charm::GeV, 300, 0.0, 3.0, false);
    add_fill_histogram_sys("D_m_tracks"       , channels, truth_daughter_mass(i) * Charm::GeV, 300, 0.0, 3.0, false);
    add_fill_histogram_sys("D_m_pi0"          , channels, diphoton_resonance_mass(diphotons, PION0_MASS), 200, 0, 1000, false);
    add_fill_histogram_sys("D_differential_pt", channels, m_TruthParticles_Selected_pt->at(i) * Charm::GeV, m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(), true);
    add_fill_histogram_sys("D_differential_lep_eta", channels, std::abs(m_truth_lep1.Eta()), m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(), true);
    fill_histograms(channel);
}

void WDTruthLoop::fill_histograms(std::string channel)
{
    std::string channel_name = !channel.empty() ? "_" + channel : "";
    auto channels = std::vector<std::string>(m_systematics.size() + 1, channel_name);
    add_fill_histogram_sys("lep_pt"    , channels, m_truth_lep1.Pt(), 400, 0, 400, false);
    add_fill_histogram_sys("lep_eta"   , channels, m_truth_lep1.Eta(), 100, -3.0, 3.0, false);
    add_fill_histogram_sys("lep_phi"   , channels, m_truth_lep1.Phi(), 100, -M_PI, M_PI, false);
    add_fill_histogram_sys("lep_charge", channels, m_truth_lep1_charge, 2, -2, 2, false);
    add_fill_histogram_sys("v_pt"      , channels, m_truth_v.Pt(), 400, 0, 400, false);
    add_fill_histogram_sys("v_eta"     , channels, m_truth_v.Eta(), 100, -3.0, 3.0, false);
    add_fill_histogram_sys("v_phi"     , channels, m_truth_v.Phi(), 100, -M_PI, M_PI, false);
    add_fill_histogram_sys("v_m"       , channels, m_truth_v.M(), 400, 0, 400, false);
    add_fill_histogram_sys("met_met"   , channels, m_truth_lep2.Pt(), 400, 0, 400, false);
    add_fill_histogram_sys("met_mt"    , channels, m_truth_mt, 400, 0, 400, false);
}

void WDTruthLoop::connect_branches()
{
    connect_truth_D_meson_branches(this);

    // event info
    add_preselection_branch("EventInfo_generatorWeight_NOSYS", &m_EventInfo_generatorWeight_NOSYS);
    add_preselection_branch("EventInfo_PileupWeight_NOSYS", &m_EventInfo_PileupWeight_NOSYS);

    // sys branches
    if (m_do_systematics)
    {
        // event weights
        initialize_sys_branches(&m_sys_branches_GEN, "EventInfo_generatorWeight_%s");
    }
}

void WDTruthLoop::configure()
{
    // set configs
    std::cout << get_config() << std::endl;

    // systematics
    m_do_systematics = get_config()["do_systematics"].as<bool>(false);

    // fiducial MET cuts
    m_do_fiducial_met = get_config()["do_fiducial_met"].as<bool>(false);

    // charm production fraction reweight
    m_do_prod_fraction_rw = get_config()["do_prod_fraction_rw"].as<bool>(true);

    // reweight wjets samples
    m_do_wjets_rw = get_config()["do_wjets_rw"].as<bool>(true);
}

WDTruthLoop::WDTruthLoop(TString input_file, TString out_path, TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name),
      TruthInfo()
{
}

}  // namespace Charm
