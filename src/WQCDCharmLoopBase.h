#ifndef W_PLUS_CHARM_LOOP_H
#define W_PLUS_CHARM_LOOP_H

#include "DMesonRec.h"
#include "EventLoopBase.h"
#include "TruthInfo.h"

namespace Charm
{
class WQCDCharmLoopBase : public EventLoopBase, public DMesonRec, public TruthInfo
{
public:
    WQCDCharmLoopBase(TString input_file, TString out_path, TString tree_name = "CharmAnalysis");

    std::vector<Float_t> get_sys_lep_abs_eta() { return m_sys_lep_abs_eta; };

protected:
    virtual void connect_branches() override;

private:
    virtual void initialize() override;

    virtual int execute() override;

    virtual bool preselection() override;

private:
    std::set<std::string> m_created_channels;
    bool m_split_by_period{};
    bool m_split_by_charge{};
    bool m_truth_match{};
    bool m_do_pt_v_rw{};
    bool m_do_extra_histograms{};
    bool m_split_into_SB{};
    bool m_do_el_topocone_param{};
protected:
    bool m_anti_tight{};
    bool m_do_prod_fraction_rw{};
    bool m_do_wjets_rw{};
    bool m_fake_rate_measurement{};
    bool m_eval_fake_rate{};
    bool m_fake_factor_method{};
    bool m_rw_mm_closure{};
    bool m_fiducial_selection{};
    bool m_save_truth_wjets{};
    bool m_jet_selection{};
    bool m_track_jet_selection{};
    bool m_two_tag_region{};
    bool m_loose_w_selection{};
    bool m_loose_w_selection_only{};
    bool m_unfolding_variables{};
    const double m_eta_bins_el[6];
    const double m_eta_bins_mu[4];
    const double m_met_bins_lep[6];

protected:
    bool m_D_plus_meson{};
    bool m_D_star_meson{};
    bool m_D_star_pi0_meson{};
    bool m_D_s_meson{};
    bool m_truth_matchD{};
    bool m_truth_D{};
    bool m_do_single_entry_D{};
    bool m_do_single_entry_D_only{};
    bool m_do_lep_presel{};
    bool m_fill_wjets{};
    bool m_do_PV{};
    bool m_inclusive_fake_rate{};
    bool m_apply_lumi{};
    bool m_reweight_spg{};
    bool m_debug_dmeson_printout{};
    bool m_bin_in_truth_D_pt;
    bool m_bin_in_truth_lep_eta;
    bool m_signal_only;
    bool m_use_anti_tight_met;
    bool m_run_val_region; 
    bool m_less_closure_region;

protected:
    std::string get_period_string();

    void fill_lepton_histograms(std::string channel);

    void fill_lepton_histograms(std::vector<std::string> channels, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void fill_PV_histograms(std::string channel);

    void fill_PV_histograms(std::vector<std::string> channels, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void fill_D_plus_histograms(std::string channel);

    void fill_D_star_histograms(std::string channel);

    void fill_D_star_pi0_histograms(std::string channel);

    void fill_D_s_histograms(std::string channel);

    void fill_histograms(std::string channel = "");

    void fill_histograms_with_matrix_method();

    void jet_selection_for_sys();

    void matrix_method_for_sys(unsigned int i, bool anti_tight);

    float matrix_method_weight(unsigned int i, float pt, float eta, float met, float mt, int nbjet, bool anti_tight, std::string channel);

    float matrix_method_weight_inclusive(unsigned int i, float pt, float eta, float mt, bool anti_tight, std::string channel);

    float matrix_method_closure_reweight(unsigned int i, float met, int nbjet, std::string channel);

    float calculate_ptv_weight();

    void read_sys_branches();

    float get_dr_bjet_Dmeson(unsigned int jet, unsigned int i);

    void calculate_track_jet_properties(unsigned int DMeson_index,
                                        unsigned int sys_index,
                                        std::vector<int> &sys_track_jet_index,
                                        std::vector<Float_t> &sys_track_jet_dR,
                                        std::vector<Float_t> &sys_track_jet_pt,
                                        std::vector<Float_t> &sys_track_jet_zt,
                                        const std::vector<float> *jet_pt,
                                        const std::vector<float> *jet_eta,
                                        const std::vector<float> *jet_phi);

    float get_dr_lep_Dmeson(float eta, float phi);

    void get_jets();

    void debug_dmeson_printout(int index, bool printTruth = true);

    float m_dosig_cut;

    std::string m_channel_string;

    std::string m_jet_selection_string;

    // variables
    float m_met;
    float m_met_phi;
    float m_met_dphi;
    float m_met_mt;
    float m_d0;
    float m_d0sig;
    float m_lep_charge;
    float m_d0sig_signed;
    float m_z0sinTheta;
    float m_topoetcone20;
    float m_topoetcone20_over_pt;
    float m_ptvarcone20_TightTTVA_pt1000;
    float m_ptvarcone30_TightTTVA_pt1000;
    float m_ptvarcone20_TightTTVA_pt1000_over_pt;
    float m_ptvarcone30_TightTTVA_pt1000_over_pt;
    float m_ptvarcone30_TightTTVA_pt500;
    float m_neflowisol20;
    float m_newflowisol;
    float m_EOverP;
    float m_caloCluster_eta;
    bool m_lep_is_isolated;
    unsigned int m_nbjets;
    TLorentzVector m_lep;

    // objects
    std::vector<std::pair<unsigned int, float>> m_loose_electrons;
    std::vector<std::pair<unsigned int, float>> m_loose_muons;
    std::vector<std::pair<unsigned int, float>> m_electrons;
    std::vector<std::pair<unsigned int, float>> m_muons;
    std::vector<unsigned int> m_jets;

    // variables affected by calibration systematics
    std::vector<bool> m_sys_anti_tight;
    std::vector<Float_t> m_sys_met;
    //xyz MET tmp
    std::vector<Float_t> m_sys_met_AT;
    std::vector<Float_t> m_sys_met_perp;
    std::vector<Float_t> m_sys_met_para;
    std::vector<Float_t> m_sys_met_phi;
    std::vector<Float_t> m_sys_met_dphi;
    std::vector<Float_t> m_sys_met_mt;
    std::vector<Float_t> m_sys_lep_pt;
    std::vector<Float_t> m_sys_lep_pt_plusTopoConeET20;
    std::vector<Float_t> m_sys_lep_eta;
    std::vector<Float_t> m_sys_lep_abs_eta;
    std::vector<Float_t> m_sys_lep_calo_eta;
    std::vector<Float_t> m_sys_lep_phi;
    std::vector<Float_t> m_sys_lep_charge;
    std::vector<Float_t> m_sys_lep_dR_D;
    std::vector<TLorentzVector> m_sys_lep;
    std::vector<Float_t> m_sys_nbjets;

    std::vector<std::vector<unsigned int>> m_jets_sys;

private:
    float get_muon_trigger_weight(unsigned int sys_index, unsigned int i);
    void get_loose_electrons();
    void get_loose_muons();
    void get_tight_electrons();
    void get_tight_muons();
    bool pass_electron_trigger();
    bool pass_muon_trigger();
    bool electron_is_trigger_matched(unsigned int i, float el_pt);
    bool muon_is_trigger_matched(unsigned int i, float mu_pt);
    bool electron_is_prompt(unsigned int i);
    bool muon_is_prompt(unsigned int i);
    void configure();

private:
    bool m_have_one_D;

private:
    // runNumber of RandomRunNumber
    TString get_run_number_string();

private:
    // branches
    UInt_t m_EventInfo_runNumber{};
    Float_t m_EventInfo_correctedScaled_averageInteractionsPerCrossing{};
    Float_t m_CharmEventInfo_EventWeight{};
    Float_t m_EventInfo_generatorWeight_NOSYS{};
    Float_t m_EventInfo_PileupWeight_NOSYS{};
    Float_t m_EventInfo_jvt_effSF_NOSYS{};
    Float_t m_CharmEventInfo_TopWeight{};
    Float_t m_CharmEventInfo_PV_X{};
    Float_t m_CharmEventInfo_PV_Y{};
    Float_t m_CharmEventInfo_PV_Z{};
    Float_t m_CharmEventInfo_truthVertex_X{};
    Float_t m_CharmEventInfo_truthVertex_Y{};
    Float_t m_CharmEventInfo_truthVertex_Z{};

    bool m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH{};
    bool m_EventInfo_trigPassed_HLT_e60_lhmedium{};
    bool m_EventInfo_trigPassed_HLT_e120_lhloose{};
    bool m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose{};
    bool m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0{};
    bool m_EventInfo_trigPassed_HLT_e140_lhloose_nod0{};
    bool m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15{};
    bool m_EventInfo_trigPassed_HLT_mu26_ivarmedium{};
    bool m_EventInfo_trigPassed_HLT_mu50{};

    // muons
    std::vector<Float_t> *m_AnalysisMuons_pt_NOSYS{};
    std::vector<Float_t> *m_AnalysisMuons_eta{};
    std::vector<Float_t> *m_AnalysisMuons_phi{};
    std::vector<Float_t> *m_AnalysisMuons_charge{};
    std::vector<Float_t> *m_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS{};
    std::vector<Float_t> *m_AnalysisMuons_muon_effSF_TTVA_NOSYS{};
    std::vector<Float_t> *m_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS{};
    std::vector<Float_t> *m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS{};
    std::vector<Float_t> *m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS{};
    std::vector<Float_t> *m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS{};
    std::vector<Float_t> *m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS{};
    std::vector<Int_t> *m_AnalysisMuons_truthType{};
    std::vector<Int_t> *m_AnalysisMuons_truthOrigin{};
    std::vector<bool> *m_AnalysisMuons_is_bad{};
    std::vector<bool> *m_AnalysisMuons_mu_selected_NOSYS{};
    std::vector<bool> *m_AnalysisMuons_isQuality_Tight_NOSYS{};
    std::vector<bool> *m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS{};
    std::vector<bool> *m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15{};
    std::vector<bool> *m_AnalysisMuons_matched_HLT_mu26_ivarmedium{};
    std::vector<bool> *m_AnalysisMuons_matched_HLT_mu50{};

    // electrons
    std::vector<Float_t> *m_AnalysisElectrons_pt_NOSYS{};
    std::vector<Float_t> *m_AnalysisElectrons_eta{};
    std::vector<Float_t> *m_AnalysisElectrons_caloCluster_eta{};
    std::vector<Float_t> *m_AnalysisElectrons_phi{};
    std::vector<Float_t> *m_AnalysisElectrons_charge{};
    std::vector<Float_t> *m_AnalysisElectrons_effSF_NOSYS{};
    std::vector<Float_t> *m_AnalysisElectrons_effSF_ID_Tight_NOSYS{};
    std::vector<Float_t> *m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS{};
    std::vector<Float_t> *m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
    std::vector<Float_t> *m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS{};
    std::vector<Float_t> *m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS{};
    std::vector<Int_t> *m_AnalysisElectrons_truthType{};
    std::vector<Int_t> *m_AnalysisElectrons_truthOrigin{};
    std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherTruthType{};
    std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherTruthOrigin{};
    std::vector<Int_t> *m_AnalysisElectrons_firstEgMotherPdgId{};
    std::vector<bool> *m_AnalysisElectrons_el_selected_NOSYS{};
    std::vector<bool> *m_AnalysisElectrons_likelihood_Medium{};
    std::vector<bool> *m_AnalysisElectrons_likelihood_Tight{};
    std::vector<bool> *m_AnalysisElectrons_isIsolated_FCHighPtCaloOnly_NOSYS{};
    std::vector<bool> *m_AnalysisElectrons_isIsolated_FCLoose_NOSYS{};
    std::vector<bool> *m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS{};
    std::vector<bool> *m_AnalysisElectrons_isIsolated_Gradient_NOSYS{};
    std::vector<bool> *m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH{};
    std::vector<bool> *m_AnalysisElectrons_matched_HLT_e60_lhmedium{};
    std::vector<bool> *m_AnalysisElectrons_matched_HLT_e120_lhloose{};
    std::vector<bool> *m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose{};
    std::vector<bool> *m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0{};
    std::vector<bool> *m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0{};

    // jets
    std::vector<float> *m_AnalysisJets_pt_NOSYS{};
    std::vector<float> *m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70{};
    std::vector<float> *m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS{};
    std::vector<float> *m_AnalysisJets_eta{};
    std::vector<float> *m_AnalysisJets_phi{};
    std::vector<float> *m_AnalysisJets_m{};
    std::vector<bool> *m_AnalysisJets_jet_selected_NOSYS{};
    float m_EventInfo_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS{};

    // track-jets
    std::vector<float> *m_AnalysisAntiKt10PV0TrackJets_pt{};
    std::vector<float> *m_AnalysisAntiKt10PV0TrackJets_eta{};
    std::vector<float> *m_AnalysisAntiKt10PV0TrackJets_phi{};
    std::vector<float> *m_AnalysisAntiKt10PV0TrackJets_m{};
    std::vector<float> *m_AnalysisAntiKt8PV0TrackJets_pt{};
    std::vector<float> *m_AnalysisAntiKt8PV0TrackJets_eta{};
    std::vector<float> *m_AnalysisAntiKt8PV0TrackJets_phi{};
    std::vector<float> *m_AnalysisAntiKt8PV0TrackJets_m{};
    std::vector<float> *m_AnalysisAntiKt6PV0TrackJets_pt{};
    std::vector<float> *m_AnalysisAntiKt6PV0TrackJets_eta{};
    std::vector<float> *m_AnalysisAntiKt6PV0TrackJets_phi{};
    std::vector<float> *m_AnalysisAntiKt6PV0TrackJets_m{};

    // MET
    Float_t m_METInfo_MET_NOSYS{};
    Float_t m_METInfo_METPhi_NOSYS{};

    // MET for AntiTight
    Float_t m_METInfoAntiTight_NOSYS_MET{};
    Float_t m_METInfoAntiTight_NOSYS_METPhi{};

    // extra stuff
    std::vector<float> *m_AnalysisElectrons_d0{};
    std::vector<float> *m_AnalysisElectrons_d0sig{};
    std::vector<float> *m_AnalysisElectrons_z0sinTheta{};
    std::vector<float> *m_AnalysisElectrons_topoetcone20{};
    std::vector<float> *m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000{};
    std::vector<float> *m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000{};
    std::vector<float> *m_AnalysisElectrons_EOverP{};
    std::vector<float> *m_AnalysisMuons_d0{};
    std::vector<float> *m_AnalysisMuons_d0sig{};
    std::vector<float> *m_AnalysisMuons_z0sinTheta{};
    std::vector<float> *m_AnalysisMuons_topoetcone20{};
    std::vector<float> *m_AnalysisMuons_ptvarcone20_TightTTVA_pt1000{};
    std::vector<float> *m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000{};
    std::vector<float> *m_AnalysisMuons_ptvarcone30_TightTTVA_pt500{};
    std::vector<float> *m_AnalysisMuons_neflowisol20{};


    // systematics
    std::vector<std::vector<std::pair<unsigned int, float>>> m_electrons_sys;
    std::vector<std::vector<std::pair<unsigned int, float>>> m_muons_sys;
    std::vector<std::vector<std::pair<unsigned int, float>>> m_loose_electrons_sys;
    std::vector<std::vector<std::pair<unsigned int, float>>> m_loose_muons_sys;

    // electrons
    std::vector<std::vector<Float_t> *> m_sys_el_pt;
    std::vector<std::vector<bool> *> m_sys_el_iso;
    std::vector<std::vector<bool> *> m_sys_el_selected;
    std::vector<std::vector<Float_t> *> m_sys_el_reco_eff;
    std::vector<std::vector<Float_t> *> m_sys_el_id_eff;
    std::vector<std::vector<Float_t> *> m_sys_el_iso_eff;
    std::vector<std::vector<Float_t> *> m_sys_el_trig_eff;
    std::vector<std::vector<Float_t> *> m_sys_el_trig_sf;
    std::vector<std::vector<Float_t> *> m_sys_el_charge_sf;
    std::vector<std::vector<Float_t> *> m_sys_el_trig_noiso_eff;
    std::vector<std::vector<Float_t> *> m_sys_el_trig_noiso_sf;

    // muon
    std::vector<std::vector<Float_t> *> m_sys_mu_pt;
    std::vector<std::vector<bool> *> m_sys_mu_iso;
    std::vector<std::vector<bool> *> m_sys_mu_quality;
    std::vector<std::vector<bool> *> m_sys_mu_selected;
    std::vector<std::vector<Float_t> *> m_sys_mu_quality_eff;
    std::vector<std::vector<Float_t> *> m_sys_mu_ttva_eff;
    std::vector<std::vector<Float_t> *> m_sys_mu_iso_eff;
    std::vector<std::vector<Float_t> *> m_sys_mu_trig_2015_eff_data;
    std::vector<std::vector<Float_t> *> m_sys_mu_trig_2015_eff_mc;
    std::vector<std::vector<Float_t> *> m_sys_mu_trig_2018_eff_data;
    std::vector<std::vector<Float_t> *> m_sys_mu_trig_2018_eff_mc;

    // jets
    std::vector<std::vector<Float_t> *> m_sys_jet_pt;
    std::vector<std::vector<Float_t> *> m_sys_jet_ftag_eff;
    std::vector<std::vector<bool> *> m_sys_jet_selected;

    // event weight branches
    sys_branches<Float_t> m_sys_branches_GEN{};
    sys_branches<Float_t> m_sys_branches_PU{};
    sys_branches<Float_t> m_sys_branches_JVT{};
    sys_branches<Float_t> m_sys_branches_FTAG{};

    // met sys branches
    sys_branches<Float_t> m_sys_branches_MET_met{};
    sys_branches<Float_t> m_sys_branches_MET_phi{};

    // electron sys branches
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_pt{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_reco_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_id_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_iso_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_trig_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_trig_sf{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_charge_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_reco_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_id_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_iso_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_trig_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_trig_sf{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_charge_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_trig_noiso_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_trig_noiso_sf{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_trig_noiso_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_electron_calib_trig_noiso_sf{};
    sys_branches<std::vector<bool> *> m_sys_branches_electron_selected{};
    sys_branches<std::vector<bool> *> m_sys_branches_electron_isIsolated_Tight_VarRad{};

    // muon sys branches
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_pt{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_quality_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_ttva_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_iso_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_trig_2015_eff_data{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_trig_2015_eff_mc{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_trig_2018_eff_data{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_trig_2018_eff_mc{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_calib_quality_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_calib_ttva_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_calib_iso_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_calib_trig_2015_eff_data{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_calib_trig_2015_eff_mc{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_calib_trig_2018_eff_data{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_muon_calib_trig_2018_eff_mc{};
    sys_branches<std::vector<bool> *> m_sys_branches_muon_selected{};
    sys_branches<std::vector<bool> *> m_sys_branches_muon_isIsolated_PflowTight_VarRad{};
    sys_branches<std::vector<bool> *> m_sys_branches_muon_isQuality_Tight{};

    // jet sys branches
    sys_branches<std::vector<Float_t> *> m_sys_branches_jet_pt{};
    sys_branches<std::vector<bool> *> m_sys_branches_jet_selected{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_jet_ftag_eff{};
    sys_branches<std::vector<Float_t> *> m_sys_branches_jet_calib_ftag_eff{};
};

}  // namespace Charm

#endif  // W_PLUS_CHARM_LOOP_H
