#include <TH1D.h>
#include <TH2D.h>

#include "EventLoopBase.h"

namespace Charm
{
void EventLoopBase::multiply_event_weight(float x, unsigned int i)
{
    if (i == 0)
    {
        m_event_weight *= x;
    }
    else
    {
        m_event_weight_sys.at(i - 1) *= x;
    }
}

void EventLoopBase::multiply_event_weights(float x)
{
    m_event_weight *= x;
    if (m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            m_event_weight_sys.at(i) *= x;
        }
    }
}

void EventLoopBase::multiply_nominal_weight(float x)
{
    m_event_weight *= x;
}

void EventLoopBase::multiply_sys_weight(float x, unsigned int i)
{
    m_event_weight_sys.at(i) *= x;
}

void EventLoopBase::multiply_sys_weight(float x, std::string key)
{
    m_event_weight_sys.at(get_sys_position(key)) *= x;
}

void EventLoopBase::multiply_nominal_and_sys_weight(float nominal, sys_branches<Float_t> *sys, bool fix_zero)
{
    if (!fix_zero || (nominal && !isnan(nominal)))
    {
        m_event_weight *= nominal;
    }
    std::vector<unsigned int> sys_position;
    for (unsigned int j = 0; j < sys->keys.size(); j++)
    {
        sys->branches.at(j)->GetEntry(m_current_event);
        sys_position.push_back(get_sys_position(sys->keys.at(j)));
    }
    // std::cout << "nominal " << nominal << std::endl;
    for (unsigned int i = 0; i < m_systematics.size(); i++)
    {
        unsigned int j = std::find(sys_position.begin(), sys_position.end(), i) - sys_position.begin();
        if (j < sys_position.size())
        {
            // std::cout << sys->keys.at(j) << " " << sys->values.at(j) << " " << i << std::endl;
            if (!fix_zero || (sys->values.at(j) && !isnan(sys->values.at(j))))
            {
                m_event_weight_sys.at(i) *= sys->values.at(j);
            }
        }
        else
        {
            if (!fix_zero || (nominal && !isnan(nominal)))
            {
                m_event_weight_sys.at(i) *= nominal;
            }
        }
    }
}

void EventLoopBase::multiply_mc_event_weight(float nominal, sys_branches<Float_t> *sys, bool apply_lumi, bool lumi_ratio, bool fix_sherpa_weights)
{
    float lumi = 1.0;
    if (apply_lumi)
    {
        lumi = m_lumi;
    }
    if (lumi_ratio && apply_lumi)
    {
        lumi = lumi / Charm::LUMI_RUN2;
    }
    float nominal_gen_weight = nominal;
    if (fix_sherpa_weights)
    {
        if (fabs(nominal_gen_weight) > 10)
        {
            nominal_gen_weight = 1.0;
        }
    }
    float exp_weight = lumi * m_cross_section / m_init_sum_of_w;
    m_event_weight *= nominal_gen_weight * exp_weight;
    std::vector<unsigned int> sys_position;
    for (unsigned int j = 0; j < sys->keys.size(); j++)
    {
        sys->branches.at(j)->GetEntry(m_current_event);
        sys_position.push_back(get_sys_position(sys->keys.at(j)));
        if (fix_sherpa_weights)
        {
            if (fabs(sys->values.at(j)) > 10)
            {
                sys->values.at(j) = 1.0;
            }
        }
    }
    for (unsigned int i = 0; i < m_systematics.size(); i++)
    {
        unsigned int j = std::find(sys_position.begin(), sys_position.end(), i) - sys_position.begin();
        if (j < sys_position.size())
        {
            m_event_weight_sys.at(i) *= sys->values.at(j) * lumi * m_cross_section / m_init_sum_of_w_sys_vec.at(i);
        }
        else
        {
            m_event_weight_sys.at(i) *= nominal_gen_weight * exp_weight;
        }
    }
}

float EventLoopBase::get_ptv_weight(float pt, std::string jet_slice)
{
    TH1D *rw_histo = m_rw_map[m_mc_gen + "_" + jet_slice + "_ptlllog"];
    int bin = rw_histo->FindBin(log10(pt));
    return rw_histo->GetBinContent(bin);
}

// check if channel exists for histograms
void EventLoopBase::check_channel(std::string channel)
{
    if (channel == "")
    {
        return;
    }
    if (m_histograms.find(channel) == m_histograms.end())
    {
        m_histograms[channel] = {};
        if (m_do_systematics)
        {
            m_histograms_sys[channel] = {};
        }
    }
}

// fill histogram in channel
void EventLoopBase::fill_histogram_in_channel(std::string channel, std::string name, float val, int n, int xmin, int xmax, bool do_sys)
{
    check_channel(channel);
    bool histo_exists = !(m_histograms.at(channel).find(name) == m_histograms.at(channel).end());
    if (!histo_exists)
    {
        add_histogram(name, channel, do_sys, n, xmin, xmax);
    }
    m_histograms.at(channel).at(name)->Fill(val, m_event_weight);
    if (do_sys && m_do_systematics)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i));
        }
    }
}

void EventLoopBase::fill_histogram_in_channel(std::string channel, std::string name, float val, int n, const double *xbins, bool do_sys)
{
    check_channel(channel);
    bool histo_exists = !(m_histograms.at(channel).find(name) == m_histograms.at(channel).end());
    if (!histo_exists)
    {
        add_histogram(name, channel, do_sys, n, xbins);
    }
    m_histograms.at(channel).at(name)->Fill(val, m_event_weight);
    if (do_sys && m_do_systematics)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i));
        }
    }
}

void EventLoopBase::fill_histogram_in_channel(std::string channel, std::string name, float valx, float valy, int nx, float xmin, float xmax, int ny, float ymin, float ymax, bool do_sys)
{
    check_channel(channel);
    bool histo_exists = !(m_histograms.at(channel).find(name) == m_histograms.at(channel).end());
    if (!histo_exists)
    {
        add_histogram(name, channel, do_sys, nx, xmin, xmax, ny, ymin, ymax);
    }
    dynamic_cast<TH2D *>(m_histograms.at(channel).at(name))->Fill(valx, valy, m_event_weight);
    if (do_sys && m_do_systematics)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            dynamic_cast<TH2D *>(histograms_sys->at(i))->Fill(valx, valy, m_event_weight_sys.at(i));
        }
    }
}

// fill histogram without checking whether it exists or not
void EventLoopBase::add_fill_histogram_fast(std::string name, float val, int n, float xmin, float xmax, bool histo_exists, bool do_sys)
{
    if (!histo_exists)
    {
        add_histogram(name, m_channel, do_sys, n, xmin, xmax);
    }
    m_histograms.at(m_channel).at(name)->Fill(val, m_event_weight);
    if (do_sys && m_do_systematics)
    {
        auto *histograms_sys = &m_histograms_sys[m_channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i));
        }
    }
}

// fill histogram without checking whether it exists or not and use calibration systematics
void EventLoopBase::add_fill_histogram_fast(std::string name, std::vector<float> val, int n, float xmin, float xmax, bool histo_exists, bool do_sys)
{
    if (!histo_exists)
    {
        add_histogram(name, m_channel, do_sys, n, xmin, xmax);
    }
    m_histograms.at(m_channel).at(name)->Fill(val.at(0), m_event_weight);
    if (do_sys && m_do_systematics)
    {
        auto *histograms_sys = &m_histograms_sys[m_channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            histograms_sys->at(i)->Fill(val.at(i + 1), m_event_weight_sys.at(i));
        }
    }
}

// fill histogram without checking whether it exists or not
void EventLoopBase::add_fill_histogram_fast_in_channel(std::string channel, std::string name, float val, int n, float xmin, float xmax, bool histo_exists, bool do_sys)
{
    check_channel(channel);
    if (!histo_exists)
    {
        add_histogram(name, channel, do_sys, n, xmin, xmax);
    }
    m_histograms.at(channel).at(name)->Fill(val, m_event_weight);
    if (do_sys && m_do_systematics)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i));
        }
    }
}

void EventLoopBase::add_fill_histogram_fast_in_channel(std::string channel, std::string name, float valx, float valy, int nx, float xmin, float xmax, int ny, float ymin, float ymax, bool histo_exists, bool do_sys)
{
    check_channel(channel);
    if (!histo_exists)
    {
        add_histogram(name, channel, do_sys, nx, xmin, xmax, ny, ymin, ymax);
    }
    dynamic_cast<TH2D *>(m_histograms.at(channel).at(name))->Fill(valx, valy, m_event_weight);
    if (do_sys && m_do_systematics)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            dynamic_cast<TH2D *>(histograms_sys->at(i))->Fill(valx, valy, m_event_weight_sys.at(i));
        }
    }
}

void EventLoopBase::add_fill_histogram_fast_in_channel(std::string channel, std::string name, float valx, float valy, int nx, float xmin, float xmax, int ny, const double *ybins, bool histo_exists, bool do_sys)
{
    check_channel(channel);
    if (!histo_exists)
    {
        add_histogram(name, channel, do_sys, nx, xmin, xmax, ny, ybins);
    }
    dynamic_cast<TH2D *>(m_histograms.at(channel).at(name))->Fill(valx, valy, m_event_weight);
    if (m_do_systematics && do_sys)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            dynamic_cast<TH2D *>(histograms_sys->at(i))->Fill(valx, valy, m_event_weight_sys.at(i));
        }
    }
}

// fill histogram sys
void EventLoopBase::add_fill_histogram_sys_fast(std::string name, std::vector<float> val, int n, float xmin, float xmax, bool do_sys)
{
    if (!m_histograms_created)
    {
        add_histogram(name, do_sys, n, xmin, xmax);
    }
    std::string nominal_channel = m_channel_sys.at(0);
    if (nominal_channel != "")
    {
        m_histograms.at(nominal_channel).at(name)->Fill(val.at(0), m_event_weight);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1);
            if (sys_channel != "")
            {
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                histograms_sys->at(i)->Fill(val.at(i + 1), m_event_weight_sys.at(i));
            }
        }
    }
}

// fill histogram sys
void EventLoopBase::add_fill_histogram_sys_fast(std::string name, float val, int n, float xmin, float xmax, bool do_sys)
{
    if (!m_histograms_created)
    {
        add_histogram(name, do_sys, n, xmin, xmax);
    }
    std::string nominal_channel = m_channel_sys.at(0);
    if (nominal_channel != "")
    {
        m_histograms.at(nominal_channel).at(name)->Fill(val, m_event_weight);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1);
            if (sys_channel != "")
            {
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i));
            }
        }
    }
}

// fill histogram sys
// TODO: make a single template function for all add_fill_histogram_sys?
void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> val, int n, float xmin, float xmax, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, n, xmin, xmax);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        m_histograms.at(nominal_channel).at(name)->Fill(val.at(0), m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, n, xmin, xmax);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                histograms_sys->at(i)->Fill(val.at(i + 1), m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

// fill histogram sys
void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, float val, int n, float xmin, float xmax, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, n, xmin, xmax);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        m_histograms.at(nominal_channel).at(name)->Fill(val, m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, n, xmin, xmax);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

// fill histogram sys
void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> val, int n, const double *xbins, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, n, xbins);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        m_histograms.at(nominal_channel).at(name)->Fill(val.at(0), m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, n, xbins);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                histograms_sys->at(i)->Fill(val.at(i + 1), m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

// fill histogram sys
void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, float val, int n, const double *xbins, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, n, xbins);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        m_histograms.at(nominal_channel).at(name)->Fill(val, m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, n, xbins);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

// fill histogram sys
void EventLoopBase::add_fill_histogram_sys(std::string name, std::string suffix, std::vector<float> val, int n, float xmin, float xmax, bool do_sys)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix;
    if (m_channel_sys.at(0) != "")
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, n, xmin, xmax);
        }
        m_histograms.at(nominal_channel).at(name)->Fill(val.at(0), m_event_weight);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix;
            if (m_channel_sys.at(i + 1) != "")
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, n, xmin, xmax);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                histograms_sys->at(i)->Fill(val.at(i + 1), m_event_weight_sys.at(i));
            }
        }
    }
}

// fill histogram sys
void EventLoopBase::add_fill_histogram_sys(std::string name, std::string suffix, float val, int n, float xmin, float xmax, bool do_sys)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix;
    if (m_channel_sys.at(0) != "")
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, n, xmin, xmax);
        }
        m_histograms.at(nominal_channel).at(name)->Fill(val, m_event_weight);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix;
            if (m_channel_sys.at(i + 1) != "")
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, n, xmin, xmax);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                histograms_sys->at(i)->Fill(val, m_event_weight_sys.at(i));
            }
        }
    }
}

void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> valx, std::vector<float> valy, int nx,
                                           float xmin, float xmax, int ny, float ymin, float ymax, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, nx, xmin, xmax, ny, ymin, ymax);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        dynamic_cast<TH2D *>(m_histograms.at(nominal_channel).at(name))->Fill(valx.at(0), valy.at(0), m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, nx, xmin, xmax, ny, ymin, ymax);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                dynamic_cast<TH2D *>(histograms_sys->at(i))->Fill(valx.at(i + 1), valy.at(i + 1), m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> valx, std::vector<float> valy, int nx,
                                           float xmin, float xmax, int ny, const double *ybins, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, nx, xmin, xmax, ny, ybins);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        dynamic_cast<TH2D *>(m_histograms.at(nominal_channel).at(name))->Fill(valx.at(0), valy.at(0), m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, nx, xmin, xmax, ny, ybins);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                dynamic_cast<TH2D *>(histograms_sys->at(i))->Fill(valx.at(i + 1), valy.at(i + 1), m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, float valx, float valy,
                                           int nx, const double *xbins, int ny, const double *ybins, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, nx, xbins, ny, ybins);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        dynamic_cast<TH2D *>(m_histograms.at(nominal_channel).at(name))->Fill(valx, valy, m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, nx, xbins, ny, ybins);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                dynamic_cast<TH2D *>(histograms_sys->at(i))->Fill(valx, valy, m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

void EventLoopBase::add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> valx, float valy,
                                           int nx, const double *xbins, int ny, const double *ybins, bool do_sys, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    std::string nominal_channel = m_channel_sys.at(0) + suffix.at(0);
    if (m_channel_sys.at(0) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(0))))
    {
        check_channel(nominal_channel);
        bool histo_exists = !(m_histograms.at(nominal_channel).find(name) == m_histograms.at(nominal_channel).end());
        if (!histo_exists)
        {
            add_histogram(name, nominal_channel, do_sys, nx, xbins, ny, ybins);
        }
        double extra_w = 1.0;
        if (extra_weight.size() && extra_weight.at(0) != 0)
        {
            extra_w = extra_weight.at(0);
        }
        dynamic_cast<TH2D *>(m_histograms.at(nominal_channel).at(name))->Fill(valx.at(0), valy, m_event_weight * extra_w);
    }
    if (do_sys && m_do_systematics)
    {
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            std::string sys_channel = m_channel_sys.at(i + 1) + suffix.at(i + 1);
            if (m_channel_sys.at(i + 1) != "" && (!extra_flag.size() || (extra_flag.size() && extra_flag.at(i + 1))))
            {
                check_channel(sys_channel);
                bool histo_exists = !(m_histograms.at(sys_channel).find(name) == m_histograms.at(sys_channel).end());
                if (!histo_exists)
                {
                    add_histogram(name, sys_channel, do_sys, nx, xbins, ny, ybins);
                }
                auto *histograms_sys = &m_histograms_sys[sys_channel][name];
                double extra_w = 1.0;
                if (extra_weight.size() && extra_weight.at(i + 1) != 0)
                {
                    extra_w = extra_weight.at(i + 1);
                }
                dynamic_cast<TH2D *>(histograms_sys->at(i))->Fill(valx.at(i + 1), valy, m_event_weight_sys.at(i) * extra_w);
            }
        }
    }
}

// add histogram
void EventLoopBase::add_histogram(std::string name, bool do_sys, int n, float xmin, float xmax)
{
    for (auto channel : m_channels)
    {
        std::string full_name = channel + "__" + name;
        m_histograms[channel][name] = new TH1D(full_name.c_str(), full_name.c_str(), n, xmin, xmax);
        m_histograms[channel][name]->Sumw2(true);
        if (m_do_systematics && do_sys)
        {
            auto *histograms_sys = &m_histograms_sys[channel][name];
            for (unsigned int i = 0; i < m_systematics.size(); i++)
            {
                full_name = m_systematics.at(i) + "_-_" + channel + "__" + name;
                histograms_sys->push_back(new TH1D(full_name.c_str(), full_name.c_str(), n, xmin, xmax));
                histograms_sys->at(i)->Sumw2(true);
            }
        }
    }
}

void EventLoopBase::add_histogram(std::string name, bool do_sys, int n, const double *xbins)
{
    for (auto channel : m_channels)
    {
        std::string full_name = channel + "__" + name;
        m_histograms[channel][name] = new TH1D(full_name.c_str(), full_name.c_str(), n, xbins);
        m_histograms[channel][name]->Sumw2(true);
        if (m_do_systematics && do_sys)
        {
            auto *histograms_sys = &m_histograms_sys[channel][name];
            for (unsigned int i = 0; i < m_systematics.size(); i++)
            {
                full_name = m_systematics.at(i) + "_-_" + channel + "__" + name;
                histograms_sys->push_back(new TH1D(full_name.c_str(), full_name.c_str(), n, xbins));
                histograms_sys->at(i)->Sumw2(true);
            }
        }
    }
}

void EventLoopBase::add_histogram(std::string name, std::string channel, bool do_sys, int n, float xmin, float xmax)
{
    std::string full_name = channel + "__" + name;
    m_histograms[channel][name] = new TH1D(full_name.c_str(), full_name.c_str(), n, xmin, xmax);
    m_histograms[channel][name]->Sumw2(true);
    if (m_do_systematics && do_sys)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            full_name = m_systematics.at(i) + "_-_" + channel + "__" + name;
            histograms_sys->push_back(new TH1D(full_name.c_str(), full_name.c_str(), n, xmin, xmax));
            histograms_sys->at(i)->Sumw2(true);
        }
    }
}

void EventLoopBase::add_histogram(std::string name, std::string channel, bool do_sys, int n, const double *xbins)
{
    std::string full_name = channel + "__" + name;
    m_histograms[channel][name] = new TH1D(full_name.c_str(), full_name.c_str(), n, xbins);
    m_histograms[channel][name]->Sumw2(true);
    if (m_do_systematics && do_sys)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            full_name = m_systematics.at(i) + "_-_" + channel + "__" + name;
            histograms_sys->push_back(new TH1D(full_name.c_str(), full_name.c_str(), n, xbins));
            histograms_sys->at(i)->Sumw2(true);
        }
    }
}

void EventLoopBase::add_histogram(std::string name, std::string channel, bool do_sys, int nx, float xmin, float xmax, int ny, float ymin, float ymax)
{
    std::string full_name = channel + "__" + name;
    m_histograms[channel][name] = new TH2D(full_name.c_str(), full_name.c_str(), nx, xmin, xmax, ny, ymin, ymax);
    m_histograms[channel][name]->Sumw2(true);
    if (m_do_systematics && do_sys)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            full_name = m_systematics.at(i) + "_-_" + channel + "__" + name;
            histograms_sys->push_back(new TH2D(full_name.c_str(), full_name.c_str(), nx, xmin, xmax, ny, ymin, ymax));
            histograms_sys->at(i)->Sumw2(true);
        }
    }
}

void EventLoopBase::add_histogram(std::string name, std::string channel, bool do_sys, int nx, float xmin, float xmax, int ny, const double *ybins)
{
    std::string full_name = channel + "__" + name;
    m_histograms[channel][name] = new TH2D(full_name.c_str(), full_name.c_str(), nx, xmin, xmax, ny, ybins);
    m_histograms[channel][name]->Sumw2(true);
    if (m_do_systematics && do_sys)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            full_name = m_systematics.at(i) + "_-_" + channel + "__" + name;
            histograms_sys->push_back(new TH2D(full_name.c_str(), full_name.c_str(), nx, xmin, xmax, ny, ybins));
            histograms_sys->at(i)->Sumw2(true);
        }
    }
}

void EventLoopBase::add_histogram(std::string name, std::string channel, bool do_sys, int nx, const double *xbins, int ny, const double *ybins)
{
    std::string full_name = channel + "__" + name;
    m_histograms[channel][name] = new TH2D(full_name.c_str(), full_name.c_str(), nx, xbins, ny, ybins);
    m_histograms[channel][name]->Sumw2(true);
    if (m_do_systematics && do_sys)
    {
        auto *histograms_sys = &m_histograms_sys[channel][name];
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            full_name = m_systematics.at(i) + "_-_" + channel + "__" + name;
            histograms_sys->push_back(new TH2D(full_name.c_str(), full_name.c_str(), nx, xbins, ny, ybins));
            histograms_sys->at(i)->Sumw2(true);
        }
    }
}

// sys position
unsigned int EventLoopBase::get_sys_position(std::string key)
{
    return std::find(m_systematics.begin(), m_systematics.end(), key) - m_systematics.begin();
}

// get systematics from group
std::vector<std::string> EventLoopBase::get_systematics_from_group(std::string group)
{
    if (m_used_systematics.size() && (std::find(m_used_systematics.begin(),
                                                m_used_systematics.end(), group) != m_used_systematics.end()))
    {
        return m_systematics_groups.at(group);
    }
    return {};
}

}  // namespace Charm
