#ifndef D_MESON_REC_H
#define D_MESON_REC_H

#include "EventLoopBase.h"
#include "TruthInfo.h"

namespace Charm
{
// Forward declaration
class EventLoopBase;
class TruthInfo;

class DMesonRec
{
private:
    friend class TruthInfo;

public:
    DMesonRec();

    bool fill_D_plus_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dplus_index, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {}, bool diff_bins_override = false);

    bool fill_D_plus_histograms_scan(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dplus_index, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    bool fill_D_star_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dstar_index, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {}, bool diff_bins_override = false);

    bool fill_D_star_histograms_scan(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dstar_index, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    bool fill_D_star_pi0_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dstar_pi0_index, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {}, bool diff_bins_override = false);

    bool fill_D_s_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Ds_index, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {}, bool diff_bins_override = false);

    bool fill_D_s_histograms_scan(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Ds_index, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void connect_D_meson_branches(EventLoopBase *ELB);

    std::string get_charge_prefix(float lep_charge, unsigned int index);

    std::pair<unsigned int, int> get_best_D_candidate(std::vector<std::pair<unsigned int, int>> candidates);

    void set_lepton(TLorentzVector *lep) { m_lep = lep; };

    float D_meson_lepton_dR(unsigned int index, float lep_eta, float lep_phi);

private:
    float D_meson_min_track_pt(unsigned int index);
    float D_meson_max_track_dR(unsigned int index);
    float D_meson_lepton_dR(unsigned int index);
    bool cut_fail_condition(int cut_number, int &failed_cut);
    float soft_pion_Dstar_separation(unsigned int index, unsigned int D0Index);
    bool pass_ith_cut(unsigned int i, unsigned int index);
    std::string D_meson_pt_bin(EventLoopBase *ELB, unsigned int index);
    std::string D_meson_eta_bin(EventLoopBase *ELB, int i);
    bool D_pass_tight(unsigned int index);

protected:
    bool m_D_tightPrimary_tracks;
    bool m_do_Nminus1;
    bool m_do_twice_failed;
    bool m_bin_in_D_pt;
    bool m_bin_in_lep_eta;
    int m_do_cutScan = 0;
    std::string m_D_branch_sys = "";

private:
    static const int m_NcutVals = 10;
    double m_cut_vals_Lxy[m_NcutVals] = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
    //double m_cut_vals_Lxy[m_NcutVals] = {0.0, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6};
    double m_cut_vals_TrPt[m_NcutVals] = {500, 550, 600, 650, 700, 750., 800, 850, 900, 950};
    double m_cut_vals_Pt[m_NcutVals] = {5.0, 6.0, 7.0, 8.0, 9.0, 10., 11.0, 12.0, 13.0, 14.0};
    double m_cut_vals_Ctau[m_NcutVals] = {0.0, 0.20, 0.22, 0.24, 0.26, 0.28, 0.30, 0.32, 0.34, 0.36};
    double m_cut_vals_CosThetaStar[m_NcutVals] = {-1.0, -.95, -.90, -.85, -.80, -.75, -.70, -.65, -.60, -.55};
    double m_cut_vals_Chi2[m_NcutVals] = {100000.,20.0, 18.0, 15.0, 12.0, 10.0, 8.0, 7.0, 6.0, 5.0}; 
    double m_cut_vals_Iso[m_NcutVals] = {100., 4.5, 4.0, 3.5, 3.0, 2.5, 2.0, 1.5, 1.0, 0.5};
//     double m_cut_vals_3DimpactSig[m_NcutVals] = {100., 20.0, 18.0, 16.0, 14.0, 12.0, 10.0, 8.0, 4.0, 3.0};
    double m_cut_vals_3DimpactSig[m_NcutVals] = {100., 14.0, 12.0, 10.0, 9.0, 8.0, 7.0, 6.0, 5.0, 4.0};
    double m_cut_vals_Dplus_Ds_test[m_NcutVals] = {4.0, 5.0, 6.0, 7.0, 8.0, 10.0, 12.0, 14.0, 15.0, 16.0};

private:
    std::set<std::string> m_created_channels;
    TLorentzVector *m_lep;

protected:
    void get_D_presel(EventLoopBase *ELB, int decayType);
    void get_D_plus_mesons(EventLoopBase *ELB, bool loose_selection = false);
    void get_D_star_mesons(EventLoopBase *ELB, bool loose_selection = false);
    void get_D_star_pi0_mesons(EventLoopBase *ELB, bool loose_selection = false);
    void get_D_s_mesons(EventLoopBase *ELB, bool loose_selection = false);

protected:
    std::vector<unsigned int> m_presel_Dplus;
    std::vector<unsigned int> m_presel_Dstar;
    std::vector<unsigned int> m_presel_Dstar_pi0;
    std::vector<unsigned int> m_presel_Ds;
    std::vector<std::pair<unsigned int, int>> m_D_plus_mesons;
    std::vector<std::pair<unsigned int, int>> m_D_star_mesons;
    std::vector<std::pair<unsigned int, int>> m_D_star_pi0_mesons;
    std::vector<std::pair<unsigned int, int>> m_D_s_mesons;

protected:
    std::vector<Float_t> *m_DMesons_costhetastar{};
    std::vector<Float_t> *m_DMesons_DeltaMass{};
    std::vector<Float_t> *m_DMesons_fitOutput__Chi2{};
    std::vector<Float_t> *m_DMesons_fitOutput__Impact{};
    std::vector<Float_t> *m_DMesons_fitOutput__ImpactError{};
    std::vector<Float_t> *m_DMesons_fitOutput__ImpactZ0{};
    std::vector<Float_t> *m_DMesons_fitOutput__ImpactZ0Error{};
    std::vector<Float_t> *m_DMesons_fitOutput__ImpactTheta{};
    std::vector<Float_t> *m_DMesons_fitOutput__ImpactZ0SinTheta{};
    std::vector<Float_t> *m_DMesons_fitOutput__ImpactSignificance{};
    std::vector<Float_t> *m_DMesons_fitOutput__Lxy{};
    std::vector<Float_t> *m_DMesons_mKpi1{};
    std::vector<Float_t> *m_DMesons_mKpi2{};
    std::vector<Float_t> *m_DMesons_mPhi1{};
    std::vector<Float_t> *m_DMesons_mPhi2{};
    std::vector<Float_t> *m_DMesons_ptcone40{};
    std::vector<Float_t> *m_DMesons_SlowPionD0{};
    std::vector<Float_t> *m_DMesons_SlowPionZ0SinTheta{};
    std::vector<Float_t> *m_DMesons_eta{};
    std::vector<Float_t> *m_DMesons_m{};
    std::vector<Float_t> *m_DMesons_phi{};
    std::vector<Float_t> *m_DMesons_pt{};
    std::vector<Int_t> *m_DMesons_D0Index{};
    std::vector<Int_t> *m_DMesons_decayType{};
    std::vector<Int_t> *m_DMesons_fitOutput__Charge{};
    std::vector<Int_t> *m_DMesons_pdgId{};
    std::vector<Int_t> *m_DMesons_truthBarcode{};
    std::vector<std::vector<bool>> *m_DMesons_daughterInfo__passTight{};
    std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__eta{};
    std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__phi{};
    std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__pt{};
    std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__z0SinTheta{};
    std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__z0SinThetaPV{};
    std::vector<std::vector<Float_t>> *m_DMesons_fitOutput__VertexPosition{};
    std::vector<std::vector<Float_t>> *m_DMesons_daughterInfo__pdgId{};
    std::vector<std::vector<Int_t>> *m_DMesons_daughterInfo__truthDBarcode{};
    std::vector<std::vector<Int_t>> *m_DMesons_daughterInfo__truthBarcode{};

protected:
    std::vector<Float_t> m_sys_dmeson_mass;
};

}  // namespace Charm

#endif  // D_MESON_REC_H
