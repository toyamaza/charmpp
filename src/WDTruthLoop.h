#ifndef WD_TRUTH_LOOP_H
#define WD_TRUTH_LOOP_H

#include "EventLoopBase.h"
#include "HelperFunctions.h"
#include "TruthInfo.h"

namespace Charm
{
class WDTruthLoop : public EventLoopBase, public TruthInfo
{
public:
    WDTruthLoop(TString input_file, TString out_path, TString tree_name = "CharmAnalysis");

private:
    virtual void connect_branches() override;

    virtual void initialize() override;

    virtual bool preselection() override;

    virtual int execute() override;

    float dR_min(std::vector<unsigned int> *selected, bool OS = false);

    void fill_histograms(std::string channel = "");

    void fill_D_histograms(std::string channel, unsigned int i);

    void fill_Dplus_histograms(std::string channel = "", bool requireDecay = false);

    void fill_Dstar_histograms(std::string channel = "", bool requireDecay = false);

    void fill_Dzero_histograms(std::string channel = "", bool requireDecay = false);

    void configure();

    // event info
    Float_t m_EventInfo_generatorWeight_NOSYS{};
    Float_t m_EventInfo_PileupWeight_NOSYS{};

    // systematics
    sys_branches<Float_t> m_sys_branches_GEN{};

    // fiducial met cuts
    bool m_do_fiducial_met{false};
    bool m_do_prod_fraction_rw{false};
    bool m_do_wjets_rw{false};
};

}  // namespace Charm

#endif  // WD_TRUTH_LOOP_H
