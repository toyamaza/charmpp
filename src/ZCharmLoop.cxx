#include "ZCharmLoop.h"

#include <math.h>

#include <iostream>

#include "TRegexp.h"
#include "TVector2.h"

namespace Charm
{
void ZCharmLoop::initialize()
{
    std::cout << "ZCharmLoop::initiaize" << std::endl;

    // systematics
    if (m_do_systematics)
    {
        // GEN
        m_sys_branches_GEN.init(get_systematics_from_group("systematics_GEN"));

        // PRW
        m_sys_branches_PU.init(get_systematics_from_group("systematics_PRW"));

        // JVT
        m_sys_branches_JVT.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_JVT.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_JVT.init(get_systematics_from_group("systematics_JET_Jvt"));
        m_sys_branches_JVT.init(get_systematics_from_group("systematics_MUON_Calib"));

        // FTAG
        m_sys_branches_FTAG.init(get_systematics_from_group("systematics_FT_EFF"));
        m_sys_branches_FTAG.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_FTAG.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_FTAG.init(get_systematics_from_group("systematics_JET_Calib"));

        // MET
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_MET_Calib"));
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_MUON_Calib"));

        // electron calib
        m_sys_branches_electron_isIsolated_FCTight.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_pt.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_selected_other_sys.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_electron_selected_other_sys.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_electron_selected.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_reco_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_id_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_iso_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_trig_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_trig_sf.init(get_systematics_from_group("systematics_EL_Calib"));

        // muon calib
        m_sys_branches_muon_isQuality_Tight.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_pt.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_selected_other_sys.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_muon_selected_other_sys.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_muon_selected.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_isIsolated_PflowTight_VarRad.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_quality_eff.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_ttva_eff.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2015_eff_data.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_iso_eff.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2015_eff_mc.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2018_eff_data.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2018_eff_mc.init(get_systematics_from_group("systematics_MUON_Calib"));

        // electron weights
        m_sys_branches_electron_reco_eff.init(get_systematics_from_group("systematics_EL_EFF_Reco"));
        m_sys_branches_electron_id_eff.init(get_systematics_from_group("systematics_EL_EFF_ID"));
        m_sys_branches_electron_iso_eff.init(get_systematics_from_group("systematics_EL_EFF_Iso"));
        m_sys_branches_electron_trig_eff.init(get_systematics_from_group("systematics_EL_EFF_Trigger"));
        m_sys_branches_electron_trig_sf.init(get_systematics_from_group("systematics_EL_EFF_Trigger"));

        // muon weights
        m_sys_branches_muon_quality_eff.init(get_systematics_from_group("systematics_MUON_EFF_RECO"));
        m_sys_branches_muon_ttva_eff.init(get_systematics_from_group("systematics_MUON_EFF_TTVA"));
        m_sys_branches_muon_trig_2015_eff_data.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));
        m_sys_branches_muon_iso_eff.init(get_systematics_from_group("systematics_MUON_EFF_ISO"));
        m_sys_branches_muon_trig_2015_eff_mc.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));
        m_sys_branches_muon_trig_2018_eff_data.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));
        m_sys_branches_muon_trig_2018_eff_mc.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));

        // jet calib
        m_sys_branches_jet_pt.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_jet_selected.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_jet_selected_other_sys.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_jet_selected_other_sys.init(get_systematics_from_group("systematics_MUON_Calib"));
    }
}

void ZCharmLoop::read_sys_branches()
{
    // read electrons
    m_sys_el_iso = get_val_for_systematics(m_AnalysisElectrons_isIsolated_FCTight_NOSYS, &m_sys_branches_electron_isIsolated_FCTight);
    m_sys_el_pt = get_val_for_systematics(m_AnalysisElectrons_pt_NOSYS, &m_sys_branches_electron_pt);
    m_sys_el_selected = get_val_for_systematics(m_AnalysisElectrons_el_selected_NOSYS, &m_sys_branches_electron_selected);
    m_sys_el_selected_other = get_val_for_systematics(m_AnalysisElectrons_el_selected_NOSYS, &m_sys_branches_electron_selected_other_sys);
    m_sys_el_reco_eff = get_val_for_systematics(m_AnalysisElectrons_effSF_NOSYS, &m_sys_branches_electron_reco_eff, &m_sys_branches_electron_calib_reco_eff);
    m_sys_el_id_eff = get_val_for_systematics(m_AnalysisElectrons_effSF_ID_Tight_NOSYS, &m_sys_branches_electron_id_eff, &m_sys_branches_electron_calib_id_eff);
    m_sys_el_iso_eff = get_val_for_systematics(m_AnalysisElectrons_effSF_Isol_Tight_FCTight_NOSYS, &m_sys_branches_electron_iso_eff, &m_sys_branches_electron_calib_iso_eff);
    m_sys_el_trig_sf = get_val_for_systematics(m_AnalysisElectrons_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
                                               &m_sys_branches_electron_trig_sf, &m_sys_branches_electron_calib_trig_sf);
    m_sys_el_trig_eff = get_val_for_systematics(m_AnalysisElectrons_effSF_Trig_Tight_FCTight_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
                                                &m_sys_branches_electron_trig_eff, &m_sys_branches_electron_calib_trig_eff);

    // read muons
    m_sys_mu_pt = get_val_for_systematics(m_AnalysisMuons_pt_NOSYS, &m_sys_branches_muon_pt);
    m_sys_mu_quality = get_val_for_systematics(m_AnalysisMuons_isQuality_Tight_NOSYS, &m_sys_branches_muon_isQuality_Tight);
    m_sys_mu_selected = get_val_for_systematics(m_AnalysisMuons_mu_selected_NOSYS, &m_sys_branches_muon_selected);
    m_sys_mu_selected_other = get_val_for_systematics(m_AnalysisMuons_mu_selected_NOSYS, &m_sys_branches_muon_selected_other_sys);
    m_sys_mu_iso = get_val_for_systematics(m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS, &m_sys_branches_muon_isIsolated_PflowTight_VarRad);
    m_sys_mu_quality_eff = get_val_for_systematics(m_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS, &m_sys_branches_muon_quality_eff, &m_sys_branches_muon_calib_quality_eff);
    m_sys_mu_ttva_eff = get_val_for_systematics(m_AnalysisMuons_muon_effSF_TTVA_NOSYS, &m_sys_branches_muon_ttva_eff, &m_sys_branches_muon_calib_ttva_eff);
    m_sys_mu_iso_eff = get_val_for_systematics(m_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS, &m_sys_branches_muon_iso_eff, &m_sys_branches_muon_calib_iso_eff);
    m_sys_mu_trig_2015_eff_mc = get_val_for_systematics(m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
                                                        &m_sys_branches_muon_trig_2015_eff_mc, &m_sys_branches_muon_calib_trig_2015_eff_mc);
    m_sys_mu_trig_2015_eff_data = get_val_for_systematics(m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
                                                          &m_sys_branches_muon_trig_2015_eff_data, &m_sys_branches_muon_calib_trig_2015_eff_data);
    m_sys_mu_trig_2018_eff_mc = get_val_for_systematics(m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
                                                        &m_sys_branches_muon_trig_2018_eff_mc, &m_sys_branches_muon_calib_trig_2018_eff_mc);
    m_sys_mu_trig_2018_eff_data = get_val_for_systematics(m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
                                                          &m_sys_branches_muon_trig_2018_eff_data, &m_sys_branches_muon_calib_trig_2018_eff_data);

    // read jets
    m_sys_jet_pt = get_val_for_systematics(m_AnalysisJets_pt_NOSYS, &m_sys_branches_jet_pt);
    m_sys_jet_selected = get_val_for_systematics(m_AnalysisJets_jet_selected_NOSYS, &m_sys_branches_jet_selected, &m_sys_branches_jet_selected_other_sys);
}

void ZCharmLoop::get_tight_electrons()
{
    m_electrons_sys.clear();
    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_electrons_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++)
        {
            // nominal
            if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_el_selected.at(j)->at(i)) &&
                (m_sys_el_selected_other.at(j)->at(i)) &&
                (m_sys_el_iso.at(j)->at(i)) &&
                (m_AnalysisElectrons_likelihood_Tight->at(i)) &&
                ((m_is_mc && m_truth_match && electron_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc)))
            {
                // TODO: fix ntuples to have different SF for calibration sys!
                float sf = 1.;
                if (m_is_mc)
                {
                    sf *= m_sys_el_reco_eff.at(j)->at(i) *
                          m_sys_el_id_eff.at(j)->at(i) *
                          m_sys_el_iso_eff.at(j)->at(i);
                }

                std::pair<unsigned int, float> pair(i, sf);
                m_electrons_sys.at(j).push_back(pair);
            }
        }
    }
}

void ZCharmLoop::get_tight_muons()
{
    m_muons_sys.clear();
    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_muons_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++)
        {
            // nominal
            if ((m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_mu_selected.at(j)->at(i)) &&
                (m_sys_mu_selected_other.at(j)->at(i)) &&
                (m_sys_mu_quality.at(j)->at(i)) &&
                (m_sys_mu_iso.at(j)->at(i)) &&
                ((m_is_mc && m_truth_match && muon_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc)))
            {
                // TODO: fix ntuples to have different SF for calibration sys!
                float sf = 1.;
                if (m_is_mc)
                {
                    sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                          m_sys_mu_ttva_eff.at(j)->at(i) *
                          m_sys_mu_iso_eff.at(j)->at(i);
                }

                std::pair<unsigned int, float> pair(i, sf);
                m_muons_sys.at(j).push_back(pair);
            }
        }
    }
}

void ZCharmLoop::get_jets()
{
    m_jets_sys.clear();
    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_jets_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisJets_pt_NOSYS->size(); i++)
        {
            // nominal
            // if (((fabs(m_AnalysisJets_eta->at(i)) < 2.5 && (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 25.)) ||
            //      (fabs(m_AnalysisJets_eta->at(i)) >= 2.5 && fabs(m_AnalysisJets_eta->at(i)) < 5.0 && (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 30.))) &&
            //     (m_sys_jet_selected.at(j)->at(i)))
            // {
            if (((fabs(m_AnalysisJets_eta->at(i)) < 2.4 && (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.)) ||
                 (fabs(m_AnalysisJets_eta->at(i)) >= 2.4 && fabs(m_AnalysisJets_eta->at(i)) <= 5.0 && (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.))) &&
                (m_sys_jet_selected.at(j)->at(i)))
            {
                m_jets_sys.at(j).push_back(i);
            }
        }
    }
}

void ZCharmLoop::jet_selection_for_sys(unsigned int i)
{
    auto jets = m_jets_sys.at(i);
    for (unsigned int j = 0; j < jets.size(); j++)
    {
        m_sys_njets.at(i) += 1;
        if (m_AnalysisJets_ftag_select_DL1_FixedCutBEff_70->at(j))
        {
            m_sys_nbjets.at(i) += 1;
        }
    }
}

bool ZCharmLoop::pass_electron_trigger()
{
    if (m_EventInfo_runNumber < 290000)
    {
        return m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH ||
               m_EventInfo_trigPassed_HLT_e60_lhmedium ||
               m_EventInfo_trigPassed_HLT_e120_lhloose;
    }
    else
    {
        return m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose ||
               m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 ||
               m_EventInfo_trigPassed_HLT_e140_lhloose_nod0;
    }
}

bool ZCharmLoop::pass_muon_trigger()
{
    if (m_EventInfo_runNumber < 290000)
    {
        return m_EventInfo_trigPassed_HLT_mu50 ||
               m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15;
    }
    else
    {
        return m_EventInfo_trigPassed_HLT_mu50 ||
               m_EventInfo_trigPassed_HLT_mu26_ivarmedium;
    }
}

bool ZCharmLoop::electron_is_trigger_matched(unsigned int i, unsigned int j)
{
    if (m_EventInfo_runNumber < 290000)
    {
        return ((m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH && m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH->at(i)) ||
                (m_EventInfo_trigPassed_HLT_e60_lhmedium && m_AnalysisElectrons_matched_HLT_e60_lhmedium->at(i)) ||
                (m_EventInfo_trigPassed_HLT_e120_lhloose && m_AnalysisElectrons_matched_HLT_e120_lhloose->at(i))) ||
               ((m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH && m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH->at(j)) ||
                (m_EventInfo_trigPassed_HLT_e60_lhmedium && m_AnalysisElectrons_matched_HLT_e60_lhmedium->at(j)) ||
                (m_EventInfo_trigPassed_HLT_e120_lhloose && m_AnalysisElectrons_matched_HLT_e120_lhloose->at(j)));
    }
    else
    {
        return ((m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose && m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose->at(i)) ||
                (m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 && m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0->at(i)) ||
                (m_EventInfo_trigPassed_HLT_e140_lhloose_nod0 && m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0->at(i))) ||
               ((m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose && m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose->at(j)) ||
                (m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 && m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0->at(j)) ||
                (m_EventInfo_trigPassed_HLT_e140_lhloose_nod0 && m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0->at(j)));
    }
}

bool ZCharmLoop::muon_is_trigger_matched(unsigned int i, unsigned int j)
{
    if (m_EventInfo_runNumber < 290000)
    {
        return ((m_EventInfo_trigPassed_HLT_mu50 && m_AnalysisMuons_matched_HLT_mu50->at(i)) ||
                (m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15 && m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15->at(i))) ||
               ((m_EventInfo_trigPassed_HLT_mu50 && m_AnalysisMuons_matched_HLT_mu50->at(j)) ||
                (m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15 && m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15->at(j)));
    }
    else
    {
        return ((m_EventInfo_trigPassed_HLT_mu50 && m_AnalysisMuons_matched_HLT_mu50->at(i)) ||
                (m_EventInfo_trigPassed_HLT_mu26_ivarmedium && m_AnalysisMuons_matched_HLT_mu26_ivarmedium->at(i))) ||
               ((m_EventInfo_trigPassed_HLT_mu50 && m_AnalysisMuons_matched_HLT_mu50->at(j)) ||
                (m_EventInfo_trigPassed_HLT_mu26_ivarmedium && m_AnalysisMuons_matched_HLT_mu26_ivarmedium->at(j)));
    }
}

float ZCharmLoop::get_electron_trigger_weight(unsigned int sys_index, unsigned int i, unsigned int j)
{
    float effData1 = m_sys_el_trig_eff.at(sys_index)->at(i);
    float effData2 = m_sys_el_trig_eff.at(sys_index)->at(j);
    float effMC1 = (1. / effData1) * m_sys_el_trig_sf.at(sys_index)->at(i);
    float effMC2 = (1. / effData2) * m_sys_el_trig_sf.at(sys_index)->at(j);

    float PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
    float PData = effData1 * effData2 + effData1 * (1 - effData2) + (1 - effData1) * effData2;

    if (PMC > 0)
    {
        return PData / PMC;
    }
    else
    {
        return 0;
    }
}

float ZCharmLoop::get_muon_trigger_weight(unsigned int sys_index, unsigned int i, unsigned int j)
{
    if (m_EventInfo_runNumber < 290000)
    {
        float effMC1 = m_sys_mu_trig_2015_eff_mc.at(sys_index)->at(i);
        float effMC2 = m_sys_mu_trig_2015_eff_mc.at(sys_index)->at(j);
        float effData1 = m_sys_mu_trig_2015_eff_data.at(sys_index)->at(i);
        float effData2 = m_sys_mu_trig_2015_eff_data.at(sys_index)->at(j);

        float PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
        float PData = effData1 * effData2 + effData1 * (1 - effData2) + (1 - effData1) * effData2;

        if (PMC > 0)
        {
            return PData / PMC;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        float effMC1 = m_sys_mu_trig_2018_eff_mc.at(sys_index)->at(i);
        float effMC2 = m_sys_mu_trig_2018_eff_mc.at(sys_index)->at(j);
        float effData1 = m_sys_mu_trig_2018_eff_data.at(sys_index)->at(i);
        float effData2 = m_sys_mu_trig_2018_eff_data.at(sys_index)->at(j);

        float PMC = effMC1 * effMC2 + effMC1 * (1 - effMC2) + (1 - effMC1) * effMC2;
        float PData = effData1 * effData2 + effData1 * (1 - effData2) + (1 - effData1) * effData2;

        if (PMC > 0)
        {
            return PData / PMC;
        }
        else
        {
            return 0;
        }
    }
}

bool ZCharmLoop::electron_is_prompt(unsigned int i)
{
    // prompt
    if (m_AnalysisElectrons_truthType->at(i) == 2)
        return true;
    // conversions
    else if (m_AnalysisElectrons_truthType->at(i) == 4 &&
             fabs(m_AnalysisElectrons_firstEgMotherPdgId->at(i)) == 11)
    {
        if (m_AnalysisElectrons_truthOrigin->at(i) == 5)
            return true;
        else if (m_AnalysisElectrons_truthOrigin->at(i) == 7 &&
                 m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 2 &&
                 Charm::is_in(m_AnalysisElectrons_firstEgMotherTruthOrigin->at(i),
                              {10, 12, 13, 14, 43}))
            return true;
    }
    // fsr
    else if (m_AnalysisElectrons_truthType->at(i) == 4 &&
             Charm::is_in(m_AnalysisElectrons_truthOrigin->at(i), {5, 7}) &&
             m_AnalysisElectrons_firstEgMotherTruthOrigin->at(i) == 40)
        return true;
    else if (m_AnalysisElectrons_truthType->at(i) == 15 &&
             m_AnalysisElectrons_truthOrigin->at(i) == 40)
        return true;

    // end
    return false;
}

bool ZCharmLoop::muon_is_prompt(unsigned int i)
{
    return ((m_AnalysisMuons_truthType->at(i) == 6) &&
            Charm::is_in(m_AnalysisMuons_truthOrigin->at(i),
                         {10, 12, 13, 14, 43}));
}

bool ZCharmLoop::preselection()
{
    set_channel("inclusive");
    increment_cutflow(m_channel, 1);

    // sys branches for leptons
    read_sys_branches();

    // electron selection
    get_tight_electrons();

    // muon selection
    get_tight_muons();

    // jets
    get_jets();

    // clear all before running over systematics
    m_sys_Z_m = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_Z_pt = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep1_pt = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep1_eta = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep1_phi = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep2_pt = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep2_eta = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep2_phi = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_njets = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_nbjets = std::vector<float>(m_systematics.size() + 1, 0.);
    bool event_pass = false;

    for (unsigned int i = 0; i < m_systematics.size() + 1; i++)
    {
        // bad muon veto
        for (unsigned int j = 0; j < m_muons_sys.at(i).size(); j++)
        {
            if (m_AnalysisMuons_is_bad->at(m_muons_sys.at(i).at(j).first))
            {
                continue;
            }
        }

        // jet multiplicity
        jet_selection_for_sys(i);

        bool nominal = i == 0;
        unsigned int sys_index = i - 1;
        auto electrons = m_electrons_sys.at(i);
        auto muons = m_muons_sys.at(i);
        TLorentzVector lep1;
        TLorentzVector lep2;

        std::string channel_string = "";

        // require exactly two same-flavor leptons
        if (!((electrons.size() == 2 && muons.size() == 0) ||
              (muons.size() == 2 && electrons.size() == 0)))
        {
            continue;
        }

        if (electrons.size() == 2)
        {
            // electron index
            unsigned int index1 = electrons.at(0).first;
            unsigned int index2 = electrons.at(1).first;

            // order by pT
            if (m_sys_el_pt.at(i)->at(index2) > m_sys_el_pt.at(i)->at(index1))
            {
                unsigned int temp = index1;
                index1 = index2;
                index2 = temp;
            }

            // charge
            if (m_AnalysisElectrons_charge->at(index1) * m_AnalysisElectrons_charge->at(index2) > 0)
            {
                continue;
            }

            // pass electron trigger
            if (!pass_electron_trigger())
            {
                continue;
            }

            // single electron trigger
            if (!electron_is_trigger_matched(index1, index2))
            {
                continue;
            }

            // el channel
            channel_string = "el";

            // fill variables
            lep1.SetPtEtaPhiM(m_sys_el_pt.at(i)->at(index1) * Charm::GeV,
                              m_AnalysisElectrons_eta->at(index1),
                              m_AnalysisElectrons_phi->at(index1),
                              Charm::ELECTRON_MASS * Charm::GeV);
            lep2.SetPtEtaPhiM(m_sys_el_pt.at(i)->at(index2) * Charm::GeV,
                              m_AnalysisElectrons_eta->at(index2),
                              m_AnalysisElectrons_phi->at(index2),
                              Charm::ELECTRON_MASS * Charm::GeV);
            m_sys_lep1_pt.at(i) = m_sys_el_pt.at(i)->at(index1) * Charm::GeV;
            m_sys_lep1_eta.at(i) = m_AnalysisElectrons_eta->at(index1);
            m_sys_lep1_phi.at(i) = m_AnalysisElectrons_phi->at(index1);
            m_sys_lep2_pt.at(i) = m_sys_el_pt.at(i)->at(index2) * Charm::GeV;
            m_sys_lep2_eta.at(i) = m_AnalysisElectrons_eta->at(index2);
            m_sys_lep2_phi.at(i) = m_AnalysisElectrons_phi->at(index2);
            if (nominal)
            {
                m_Z_eta = (lep1 + lep2).Eta();
                m_Z_phi = (lep1 + lep2).Phi();
                m_lep_dPhi = lep1.DeltaPhi(lep2);
                m_lep_dR = lep1.DeltaR(lep2);
                m_lep1_d0 = m_AnalysisElectrons_d0->at(index1);
                m_lep1_d0sig = m_AnalysisElectrons_d0sig->at(index1);
                m_lep1_charge = m_AnalysisElectrons_charge->at(index1);
                m_lep2_d0 = m_AnalysisElectrons_d0->at(index2);
                m_lep2_d0sig = m_AnalysisElectrons_d0sig->at(index2);
                m_lep2_charge = m_AnalysisElectrons_charge->at(index2);
                if (m_do_extra_histograms)
                {
                    m_lep1_z0sinTheta = m_AnalysisElectrons_z0sinTheta->at(index1);
                    m_lep1_topoetcone20 = m_AnalysisElectrons_topoetcone20->at(index1) * Charm::GeV;
                    m_lep1_ptvarcone20_TightTTVA_pt1000 = m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(index1) * Charm::GeV;
                    m_lep1_ptvarcone30_TightTTVA_pt1000 = m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000->at(index1) * Charm::GeV;
                    m_lep1_topoetcone20_over_pt = m_lep1_topoetcone20 / lep1.Pt();
                    m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt = m_lep1_ptvarcone20_TightTTVA_pt1000 / lep1.Pt();
                    m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt = m_lep1_ptvarcone30_TightTTVA_pt1000 / lep1.Pt();
                    m_lep2_z0sinTheta = m_AnalysisElectrons_z0sinTheta->at(index2);
                    m_lep2_topoetcone20 = m_AnalysisElectrons_topoetcone20->at(index2) * Charm::GeV;
                    m_lep2_ptvarcone20_TightTTVA_pt1000 = m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(index2) * Charm::GeV;
                    m_lep2_ptvarcone30_TightTTVA_pt1000 = m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000->at(index2) * Charm::GeV;
                    m_lep2_topoetcone20_over_pt = m_lep2_topoetcone20 / lep2.Pt();
                    m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt = m_lep2_ptvarcone20_TightTTVA_pt1000 / lep2.Pt();
                    m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt = m_lep2_ptvarcone30_TightTTVA_pt1000 / lep2.Pt();
                    m_lep1_ptvarcone30_plus_neflowisol20 = -999;
                    m_lep2_ptvarcone30_plus_neflowisol20 = -999;
                }
            }
            // efficiency weight
            if (m_is_mc)
            {
                multiply_event_weight(m_electrons_sys.at(i).at(0).second * m_electrons_sys.at(i).at(1).second * get_electron_trigger_weight(i, index1, index2), i);
            }
        }
        else if (muons.size() == 2)
        {
            // muon index
            unsigned int index1 = muons.at(0).first;
            unsigned int index2 = muons.at(1).first;

            // order by pT
            if (m_sys_mu_pt.at(i)->at(index2) > m_sys_mu_pt.at(i)->at(index1))
            {
                unsigned int temp = index1;
                index1 = index2;
                index2 = temp;
            }

            // charge
            if (m_AnalysisMuons_charge->at(index1) * m_AnalysisMuons_charge->at(index2) > 0)
            {
                continue;
            }

            // pass muon trigger
            if (!pass_muon_trigger())
            {
                continue;
            }

            // single muon trigger
            if (!muon_is_trigger_matched(index1, index2))
            {
                continue;
            }

            // mu channel
            channel_string = "mu";

            // fill variables
            lep1.SetPtEtaPhiM(m_sys_mu_pt.at(i)->at(index1) * Charm::GeV,
                              m_AnalysisMuons_eta->at(index1),
                              m_AnalysisMuons_phi->at(index1),
                              Charm::MUON_MASS * Charm::GeV);
            lep2.SetPtEtaPhiM(m_sys_mu_pt.at(i)->at(index2) * Charm::GeV,
                              m_AnalysisMuons_eta->at(index2),
                              m_AnalysisMuons_phi->at(index2),
                              Charm::MUON_MASS * Charm::GeV);
            m_sys_lep1_pt.at(i) = m_sys_mu_pt.at(i)->at(index1) * Charm::GeV;
            m_sys_lep1_eta.at(i) = m_AnalysisMuons_eta->at(index1);
            m_sys_lep1_phi.at(i) = m_AnalysisMuons_phi->at(index1);
            m_sys_lep2_pt.at(i) = m_sys_mu_pt.at(i)->at(index2) * Charm::GeV;
            m_sys_lep2_eta.at(i) = m_AnalysisMuons_eta->at(index2);
            m_sys_lep2_phi.at(i) = m_AnalysisMuons_phi->at(index2);
            if (nominal)
            {
                m_Z_eta = (lep1 + lep2).Eta();
                m_Z_phi = (lep1 + lep2).Phi();
                m_lep_dPhi = lep1.DeltaPhi(lep2);
                m_lep_dR = lep1.DeltaR(lep2);
                m_lep1_d0 = m_AnalysisMuons_d0->at(index1);
                m_lep1_d0sig = m_AnalysisMuons_d0sig->at(index1);
                m_lep1_charge = m_AnalysisMuons_charge->at(index1);
                m_lep2_d0 = m_AnalysisMuons_d0->at(index2);
                m_lep2_d0sig = m_AnalysisMuons_d0sig->at(index2);
                m_lep2_charge = m_AnalysisMuons_charge->at(index2);
                if (m_do_extra_histograms)
                {
                    m_lep1_z0sinTheta = m_AnalysisMuons_z0sinTheta->at(index1);
                    m_lep1_topoetcone20 = m_AnalysisMuons_topoetcone20->at(index1) * Charm::GeV;
                    m_lep1_ptvarcone30_TightTTVA_pt1000 = m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000->at(index1) * Charm::GeV;
                    m_lep1_topoetcone20_over_pt = m_lep1_topoetcone20 / lep1.Pt();
                    m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt = m_lep1_ptvarcone30_TightTTVA_pt1000 / lep1.Pt();
                    m_lep1_ptvarcone30_plus_neflowisol20 = (m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(index1) + 0.4 * m_AnalysisMuons_neflowisol20->at(index1)) / m_sys_mu_pt.at(i)->at(index1);
                    m_lep2_z0sinTheta = m_AnalysisMuons_z0sinTheta->at(index2);
                    m_lep2_topoetcone20 = m_AnalysisMuons_topoetcone20->at(index2) * Charm::GeV;
                    m_lep2_ptvarcone30_TightTTVA_pt1000 = m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000->at(index2) * Charm::GeV;
                    m_lep2_topoetcone20_over_pt = m_lep2_topoetcone20 / lep2.Pt();
                    m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt = m_lep2_ptvarcone30_TightTTVA_pt1000 / lep2.Pt();
                    m_lep2_ptvarcone30_plus_neflowisol20 = (m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(index2) + 0.4 * m_AnalysisMuons_neflowisol20->at(index2)) / m_sys_mu_pt.at(i)->at(index2);
                    m_lep1_ptvarcone20_TightTTVA_pt1000 = -999;
                    m_lep2_ptvarcone20_TightTTVA_pt1000 = -999;
                    m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt = -999;
                    m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt = -999;
                }
            }
            // efficiency weight
            if (m_is_mc)
            {
                multiply_event_weight(muons.at(0).second * muons.at(1).second * get_muon_trigger_weight(i, index1, index2), i);
            }
        }
        else
        {
            std::cout << "wrong size" << std::endl;
            return false;
        }

        // set channel
        if (m_split_by_period && channel_string != "")
        {
            channel_string = get_period_string() + "_" + channel_string;
        }

        // Z boson
        TLorentzVector Zboson = lep1 + lep2;
        if (Zboson.M() >= 70 && Zboson.M() <= 110)
        {
            m_channel_sys.at(i) = channel_string;
            event_pass = true;
        }
        m_sys_Z_m.at(i) = Zboson.M();
        m_sys_Z_pt.at(i) = Zboson.Pt();
    }

    // for (unsigned int i = 0; i < m_systematics.size() + 1; i++) {
    //     std::cout << i << " channel " << m_channel_sys.at(i) << std::endl;
    // }

    if (!event_pass)
    {
        return false;
    }

    // event weight
    if (m_is_mc)
    {
        // mc event weight, initial sum of weights, x-sec, lumi weight
        multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_branches_GEN);

        // jvt weight and systematics
        multiply_nominal_and_sys_weight(m_EventInfo_jvt_effSF_NOSYS, &m_sys_branches_JVT);

        // pileup weight and systematics
        multiply_nominal_and_sys_weight(m_EventInfo_PileupWeight_NOSYS, &m_sys_branches_PU);

        // FTAG systematics
        multiply_nominal_and_sys_weight(m_EventInfo_ftag_effSF_DL1_FixedCutBEff_70_NOSYS, &m_sys_branches_FTAG);
    }

    // pt(V) reweight
    if (m_is_mc && m_do_pt_v_rw)
    {
        float ptv_weight = 1.0;
        if (m_mc_gen == "powheg" || m_mc_gen == "sherpa")
        {
            ptv_weight = calculate_ptv_weight();
        }
        multiply_event_weights(ptv_weight);
    }

    // jet multiplicity
    m_n_jets = 0;
    m_n_bjets = 0;
    for (unsigned int i = 0; i < m_AnalysisJets_ftag_select_DL1_FixedCutBEff_70->size(); i++)
    {
        m_n_jets += 1;
        if (m_AnalysisJets_ftag_select_DL1_FixedCutBEff_70->at(i))
        {
            m_n_bjets += 1;
        }
    }

    m_sys_met = get_val_for_systematics(m_METInfo_NOSYS_MET, &m_sys_branches_MET_met, Charm::GeV);

    return true;
}

int ZCharmLoop::execute()
{
    // fill histograms
    fill_histograms();

    return 1;
}

float ZCharmLoop::calculate_ptv_weight()
{
    std::vector<std::pair<int, unsigned int>> truth_leptons;
    for (unsigned int i = 0; i < m_TruthLeptons_barcode->size(); i++)
    {
        std::pair<int, unsigned int> pair(m_TruthLeptons_barcode->at(i), i);
        truth_leptons.push_back(pair);
    }
    std::sort(truth_leptons.begin(), truth_leptons.end());
    if (!(truth_leptons.size() >= 2))
    {
        std::cout << "Warning:: less than two truth leptons in dsid " << m_dataset_id << std::endl;
        return 1.0;
    }

    // truth leptons
    unsigned int index1 = truth_leptons.at(0).second;
    unsigned int index2 = truth_leptons.at(1).second;
    TLorentzVector lep1;
    TLorentzVector lep2;
    lep1.SetPtEtaPhiM(m_TruthLeptons_pt->at(index1) * Charm::GeV,
                      m_TruthLeptons_eta->at(index1),
                      m_TruthLeptons_phi->at(index1),
                      m_TruthLeptons_m->at(index1) * Charm::GeV);
    lep2.SetPtEtaPhiM(m_TruthLeptons_pt->at(index2) * Charm::GeV,
                      m_TruthLeptons_eta->at(index2),
                      m_TruthLeptons_phi->at(index2),
                      m_TruthLeptons_m->at(index2) * Charm::GeV);

    // truth V
    TLorentzVector truth_v = lep1 + lep2;

    // rw histo
    float w = get_ptv_weight(truth_v.Pt());

    return w;
}

std::string ZCharmLoop::get_period_string()
{
    return get_period(m_EventInfo_runNumber);
}

void ZCharmLoop::fill_histograms()
{
    add_fill_histogram_sys_fast("lep1_pt", m_sys_lep1_pt,
                                400, 0, 400, true);
    add_fill_histogram_sys_fast("lep1_eta", m_sys_lep1_eta,
                                100, -3.0, 3.0, true);
    add_fill_histogram_sys_fast("lep2_pt", m_sys_lep2_pt,
                                400, 0, 400, true);
    add_fill_histogram_sys_fast("lep2_eta", m_sys_lep2_eta,
                                100, -3.0, 3.0, true);
    add_fill_histogram_sys_fast("Z_m", m_sys_Z_m,
                                100, 60, 120, true);
    add_fill_histogram_sys_fast("Z_pt", m_sys_Z_pt,
                                400, 0, 400, true);
    add_fill_histogram_sys_fast("pileup", m_EventInfo_correctedScaled_averageInteractionsPerCrossing,
                                80, 0, 80, true);
    add_fill_histogram_sys_fast("met_met", m_sys_met,
                                400, 0, 400, true);
    add_fill_histogram_sys_fast("njets", m_sys_njets,
                                20, -0.5, 19.5, true);
    add_fill_histogram_sys_fast("nbjets", m_sys_nbjets,
                                20, -0.5, 19.5, true);
    add_fill_histogram_sys_fast("lep1_phi", m_sys_lep1_phi,
                                100, -M_PI, M_PI);
    add_fill_histogram_sys_fast("lep2_phi", m_sys_lep2_phi,
                                100, -M_PI, M_PI);
    add_fill_histogram_sys_fast("lep_dphi", m_lep_dPhi,
                                100, -M_PI, M_PI);
    add_fill_histogram_sys_fast("lep_dR", m_lep_dR,
                                100, 0, 6);
    add_fill_histogram_sys_fast("Z_eta", m_Z_eta,
                                100, -8.0, 8.0);
    add_fill_histogram_sys_fast("Z_phi", m_Z_phi,
                                100, -M_PI, M_PI);
    add_fill_histogram_sys_fast("run_number", m_EventInfo_runNumber,
                                150, 250000, 400000);
    if (m_do_extra_histograms)
    {
        add_fill_histogram_sys_fast("lep1_d0", m_lep1_d0 * Charm::mum,
                                    400, -200, 200);
        add_fill_histogram_sys_fast("lep1_d0_fixed", rescaled_d0(m_lep1_d0, m_EventInfo_runNumber, m_is_mc) * Charm::mum,
                                    400, -200, 200);
        add_fill_histogram_sys_fast("lep1_d0sig", m_lep1_d0sig,
                                    100, -6.0, 6.0);
        add_fill_histogram_sys_fast("lep1_d0sig_fixed", m_lep1_d0sig / m_lep1_d0 * rescaled_d0(m_lep1_d0, m_EventInfo_runNumber, m_is_mc),
                                    100, -6.0, 6.0);
        add_fill_histogram_sys_fast("lep1_z0sinTheta", m_lep1_z0sinTheta,
                                    100, -0.6, 0.6);
        add_fill_histogram_sys_fast("lep1_topoetcone20", m_lep1_topoetcone20,
                                    100, 0.0, 10.0);
        add_fill_histogram_sys_fast("lep1_topoetcone20_over_pt", m_lep1_topoetcone20_over_pt,
                                    100, 0.0, 0.16);
        add_fill_histogram_sys_fast("lep1_ptvarcone30_TightTTVA_pt1000", m_lep1_ptvarcone30_TightTTVA_pt1000,
                                    100, 0.0, 10.0);
        add_fill_histogram_sys_fast("lep1_ptvarcone30_TightTTVA_pt1000_over_pt", m_lep1_ptvarcone30_TightTTVA_pt1000_over_pt,
                                    100, 0.0, 0.1);
        add_fill_histogram_sys_fast("lep2_d0", m_lep2_d0 * Charm::mum,
                                    400, -200, 200);
        add_fill_histogram_sys_fast("lep2_d0_fixed", rescaled_d0(m_lep2_d0, m_EventInfo_runNumber, m_is_mc) * Charm::mum,
                                    400, -200, 200);
        add_fill_histogram_sys_fast("lep2_d0sig", m_lep2_d0sig,
                                    100, -6.0, 6.0);
        add_fill_histogram_sys_fast("lep2_d0sig_fixed", m_lep2_d0sig / m_lep2_d0 * rescaled_d0(m_lep2_d0, m_EventInfo_runNumber, m_is_mc),
                                    100, -6.0, 6.0);
        add_fill_histogram_sys_fast("lep2_z0sinTheta", m_lep2_z0sinTheta,
                                    100, -0.6, 0.6);
        add_fill_histogram_sys_fast("lep2_topoetcone20", m_lep2_topoetcone20,
                                    100, 0.0, 10.0);
        add_fill_histogram_sys_fast("lep2_topoetcone20_over_pt", m_lep2_topoetcone20_over_pt,
                                    100, 0.0, 0.16);
        add_fill_histogram_sys_fast("lep2_ptvarcone30_TightTTVA_pt1000", m_lep2_ptvarcone30_TightTTVA_pt1000,
                                    100, 0.0, 10.0);
        add_fill_histogram_sys_fast("lep2_ptvarcone30_TightTTVA_pt1000_over_pt", m_lep2_ptvarcone30_TightTTVA_pt1000_over_pt,
                                    100, 0.0, 0.1);
        add_fill_histogram_sys_fast("CharmEventInfo_PV_Z", m_CharmEventInfo_PV_Z * Charm::mm,
                                    100, -300, 300);
        add_fill_histogram_sys_fast("beamPosX_minus_PVX", (m_CharmEventInfo_beamPosX - m_CharmEventInfo_PV_X) * Charm::mum,
                                    100, -80, 80);
        add_fill_histogram_sys_fast("beamPosY_minus_PVY", (m_CharmEventInfo_beamPosY - m_CharmEventInfo_PV_Y) * Charm::mum,
                                    100, -80, 80);
        // channel specific
        add_fill_histogram_sys_fast("lep1_ptvarcone20_TightTTVA_pt1000", m_lep1_ptvarcone20_TightTTVA_pt1000,
                                    100, 0.0, 10.0);
        add_fill_histogram_sys_fast("lep1_ptvarcone20_TightTTVA_pt1000_over_pt", m_lep1_ptvarcone20_TightTTVA_pt1000_over_pt,
                                    100, 0.0, 0.1);
        add_fill_histogram_sys_fast("lep2_ptvarcone20_TightTTVA_pt1000", m_lep2_ptvarcone20_TightTTVA_pt1000,
                                    100, 0.0, 10.0);
        add_fill_histogram_sys_fast("lep2_ptvarcone20_TightTTVA_pt1000_over_pt", m_lep2_ptvarcone20_TightTTVA_pt1000_over_pt,
                                    100, 0.0, 0.1);
        add_fill_histogram_sys_fast("lep1_ptvarcone30_plus_neflowisol20", m_lep1_ptvarcone30_plus_neflowisol20,
                                    100, 0.0, 0.1);
        add_fill_histogram_sys_fast("lep2_ptvarcone30_plus_neflowisol20", m_lep2_ptvarcone30_plus_neflowisol20,
                                    100, 0.0, 0.1);
    }
    m_histograms_created = true;
}

void ZCharmLoop::connect_branches()
{
    // event info
    add_preselection_branch("METInfo_NOSYS_MET", &m_METInfo_NOSYS_MET);
    // add_preselection_branch("METInfo_NOSYS_METPhi", &m_METInfo_NOSYS_METPhi);
    add_preselection_branch("CharmEventInfo_PV_Z", &m_CharmEventInfo_PV_Z);
    add_preselection_branch("CharmEventInfo_beamPosX", &m_CharmEventInfo_beamPosX);
    add_preselection_branch("CharmEventInfo_PV_X", &m_CharmEventInfo_PV_X);
    add_preselection_branch("CharmEventInfo_beamPosY", &m_CharmEventInfo_beamPosY);
    add_preselection_branch("CharmEventInfo_PV_Y", &m_CharmEventInfo_PV_Y);
    add_preselection_branch("EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH", &m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH);
    add_preselection_branch("EventInfo_trigPassed_HLT_e60_lhmedium", &m_EventInfo_trigPassed_HLT_e60_lhmedium);
    add_preselection_branch("EventInfo_trigPassed_HLT_e120_lhloose", &m_EventInfo_trigPassed_HLT_e120_lhloose);
    add_preselection_branch("EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose", &m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose);
    add_preselection_branch("EventInfo_trigPassed_HLT_e60_lhmedium_nod0", &m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0);
    add_preselection_branch("EventInfo_trigPassed_HLT_e140_lhloose_nod0", &m_EventInfo_trigPassed_HLT_e140_lhloose_nod0);
    add_preselection_branch("EventInfo_trigPassed_HLT_mu20_iloose_L1MU15", &m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15);
    add_preselection_branch("EventInfo_trigPassed_HLT_mu26_ivarmedium", &m_EventInfo_trigPassed_HLT_mu26_ivarmedium);
    add_preselection_branch("EventInfo_trigPassed_HLT_mu50", &m_EventInfo_trigPassed_HLT_mu50);
    add_preselection_branch(Charm::get_run_number_string(m_is_mc), &m_EventInfo_runNumber);
    add_preselection_branch(Charm::get_pileup_string(m_is_mc, m_mc_period, m_data_period), &m_EventInfo_correctedScaled_averageInteractionsPerCrossing);

    // muons
    add_preselection_branch("AnalysisMuons_pt_NOSYS", &m_AnalysisMuons_pt_NOSYS);
    add_preselection_branch("AnalysisMuons_eta", &m_AnalysisMuons_eta);
    add_preselection_branch("AnalysisMuons_phi", &m_AnalysisMuons_phi);
    add_preselection_branch("AnalysisMuons_charge", &m_AnalysisMuons_charge);
    add_preselection_branch("AnalysisMuons_mu_selected_NOSYS", &m_AnalysisMuons_mu_selected_NOSYS);
    add_preselection_branch("AnalysisMuons_isQuality_Tight_NOSYS", &m_AnalysisMuons_isQuality_Tight_NOSYS);
    add_preselection_branch("AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS", &m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS);
    add_preselection_branch("AnalysisMuons_matched_HLT_mu20_iloose_L1MU15", &m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15);
    add_preselection_branch("AnalysisMuons_matched_HLT_mu26_ivarmedium", &m_AnalysisMuons_matched_HLT_mu26_ivarmedium);
    add_preselection_branch("AnalysisMuons_matched_HLT_mu50", &m_AnalysisMuons_matched_HLT_mu50);
    add_preselection_branch("AnalysisMuons_d0", &m_AnalysisMuons_d0);
    add_preselection_branch("AnalysisMuons_d0sig", &m_AnalysisMuons_d0sig);
    add_preselection_branch("AnalysisMuons_is_bad", &m_AnalysisMuons_is_bad);
    if (m_do_extra_histograms)
    {
        add_preselection_branch("AnalysisMuons_z0sinTheta", &m_AnalysisMuons_z0sinTheta);
        add_preselection_branch("AnalysisMuons_topoetcone20", &m_AnalysisMuons_topoetcone20);
        add_preselection_branch("AnalysisMuons_ptvarcone30_TightTTVA_pt1000", &m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000);
        add_preselection_branch("AnalysisMuons_ptvarcone30_TightTTVA_pt500", &m_AnalysisMuons_ptvarcone30_TightTTVA_pt500);
        add_preselection_branch("AnalysisMuons_neflowisol20", &m_AnalysisMuons_neflowisol20);
    }

    // electrons
    add_preselection_branch("AnalysisElectrons_pt_NOSYS", &m_AnalysisElectrons_pt_NOSYS);
    add_preselection_branch("AnalysisElectrons_eta", &m_AnalysisElectrons_eta);
    add_preselection_branch("AnalysisElectrons_phi", &m_AnalysisElectrons_phi);
    add_preselection_branch("AnalysisElectrons_charge", &m_AnalysisElectrons_charge);
    add_preselection_branch("AnalysisElectrons_likelihood_Tight", &m_AnalysisElectrons_likelihood_Tight);
    add_preselection_branch("AnalysisElectrons_el_selected_NOSYS", &m_AnalysisElectrons_el_selected_NOSYS);
    add_preselection_branch("AnalysisElectrons_isIsolated_FCTight_NOSYS", &m_AnalysisElectrons_isIsolated_FCTight_NOSYS);
    add_preselection_branch("AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH", &m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH);
    add_preselection_branch("AnalysisElectrons_matched_HLT_e60_lhmedium", &m_AnalysisElectrons_matched_HLT_e60_lhmedium);
    add_preselection_branch("AnalysisElectrons_matched_HLT_e120_lhloose", &m_AnalysisElectrons_matched_HLT_e120_lhloose);
    add_preselection_branch("AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose", &m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose);
    add_preselection_branch("AnalysisElectrons_matched_HLT_e60_lhmedium_nod0", &m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0);
    add_preselection_branch("AnalysisElectrons_matched_HLT_e140_lhloose_nod0", &m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0);
    add_preselection_branch("AnalysisElectrons_d0", &m_AnalysisElectrons_d0);
    add_preselection_branch("AnalysisElectrons_d0sig", &m_AnalysisElectrons_d0sig);
    if (m_do_extra_histograms)
    {
        add_preselection_branch("AnalysisElectrons_z0sinTheta", &m_AnalysisElectrons_z0sinTheta);
        add_preselection_branch("AnalysisElectrons_topoetcone20", &m_AnalysisElectrons_topoetcone20);
        add_preselection_branch("AnalysisElectrons_ptvarcone20_TightTTVA_pt1000", &m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000);
        add_preselection_branch("AnalysisElectrons_ptvarcone30_TightTTVA_pt1000", &m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000);
    }

    // jets
    add_preselection_branch("AnalysisJets_pt_NOSYS", &m_AnalysisJets_pt_NOSYS);
    add_preselection_branch("AnalysisJets_eta", &m_AnalysisJets_eta);
    add_preselection_branch("AnalysisJets_jet_selected_NOSYS", &m_AnalysisJets_jet_selected_NOSYS);
    add_preselection_branch("AnalysisJets_ftag_select_DL1_FixedCutBEff_70", &m_AnalysisJets_ftag_select_DL1_FixedCutBEff_70);
    if (m_is_mc)
    {
        add_preselection_branch("EventInfo_ftag_effSF_DL1_FixedCutBEff_70_NOSYS", &m_EventInfo_ftag_effSF_DL1_FixedCutBEff_70_NOSYS);
    }

    // mc only
    if (m_is_mc)
    {
        add_preselection_branch("EventInfo_generatorWeight_NOSYS", &m_EventInfo_generatorWeight_NOSYS);
        add_preselection_branch("EventInfo_PileupWeight_NOSYS", &m_EventInfo_PileupWeight_NOSYS);
        add_preselection_branch("EventInfo_jvt_effSF_NOSYS", &m_EventInfo_jvt_effSF_NOSYS);
        add_preselection_branch("AnalysisElectrons_effSF_NOSYS", &m_AnalysisElectrons_effSF_NOSYS);
        add_preselection_branch("AnalysisElectrons_effSF_ID_Tight_NOSYS", &m_AnalysisElectrons_effSF_ID_Tight_NOSYS);
        add_preselection_branch("AnalysisElectrons_effSF_Isol_Tight_FCTight_NOSYS", &m_AnalysisElectrons_effSF_Isol_Tight_FCTight_NOSYS);
        add_preselection_branch("AnalysisElectrons_effSF_Trig_Tight_FCTight_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &m_AnalysisElectrons_effSF_Trig_Tight_FCTight_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
        add_preselection_branch("AnalysisElectrons_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &m_AnalysisElectrons_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
        add_preselection_branch("AnalysisElectrons_truthType", &m_AnalysisElectrons_truthType);
        add_preselection_branch("AnalysisElectrons_truthOrigin", &m_AnalysisElectrons_truthOrigin);
        add_preselection_branch("AnalysisElectrons_firstEgMotherTruthType", &m_AnalysisElectrons_firstEgMotherTruthType);
        add_preselection_branch("AnalysisElectrons_firstEgMotherTruthOrigin", &m_AnalysisElectrons_firstEgMotherTruthOrigin);
        add_preselection_branch("AnalysisElectrons_firstEgMotherPdgId", &m_AnalysisElectrons_firstEgMotherPdgId);
        add_preselection_branch("AnalysisMuons_muon_effSF_Quality_Tight_NOSYS", &m_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS);
        add_preselection_branch("AnalysisMuons_muon_effSF_TTVA_NOSYS", &m_AnalysisMuons_muon_effSF_TTVA_NOSYS);
        add_preselection_branch("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS", &m_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS);
        add_preselection_branch("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
        add_preselection_branch("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
        add_preselection_branch("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
        add_preselection_branch("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
        add_preselection_branch("AnalysisMuons_truthType", &m_AnalysisMuons_truthType);
        add_preselection_branch("AnalysisMuons_truthOrigin", &m_AnalysisMuons_truthOrigin);

        // truth leptons
        if (m_do_pt_v_rw)
        {
            add_preselection_branch("TruthLeptons_pt", &m_TruthLeptons_pt);
            add_preselection_branch("TruthLeptons_m", &m_TruthLeptons_m);
            add_preselection_branch("TruthLeptons_eta", &m_TruthLeptons_eta);
            add_preselection_branch("TruthLeptons_phi", &m_TruthLeptons_phi);
            add_preselection_branch("TruthLeptons_barcode", &m_TruthLeptons_barcode);
            add_preselection_branch("TruthLeptons_pdgId", &m_TruthLeptons_pdgId);
        }

        // sys branches
        if (m_do_systematics)
        {
            initialize_sys_branches(&m_sys_branches_GEN, "EventInfo_generatorWeight_%s");
            initialize_sys_branches(&m_sys_branches_PU, "EventInfo_%s_PileupWeight");
            initialize_sys_branches(&m_sys_branches_JVT, "EventInfo_jvt_effSF_%s");
            initialize_sys_branches(&m_sys_branches_FTAG, "EventInfo_ftag_effSF_DL1_FixedCutBEff_70_%s");
            initialize_sys_branches(&m_sys_branches_MET_met, "METInfo_%s_MET");

            // electron sys branches
            initialize_sys_branches(&m_sys_branches_electron_pt, "AnalysisElectrons_%s_pt");
            initialize_sys_branches(&m_sys_branches_electron_selected, "AnalysisElectrons_%s_el_selected");
            initialize_sys_branches(&m_sys_branches_electron_selected_other_sys, "AnalysisElectrons_el_selected_%s");
            initialize_sys_branches(&m_sys_branches_electron_isIsolated_FCTight, "AnalysisElectrons_%s_isIsolated_FCTight");
            initialize_sys_branches(&m_sys_branches_electron_reco_eff, "AnalysisElectrons_effSF_%s");
            initialize_sys_branches(&m_sys_branches_electron_id_eff, "AnalysisElectrons_effSF_ID_Tight_%s");
            initialize_sys_branches(&m_sys_branches_electron_iso_eff, "AnalysisElectrons_effSF_Isol_Tight_FCTight_%s");
            initialize_sys_branches(&m_sys_branches_electron_trig_eff, "AnalysisElectrons_effSF_Trig_Tight_FCTight_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                                    {"EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
            initialize_sys_branches(&m_sys_branches_electron_trig_sf, "AnalysisElectrons_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                                    {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
            initialize_sys_branches(&m_sys_branches_electron_calib_reco_eff, "AnalysisElectrons_%s_effSF");
            initialize_sys_branches(&m_sys_branches_electron_calib_id_eff, "AnalysisElectrons_%s_effSF_ID_Tight");
            initialize_sys_branches(&m_sys_branches_electron_calib_iso_eff, "AnalysisElectrons_%s_effSF_Isol_Tight_FCTight");
            initialize_sys_branches(&m_sys_branches_electron_calib_trig_eff, "AnalysisElectrons_%s_effSF_Trig_Tight_FCTight_Eff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0");
            initialize_sys_branches(&m_sys_branches_electron_calib_trig_sf, "AnalysisElectrons_%s_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0");

            // muon sys branches
            initialize_sys_branches(&m_sys_branches_muon_pt, "AnalysisMuons_%s_pt");
            initialize_sys_branches(&m_sys_branches_muon_selected, "AnalysisMuons_%s_mu_selected");
            initialize_sys_branches(&m_sys_branches_muon_selected_other_sys, "AnalysisMuons_mu_selected_%s");
            initialize_sys_branches(&m_sys_branches_muon_isQuality_Tight, "AnalysisMuons_%s_isQuality_Tight");
            initialize_sys_branches(&m_sys_branches_muon_isIsolated_PflowTight_VarRad, "AnalysisMuons_%s_isIsolated_PflowTight_VarRad");
            initialize_sys_branches(&m_sys_branches_muon_quality_eff, "AnalysisMuons_muon_effSF_Quality_Tight_%s");
            initialize_sys_branches(&m_sys_branches_muon_ttva_eff, "AnalysisMuons_muon_effSF_TTVA_%s");
            initialize_sys_branches(&m_sys_branches_muon_iso_eff, "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
            initialize_sys_branches(&m_sys_branches_muon_trig_2015_eff_mc, "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_%s");
            initialize_sys_branches(&m_sys_branches_muon_trig_2015_eff_data, "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_%s");
            initialize_sys_branches(&m_sys_branches_muon_trig_2018_eff_mc, "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_%s");
            initialize_sys_branches(&m_sys_branches_muon_trig_2018_eff_data, "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_%s");
            initialize_sys_branches(&m_sys_branches_muon_calib_quality_eff, "AnalysisMuons_%s_muon_effSF_Quality_Tight");
            initialize_sys_branches(&m_sys_branches_muon_calib_ttva_eff, "AnalysisMuons_%s_muon_effSF_TTVA");
            initialize_sys_branches(&m_sys_branches_muon_calib_iso_eff, "AnalysisMuons_%s_muon_effSF_Isol_PflowTight_VarRad");
            initialize_sys_branches(&m_sys_branches_muon_calib_trig_2015_eff_mc, "AnalysisMuons_%s_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50");
            initialize_sys_branches(&m_sys_branches_muon_calib_trig_2015_eff_data, "AnalysisMuons_%s_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50");
            initialize_sys_branches(&m_sys_branches_muon_calib_trig_2018_eff_mc, "AnalysisMuons_%s_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50");
            initialize_sys_branches(&m_sys_branches_muon_calib_trig_2018_eff_data, "AnalysisMuons_%s_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50");

            // jet sys branches
            initialize_sys_branches(&m_sys_branches_jet_pt, "AnalysisJets_%s_pt");
            initialize_sys_branches(&m_sys_branches_jet_selected, "AnalysisJets_%s_jet_selected");
            initialize_sys_branches(&m_sys_branches_jet_selected_other_sys, "AnalysisJets_jet_selected_%s");
        }
    }
}

ZCharmLoop::ZCharmLoop(TString input_file, TString out_path, TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name)
{
    add_channel("inclusive", {"all", "bad_muon_veto", "two_leptons", "charge", "trigger", "trigger_match", "z_mass"});
    add_channel("2018_el", {});
    add_channel("2018_mu", {});

    // settings
    m_do_extra_histograms = true;
    m_split_by_period = true;
    m_truth_match = true;
    m_do_pt_v_rw = false;
    m_do_systematics = false;
}

}  // namespace Charm
