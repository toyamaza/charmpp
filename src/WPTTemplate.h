#ifndef W_PT_TEMPLATE_H
#define W_PT_TEMPLATE_H

#include "WQCDCharmLoopBase.h"

namespace Charm
{
class WPTTemplate : public WQCDCharmLoopBase
{
public:
    WPTTemplate(TString input_file, TString out_path, TString tree_name = "CharmAnalysis");

protected:
    virtual void connect_branches() override;

private:
    virtual int execute() override;

private:
    bool m_qcd_cr;
};

}  // namespace Charm

#endif  // MET_TEMPLATE_H
