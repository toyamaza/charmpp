#include "WMETTemplate.h"

#include <math.h>

#include <iostream>

namespace Charm
{
int WMETTemplate::execute()
{
    // Event Selection
    if (m_lep.Pt() < 30 && fabs(m_d0sig) >= m_dosig_cut)
    {
        // QCD MET control region
        set_channel(m_channel + "_QCD");
        m_qcd_cr = true;
    }
    else if (m_lep.Pt() >= 30)
    {
        // Loose SR (no MET or mT cuts)
        m_qcd_cr = false;
    }
    else
    {
        // lepton purgatory: 28 <= pT < 30 and |d0sig| < 2.5 (3.0)
        return 1;
    }

    // D meson selection
    if (m_D_plus_meson)
    {
        get_D_plus_mesons(this);
    }

    // production fraction reweighting
    // must do this here becaue truth branches are not available in preselection
    reweight_production_fractions(this);

    // fill histograms
    fill_histograms(m_channel);

    // MET and mT cuts
    if (m_met >= 30.)
    {
        if (m_qcd_cr)
        {
            fill_histograms(m_channel + "_METCut");
        }
        else if (m_met_mt >= 60.)
        {
            fill_histograms(m_channel + "_SR");
        }
    }

    return 1;
}

void WMETTemplate::connect_branches()
{
    // connect lepton branches
    WQCDCharmLoopBase::connect_branches();

    // additional branches for D meson reco
    if (m_D_plus_meson)
    {
        connect_D_meson_branches(this);
    }

    // truth branches for D
    if (m_is_mc && m_truth_matchD)
    {
        connect_truth_D_meson_branches(this);
    }
}

WMETTemplate::WMETTemplate(TString input_file, TString out_path, TString tree_name)
    : WQCDCharmLoopBase(input_file, out_path, tree_name)
{
}

}  // namespace Charm
