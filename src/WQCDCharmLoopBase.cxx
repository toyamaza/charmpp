#include "WQCDCharmLoopBase.h"

#include <math.h>

#include <iostream>

#include "HelperFunctions.h"
#include "TVector2.h"

namespace Charm
{
void WQCDCharmLoopBase::initialize()
{
    std::cout << "WQCDCharmLoopBase::initiaize" << std::endl;

    // set config
    configure();

    // systematics
    if (m_do_systematics)
    {
        // GEN
        m_sys_branches_GEN.init(get_systematics_from_group("systematics_GEN"));

        // PRW
        m_sys_branches_PU.init(get_systematics_from_group("systematics_PRW"));

        // JVT
        m_sys_branches_JVT.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_JVT.init(get_systematics_from_group("systematics_JET_Jvt"));
        // TODO: check if this is fine?
        // m_sys_branches_JVT.init(get_systematics_from_group("systematics_EL_Calib"));
        // m_sys_branches_JVT.init(get_systematics_from_group("systematics_MUON_Calib"));

        // MET
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_MET_Calib"));
        m_sys_branches_MET_met.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_MET_phi.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_MET_phi.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_MET_phi.init(get_systematics_from_group("systematics_MET_Calib"));
        m_sys_branches_MET_phi.init(get_systematics_from_group("systematics_MUON_Calib"));

        // electron calib
        m_sys_branches_electron_isIsolated_Tight_VarRad.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_pt.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_selected.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_electron_selected.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_electron_selected.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_reco_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_id_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_iso_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        // m_sys_branches_electron_calib_trig_eff.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_trig_sf.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_trig_noiso_sf.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_electron_calib_charge_eff.init(get_systematics_from_group("systematics_EL_Calib"));

        // muon calib
        m_sys_branches_muon_isQuality_Tight.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_pt.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_selected.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_muon_selected.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_muon_selected.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_isIsolated_PflowTight_VarRad.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_quality_eff.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_ttva_eff.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2015_eff_data.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_iso_eff.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2015_eff_mc.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2018_eff_data.init(get_systematics_from_group("systematics_MUON_Calib"));
        m_sys_branches_muon_calib_trig_2018_eff_mc.init(get_systematics_from_group("systematics_MUON_Calib"));

        // electron weights
        m_sys_branches_electron_reco_eff.init(get_systematics_from_group("systematics_EL_EFF_Reco"));
        m_sys_branches_electron_id_eff.init(get_systematics_from_group("systematics_EL_EFF_ID"));
        m_sys_branches_electron_iso_eff.init(get_systematics_from_group("systematics_EL_EFF_Iso"));
        // m_sys_branches_electron_trig_eff.init(get_systematics_from_group("systematics_EL_EFF_Trigger"));
        m_sys_branches_electron_trig_sf.init(get_systematics_from_group("systematics_EL_EFF_Trigger"));
        m_sys_branches_electron_charge_eff.init(get_systematics_from_group("systematics_EL_CHARGE"));
        m_sys_branches_electron_trig_noiso_sf.init(get_systematics_from_group("systematics_EL_EFF_Trigger"));

        // muon weights
        m_sys_branches_muon_quality_eff.init(get_systematics_from_group("systematics_MUON_EFF_RECO"));
        m_sys_branches_muon_ttva_eff.init(get_systematics_from_group("systematics_MUON_EFF_TTVA"));
        m_sys_branches_muon_trig_2015_eff_data.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));
        m_sys_branches_muon_iso_eff.init(get_systematics_from_group("systematics_MUON_EFF_ISO"));
        m_sys_branches_muon_trig_2015_eff_mc.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));
        m_sys_branches_muon_trig_2018_eff_data.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));
        m_sys_branches_muon_trig_2018_eff_mc.init(get_systematics_from_group("systematics_MUON_EFF_Trig"));

        // jet calib
        m_sys_branches_jet_pt.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_jet_selected.init(get_systematics_from_group("systematics_JET_Calib"));
        m_sys_branches_jet_selected.init(get_systematics_from_group("systematics_EL_Calib"));
        m_sys_branches_jet_selected.init(get_systematics_from_group("systematics_MUON_Calib"));

        // FTAG
        m_sys_branches_jet_ftag_eff.init(get_systematics_from_group("systematics_FT_EFF"));
        m_sys_branches_jet_calib_ftag_eff.init(get_systematics_from_group("systematics_JET_Calib"));
    }
}

bool WQCDCharmLoopBase::preselection()
{
    set_channel("inclusive");

    // skip if not ordered to do lepton preselection
    if (!m_do_lep_presel)
    {
        std::string prepend = "inclusive";
        if(m_D_branch_sys != "")
            prepend = DMesonRec::m_D_branch_sys + "_-_inclusive";
        m_channel_sys = std::vector<std::string>(m_systematics.size() + 1, prepend);
        DMesonRec::set_lepton(nullptr);
        m_lep_charge = 0;
        if (m_data_period == "ForcedDecay")
        {
            // mc event weight, initial sum of weights, x-sec, lumi weight
            multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_branches_GEN, true, false, false);
        }
        return true;
    }

    // loose D selection for lepton overlap removal
    if (m_D_plus_meson || m_D_star_meson || m_D_star_pi0_meson || m_D_s_meson)
    {
        get_D_plus_mesons(this, true);
        get_D_star_mesons(this, true);
        get_D_star_pi0_mesons(this, true);
        get_D_s_mesons(this, true);
    }

    // other weights
    if (m_is_mc && !m_no_weights)
    {
        // mc event weight, initial sum of weights, x-sec, lumi weight
        multiply_mc_event_weight(m_EventInfo_generatorWeight_NOSYS, &m_sys_branches_GEN, m_apply_lumi, false, (m_mc_shower == "sherpa"));

        // jvt weight and systematics
        multiply_nominal_and_sys_weight(m_EventInfo_jvt_effSF_NOSYS, &m_sys_branches_JVT);

        // pileup weight and systematics
        multiply_nominal_and_sys_weight(m_EventInfo_PileupWeight_NOSYS, &m_sys_branches_PU);

        // FTAG systematics
        if (m_is_mc && m_jet_selection)
        {
            // multiply_nominal_and_sys_weight(m_EventInfo_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS, &m_sys_branches_FTAG);
        }
    }

    // sys branches for leptons
    read_sys_branches();

    // read MET
    m_sys_met = get_val_for_systematics(m_METInfo_MET_NOSYS, &m_sys_branches_MET_met, Charm::GeV);
    m_sys_met_phi = get_val_for_systematics(m_METInfo_METPhi_NOSYS, &m_sys_branches_MET_phi);

    // require exactly one loose lepton
    get_loose_electrons();
    get_loose_muons();

    // require exactly one tight lepton
    get_tight_electrons();
    get_tight_muons();

    // clear all before running over systematics
    m_sys_anti_tight = std::vector<bool>(m_systematics.size() + 1, 0.);
    m_sys_met_dphi = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_met_mt = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_pt = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_pt_plusTopoConeET20 = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_eta = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_abs_eta = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_calo_eta = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_phi = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_charge = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_lep_dR_D = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_nbjets = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_met_AT = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_met_perp = std::vector<float>(m_systematics.size() + 1, 0.);
    m_sys_met_para = std::vector<float>(m_systematics.size() + 1, 0.);

    for (unsigned int i = 0; i < m_systematics.size() + 1; i++)
    {
        bool nominal = i == 0;
        if (!m_do_systematics && !nominal)
        {
            continue;
        }

        unsigned int sys_index = i - 1;
        std::vector<std::pair<unsigned int, float>> electrons, muons;
        electrons = m_electrons_sys.at(i);
        muons = m_muons_sys.at(i);
        auto loose_electrons = m_loose_electrons_sys.at(i);
        auto loose_muons = m_loose_muons_sys.at(i);
        TLorentzVector lep;
        std::string channel_string = "";

        // bad muon veto
        for (unsigned int j = 0; j < loose_muons.size(); j++)
        {
            if (m_AnalysisMuons_is_bad->at(loose_muons.at(j).first))
            {
                continue;
            }
        }

        // exactly one loose lepton
        if ((loose_electrons.size() + loose_muons.size()) != 1)
        {
            continue;
        }

        // check if anti-tight region
        if ((electrons.size() + muons.size()) != 1)
        {
            m_sys_anti_tight.at(i) = true;
            if (!(m_fake_rate_measurement || m_fake_factor_method || (!m_is_mc && m_eval_fake_rate)))
            {
                continue;
            }
        }

        // electron channel
        if (loose_electrons.size() == 1)
        {
            // electron index
            unsigned int index = loose_electrons.at(0).first;

            // channel
            channel_string = "el";

            // pass electron trigger
            if (!pass_electron_trigger())
            {
                continue;
            }

            // single electron trigger
            if (!electron_is_trigger_matched(index, m_sys_el_pt.at(i)->at(index) * Charm::GeV))
            {
                continue;
            }

            // fill variables
            lep.SetPtEtaPhiM(m_sys_el_pt.at(i)->at(index) * Charm::GeV,
                             m_AnalysisElectrons_eta->at(index),
                             m_AnalysisElectrons_phi->at(index),
                             Charm::ELECTRON_MASS * Charm::GeV);
            if (nominal)
            {
                m_lep = lep;
            }
            m_sys_lep_pt.at(i) = m_sys_el_pt.at(i)->at(index) * Charm::GeV;
            if (m_do_el_topocone_param)
            {
                m_sys_lep_pt_plusTopoConeET20.at(i) = m_sys_lep_pt.at(i) + m_AnalysisElectrons_topoetcone20->at(index) * Charm::GeV;
            }
            m_sys_lep_eta.at(i) = m_AnalysisElectrons_eta->at(index);
            m_sys_lep_abs_eta.at(i) = std::abs(m_AnalysisElectrons_eta->at(index));
            m_sys_lep_calo_eta.at(i) = m_AnalysisElectrons_caloCluster_eta->at(index);
            m_sys_lep_phi.at(i) = m_AnalysisElectrons_phi->at(index);
            m_sys_lep_charge.at(i) = m_AnalysisElectrons_charge->at(index);
            m_sys_lep_dR_D.at(i) = get_dr_lep_Dmeson(m_sys_lep_eta.at(i), m_sys_lep_phi.at(i));
            if (nominal)
            {
                m_d0 = m_AnalysisElectrons_d0->at(index);
                m_d0sig = m_AnalysisElectrons_d0sig->at(index);
                if (m_do_extra_histograms)
                {
                    m_lep_is_isolated = m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS->at(index);
                    m_z0sinTheta = m_AnalysisElectrons_z0sinTheta->at(index);
                    m_topoetcone20 = m_AnalysisElectrons_topoetcone20->at(index) * Charm::GeV;
                    m_ptvarcone20_TightTTVA_pt1000 = m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(index) * Charm::GeV;
                    m_ptvarcone30_TightTTVA_pt1000 = m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000->at(index) * Charm::GeV;
                    m_topoetcone20_over_pt = m_topoetcone20 / lep.Pt();
                    m_ptvarcone20_TightTTVA_pt1000_over_pt = m_ptvarcone20_TightTTVA_pt1000 / lep.Pt();
                    m_ptvarcone30_TightTTVA_pt1000_over_pt = m_ptvarcone30_TightTTVA_pt1000 / lep.Pt();
                    m_newflowisol = -1.0;
                    // m_EOverP = m_AnalysisElectrons_EOverP->at(index);
                }
            }

            // efficiency weight
            if (m_is_mc)
            {
                if (!m_sys_anti_tight.at(i))
                {
                    multiply_event_weight(m_electrons_sys.at(i).at(0).second, i);
                }
                else
                {
                    multiply_event_weight(m_loose_electrons_sys.at(i).at(0).second, i);
                }
            }
        }
        else if (loose_muons.size() == 1)
        {
            // muon index
            unsigned int index = loose_muons.at(0).first;

            // channel
            channel_string = "mu";

            // pass muon trigger
            if (!pass_muon_trigger())
            {
                continue;
            }

            // single muon trigger
            if (!muon_is_trigger_matched(index, m_sys_mu_pt.at(i)->at(index) * Charm::GeV))
            {
                continue;
            }

            // fill variables
            lep.SetPtEtaPhiM(m_sys_mu_pt.at(i)->at(index) * Charm::GeV,
                             m_AnalysisMuons_eta->at(index),
                             m_AnalysisMuons_phi->at(index),
                             Charm::MUON_MASS * Charm::GeV);
            if (nominal)
            {
                m_lep = lep;
            }
            m_sys_lep_pt.at(i) = m_sys_mu_pt.at(i)->at(index) * Charm::GeV;
            if (m_do_el_topocone_param)
            {
                m_sys_lep_pt_plusTopoConeET20.at(i) = m_sys_lep_pt.at(i) + m_AnalysisMuons_topoetcone20->at(index) * Charm::GeV;
            }
            m_sys_lep_eta.at(i) = m_AnalysisMuons_eta->at(index);
            m_sys_lep_abs_eta.at(i) = std::abs(m_AnalysisMuons_eta->at(index));
            m_sys_lep_calo_eta.at(i) = m_AnalysisMuons_eta->at(index);
            m_sys_lep_phi.at(i) = m_AnalysisMuons_phi->at(index);
            m_sys_lep_charge.at(i) = m_AnalysisMuons_charge->at(index);
            m_sys_lep_dR_D.at(i) = get_dr_lep_Dmeson(m_sys_lep_eta.at(i), m_sys_lep_phi.at(i));
            if (nominal)
            {
                m_d0 = m_AnalysisMuons_d0->at(index);
                m_d0sig = m_AnalysisMuons_d0sig->at(index);
                if (m_do_extra_histograms)
                {
                    m_lep_is_isolated = m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS->at(index);
                    m_z0sinTheta = m_AnalysisMuons_z0sinTheta->at(index);
                    m_topoetcone20 = m_AnalysisMuons_topoetcone20->at(index) * Charm::GeV;
                    m_ptvarcone30_TightTTVA_pt1000 = m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000->at(index) * Charm::GeV;
                    m_topoetcone20_over_pt = m_topoetcone20 / lep.Pt();
                    m_ptvarcone30_TightTTVA_pt1000_over_pt = m_ptvarcone30_TightTTVA_pt1000 / lep.Pt();
                    m_ptvarcone20_TightTTVA_pt1000_over_pt = -1.0;
                    m_ptvarcone30_TightTTVA_pt1000_over_pt = -1.0;
                    m_newflowisol = (m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(index) + 0.4 * m_AnalysisMuons_neflowisol20->at(index)) * Charm::GeV / lep.Pt();
                }
            }
            // efficiency weight
            if (m_is_mc)
            {
                if (!m_sys_anti_tight.at(i))
                {
                    multiply_event_weight(m_muons_sys.at(i).at(0).second, i);
                }
                else
                {
                    multiply_event_weight(m_loose_muons_sys.at(i).at(0).second, i);
                }
            }
        }

        // set channel
        if (channel_string != "")
        {
            // base name
            m_channel_sys.at(i) = channel_string;

            // period string
            if (m_split_by_period)
            {
                m_channel_sys.at(i) = get_period_string() + "_" + m_channel_sys.at(i);
            }

            // lepton charge
            if (m_split_by_charge)
            {
                m_channel_sys.at(i) = m_channel_sys.at(i) + "_" + get_lepton_charge_string(m_sys_lep_charge.at(i));
            }
        }
        else
        {
            continue;
        }

        // MET for antiTight is set to a special definition w/o systematic uncertainty (as it originates from data)
        if (m_use_anti_tight_met)
        {
            if (m_sys_anti_tight.at(i))
            {
                m_sys_met_AT.at(i) = m_METInfoAntiTight_NOSYS_MET * Charm::GeV;
                if (loose_muons.size() == 1)
                {
                    TVector2 met_tmp, muon_tmp;
                    double tmp_lep_pt = m_sys_lep_pt.at(i);
                    met_tmp.SetMagPhi(m_METInfoAntiTight_NOSYS_MET * Charm::GeV, m_METInfoAntiTight_NOSYS_METPhi);
                    muon_tmp.SetMagPhi(tmp_lep_pt, m_sys_lep_phi.at(i));
                    m_sys_met.at(i) = (met_tmp - muon_tmp).Mod();
                    m_sys_met_phi.at(i) = TVector2::Phi_mpi_pi(met_tmp.Phi() - muon_tmp.Phi());
                }
            }
        }

        // additional MET variables
        m_sys_met_dphi.at(i) = TVector2::Phi_mpi_pi(m_sys_met_phi.at(i) - lep.Phi());
        m_sys_met_mt.at(i) = sqrt(2 * lep.Pt() * (m_sys_met.at(i)) * (1 - cos(m_sys_met_dphi.at(i))));
        m_sys_met_para.at(i) = m_sys_met.at(i) * cos(m_sys_met_dphi.at(i));
        m_sys_met_perp.at(i) = m_sys_met.at(i) * sin(m_sys_met_dphi.at(i));
    }

    // truth selection
    if (m_is_mc)
    {
        if (m_fiducial_selection || m_save_truth_wjets)
        {
            get_truth_wjets();
        }
        if (m_do_pt_v_rw)
        {
            if (m_mc_gen == "powheg" || m_mc_gen == "sherpa")
            {
                m_event_weight *= calculate_ptv_weight();
            }
        }
    }

    // accept event
    return true;
}

int WQCDCharmLoopBase::execute()
{
    throw std::runtime_error("execute() not implemented for base class WQCDCharmLoopBase");
    return 1;
}

float WQCDCharmLoopBase::get_muon_trigger_weight(unsigned int sys_index, unsigned int i)
{
    if (m_EventInfo_runNumber < 290000)
    {
        float effMC1 = m_sys_mu_trig_2015_eff_mc.at(sys_index)->at(i);
        float effData1 = m_sys_mu_trig_2015_eff_data.at(sys_index)->at(i);

        float PMC = effMC1;
        float PData = effData1;

        if (PMC > 0)
        {
            return PData / PMC;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        float effMC1 = m_sys_mu_trig_2018_eff_mc.at(sys_index)->at(i);
        float effData1 = m_sys_mu_trig_2018_eff_data.at(sys_index)->at(i);

        float PMC = effMC1;
        float PData = effData1;

        if (PMC > 0)
        {
            return PData / PMC;
        }
        else
        {
            return 0;
        }
    }
}

void WQCDCharmLoopBase::get_tight_electrons()
{
    m_electrons_sys.clear();

    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_electrons_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++)
        {
            // nominal
            if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_el_selected.at(j)->at(i)) &&
                (m_sys_el_iso.at(j)->at(i)) &&
                (m_AnalysisElectrons_likelihood_Tight->at(i)) &&
                ((m_is_mc && m_truth_match && electron_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc)))
            {
                float sf = 1.;
                if (m_is_mc)
                {
                    sf *= m_sys_el_reco_eff.at(j)->at(i) *
                          m_sys_el_id_eff.at(j)->at(i) *
                          m_sys_el_iso_eff.at(j)->at(i) *
                          m_sys_el_trig_sf.at(j)->at(i) *
                          m_sys_el_charge_sf.at(j)->at(i);
                }

                std::pair<unsigned int, float> pair(i, sf);
                m_electrons_sys.at(j).push_back(pair);
            }
        }
    }
}

void WQCDCharmLoopBase::get_tight_muons()
{
    m_muons_sys.clear();
    // calibration sys
    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_muons_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++)
        {
            // nominal
            if ((m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_mu_selected.at(j)->at(i)) &&
                (m_sys_mu_quality.at(j)->at(i)) &&
                (m_sys_mu_iso.at(j)->at(i)) &&
                ((m_is_mc && m_truth_match && muon_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc)))
            {
                float sf = 1.;
                if (m_is_mc)
                {
                    sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                          m_sys_mu_ttva_eff.at(j)->at(i) *
                          m_sys_mu_iso_eff.at(j)->at(i) *
                          get_muon_trigger_weight(j, i);
                }

                std::pair<unsigned int, float> pair(i, sf);
                m_muons_sys.at(j).push_back(pair);
            }
        }
    }
}

void WQCDCharmLoopBase::get_loose_electrons()
{
    m_loose_electrons_sys.clear();

    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_loose_electrons_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisElectrons_pt_NOSYS->size(); i++)
        {
            // nominal
            if ((m_sys_el_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_el_selected.at(j)->at(i)) &&
                (m_AnalysisElectrons_likelihood_Tight->at(i)) &&
                ((m_is_mc && m_truth_match && electron_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc)))
            {
                // additional requirements for Anti-Tight
                if (!m_sys_el_iso.at(j)->at(i))
                {
                    if (m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000->at(i) == 0)
                    {
                        continue;
                    }
                }
                float sf = 1.;
                if (m_is_mc)
                {
                    sf *= m_sys_el_reco_eff.at(j)->at(i) *
                          m_sys_el_id_eff.at(j)->at(i) *
                          m_sys_el_trig_noiso_sf.at(j)->at(i);
                }

                std::pair<unsigned int, float> pair(i, sf);
                m_loose_electrons_sys.at(j).push_back(pair);
            }
        }
    }
}

void WQCDCharmLoopBase::get_loose_muons()
{
    m_loose_muons_sys.clear();

    // calibration sys
    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_loose_muons_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisMuons_pt_NOSYS->size(); i++)
        {
            // nominal
            if ((m_sys_mu_pt.at(j)->at(i) * Charm::GeV >= 30.) &&
                (m_sys_mu_selected.at(j)->at(i)) &&
                (m_sys_mu_quality.at(j)->at(i)) &&
                ((m_is_mc && m_truth_match && muon_is_prompt(i)) || (m_is_mc && !m_truth_match) || (!m_is_mc)))
            {
                // additional requirements for Anti-Tight
                if (!m_sys_mu_iso.at(j)->at(i))
                {
                    if (((m_AnalysisMuons_ptvarcone30_TightTTVA_pt500->at(i) + 0.4 * m_AnalysisMuons_neflowisol20->at(i)) / m_sys_mu_pt.at(j)->at(i)) < 0.12)
                    {
                        continue;
                    }
                }
                float sf = 1.;
                if (m_is_mc)
                {
                    sf *= m_sys_mu_quality_eff.at(j)->at(i) *
                          m_sys_mu_ttva_eff.at(j)->at(i) *
                          get_muon_trigger_weight(j, i);
                }
                std::pair<unsigned int, float> pair(i, sf);
                m_loose_muons_sys.at(j).push_back(pair);
            }
        }
    }
}

float WQCDCharmLoopBase::get_dr_lep_Dmeson(float lepton_eta, float lepton_phi)
{
    float dR_min = 999;
    for (unsigned int j = 0; j < m_D_plus_mesons.size(); j++)
    {
        float dR = DMesonRec::D_meson_lepton_dR(m_D_plus_mesons.at(j).first, lepton_eta, lepton_phi);
        if (dR < dR_min)
        {
            dR_min = dR;
        }
    }
    for (unsigned int j = 0; j < m_D_star_mesons.size(); j++)
    {
        float dR = DMesonRec::D_meson_lepton_dR(m_D_star_mesons.at(j).first, lepton_eta, lepton_phi);
        if (dR < dR_min)
        {
            dR_min = dR;
        }
    }
    for (unsigned int j = 0; j < m_D_star_pi0_mesons.size(); j++)
    {
        float dR = DMesonRec::D_meson_lepton_dR(m_D_star_pi0_mesons.at(j).first, lepton_eta, lepton_phi);
        if (dR < dR_min)
        {
            dR_min = dR;
        }
    }
    for (unsigned int j = 0; j < m_D_s_mesons.size(); j++)
    {
        float dR = DMesonRec::D_meson_lepton_dR(m_D_s_mesons.at(j).first, lepton_eta, lepton_phi);
        if (dR < dR_min)
        {
            dR_min = dR;
        }
    }
    return dR_min;
}

void WQCDCharmLoopBase::get_jets()
{
    m_jets_sys.clear();
    if (!m_do_lep_presel)
    {
        return;
    }
    // calibration sys
    for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
    {
        m_jets_sys.push_back({});
        for (unsigned int i = 0; i < m_AnalysisJets_pt_NOSYS->size(); i++)
        {
            bool pass_OR = true;
            if (m_D_plus_meson)
            {
                for (unsigned int k = 0; k < m_D_plus_mesons.size(); k++)
                {
                    std::pair<unsigned int, int> Dplus_pair = m_D_plus_mesons.at(k);
                    float dR = get_dr_bjet_Dmeson(i, Dplus_pair.first);
                    if (dR < 0.4)
                    {
                        pass_OR = false;
                        break;
                    }
                }
            }
            if (m_D_star_meson)
            {
                for (unsigned int k = 0; k < m_D_star_mesons.size(); k++)
                {
                    std::pair<unsigned int, int> Dstar_pair = m_D_star_mesons.at(k);
                    float dR = get_dr_bjet_Dmeson(i, Dstar_pair.first);
                    if (dR < 0.4)
                    {
                        pass_OR = false;
                        break;
                    }
                }
            }
            if (m_D_star_pi0_meson)
            {
                for (unsigned int k = 0; k < m_D_star_pi0_mesons.size(); k++)
                {
                    std::pair<unsigned int, int> Dstar_pair = m_D_star_pi0_mesons.at(k);
                    float dR = get_dr_bjet_Dmeson(i, Dstar_pair.first);
                    if (dR < 0.4)
                    {
                        pass_OR = false;
                        break;
                    }
                }
            }
            if (m_D_s_meson)
            {
                for (unsigned int k = 0; k < m_D_s_mesons.size(); k++)
                {
                    std::pair<unsigned int, int> Ds_pair = m_D_s_mesons.at(k);
                    float dR = get_dr_bjet_Dmeson(i, Ds_pair.first);
                    if (dR < 0.4)
                    {
                        pass_OR = false;
                        break;
                    }
                }
            }
            if (((fabs(m_AnalysisJets_eta->at(i)) < 2.4 && (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.)) ||
                 (fabs(m_AnalysisJets_eta->at(i)) >= 2.4 && fabs(m_AnalysisJets_eta->at(i)) <= 5.0 && (m_sys_jet_pt.at(j)->at(i) * Charm::GeV >= 20.))) &&
                (m_sys_jet_selected.at(j)->at(i)) && pass_OR)
            {
                m_jets_sys.at(j).push_back(i);
            }
        }
    }
}

bool WQCDCharmLoopBase::pass_electron_trigger()
{
    if (m_EventInfo_runNumber < 290000)
    {
        return m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH ||
               m_EventInfo_trigPassed_HLT_e60_lhmedium ||
               m_EventInfo_trigPassed_HLT_e120_lhloose;
    }
    else
    {
        return m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose ||
               m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 ||
               m_EventInfo_trigPassed_HLT_e140_lhloose_nod0;
    }
}

bool WQCDCharmLoopBase::pass_muon_trigger()
{
    if (m_EventInfo_runNumber < 290000)
    {
        return m_EventInfo_trigPassed_HLT_mu50 ||
               m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15;
    }
    else
    {
        return m_EventInfo_trigPassed_HLT_mu50 ||
               m_EventInfo_trigPassed_HLT_mu26_ivarmedium;
    }
}

bool WQCDCharmLoopBase::electron_is_trigger_matched(unsigned int i, float el_pt)
{
    if (m_EventInfo_runNumber < 290000)
    {
        return (m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH && m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH->at(i)) ||
               (m_EventInfo_trigPassed_HLT_e60_lhmedium && m_AnalysisElectrons_matched_HLT_e60_lhmedium->at(i) && el_pt > 65.0) ||
               (m_EventInfo_trigPassed_HLT_e120_lhloose && m_AnalysisElectrons_matched_HLT_e120_lhloose->at(i) && el_pt > 132.0);
    }
    else
    {
        return (m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose && m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose->at(i)) ||
               (m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0 && m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0->at(i) && el_pt > 65.0) ||
               (m_EventInfo_trigPassed_HLT_e140_lhloose_nod0 && m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0->at(i) && el_pt > 132.0);
    }
}

bool WQCDCharmLoopBase::muon_is_trigger_matched(unsigned int i, float mu_pt)
{
    if (m_EventInfo_runNumber < 290000)
    {
        return ((m_EventInfo_trigPassed_HLT_mu50 && m_AnalysisMuons_matched_HLT_mu50->at(i) && mu_pt > 55.0 ) ||
                (m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15 && m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15->at(i)));
    }
    else
    {
        return ((m_EventInfo_trigPassed_HLT_mu50 && m_AnalysisMuons_matched_HLT_mu50->at(i) && mu_pt > 55.0) ||
                (m_EventInfo_trigPassed_HLT_mu26_ivarmedium && m_AnalysisMuons_matched_HLT_mu26_ivarmedium->at(i)));
    }
}

// see: https://gitlab.cern.ch/ATLAS-IFF/IFFTruthClassifier/-/blob/master/Root/IFFTruthClassifier.cxx
// --------------------------------------------------------------------------------------------------
bool WQCDCharmLoopBase::electron_is_prompt(unsigned int i)
{
    // prompt
    if (m_AnalysisElectrons_truthType->at(i) == 2)
    {
        return true;
    }

    // Adding these cases from ElectronEfficiencyHelpers
    if (m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 2 && fabs(m_AnalysisElectrons_firstEgMotherPdgId->at(i)) == 11)
    {
        return true;
    }

    // FSR photons from electrons
    if (m_AnalysisElectrons_truthType->at(i) == 15 && m_AnalysisElectrons_truthOrigin->at(i) == 40)
    {
        return true;
    }
    if (m_AnalysisElectrons_truthType->at(i) == 4 && Charm::is_in(m_AnalysisElectrons_truthOrigin->at(i), {5, 7}) &&
        m_AnalysisElectrons_firstEgMotherTruthType->at(i) == 15 && m_AnalysisElectrons_firstEgMotherTruthOrigin->at(i) == 40)
    {
        return true;
    }

    // If we reach here then it is not a prompt electron
    return false;
}

bool WQCDCharmLoopBase::muon_is_prompt(unsigned int i)
{
    return ((m_AnalysisMuons_truthType->at(i) == 6) &&
            Charm::is_in(m_AnalysisMuons_truthOrigin->at(i),
                         {10, 12, 13, 14, 43}));
}

float WQCDCharmLoopBase::calculate_ptv_weight()
{
    float w = get_ptv_weight(m_truth_v_pt);
    return w;
}

void WQCDCharmLoopBase::fill_PV_histograms(std::string channel)
{
    std::vector<std::string> channels = std::vector<std::string>(m_systematics.size() + 1, channel);
    fill_PV_histograms(channels);
}

void WQCDCharmLoopBase::fill_PV_histograms(std::vector<std::string> channels, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    if (m_fit_variables_only)
    {
        return;
    }
    add_fill_histogram_sys("PV_X", channels, m_CharmEventInfo_PV_X,
                           100, -0.8, -0.2, false, extra_flag, extra_weight);
    add_fill_histogram_sys("PV_Y", channels, m_CharmEventInfo_PV_Y,
                           100, -0.8, -0.2, false, extra_flag, extra_weight);
    add_fill_histogram_sys("PV_Z", channels, m_CharmEventInfo_PV_Z,
                           100, -200, 200, false, extra_flag, extra_weight);
    // add_fill_histogram_sys("truthVertex_X", channels, m_CharmEventInfo_truthVertex_X,
    //                        100, -0.8, -0.2, false, extra_flag, extra_weight);
    // add_fill_histogram_sys("truthVertex_Y", channels, m_CharmEventInfo_truthVertex_Y,
    //                        100, -0.8, -0.2, false, extra_flag, extra_weight);
    // add_fill_histogram_sys("truthVertex_Z", channels, m_CharmEventInfo_truthVertex_Z,
    //                        100, -200, 200, false, extra_flag, extra_weight);
    // add_fill_histogram_sys("truth_reco_vtx_diff_X", channels,
    //                        m_CharmEventInfo_truthVertex_X - m_CharmEventInfo_PV_X, 100, -0.2, 0.2, false, extra_flag, extra_weight);
    // add_fill_histogram_sys("truth_reco_vtx_diff_Y", channels,
    //                        m_CharmEventInfo_truthVertex_Y - m_CharmEventInfo_PV_Y, 100, -0.2, 0.2, false, extra_flag, extra_weight);
    // add_fill_histogram_sys("truth_reco_vtx_diff_Z", channels,
    //                        m_CharmEventInfo_truthVertex_Z - m_CharmEventInfo_PV_Z, 100, -0.2, 0.2, false, extra_flag, extra_weight);
}

void WQCDCharmLoopBase::fill_lepton_histograms(std::string channel)
{
    std::vector<std::string> channels = std::vector<std::string>(m_systematics.size() + 1, channel);
    fill_lepton_histograms(channels);
}

void WQCDCharmLoopBase::fill_lepton_histograms(std::vector<std::string> channels, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    if (m_fit_variables_only)
    {
        return;
    }
    add_fill_histogram_sys("lep_pt", channels, m_sys_lep_pt,
                           400, 0, 400, true, extra_flag, extra_weight);
    if (m_do_el_topocone_param)
    {
        add_fill_histogram_sys("lep_pt_plusTopoConeET20", channels, m_sys_lep_pt_plusTopoConeET20,
                           400, 0, 400, true, extra_flag, extra_weight);
    }
    add_fill_histogram_sys("lep_eta", channels, m_sys_lep_eta,
                           100, -3.0, 3.0, true, extra_flag, extra_weight);
    add_fill_histogram_sys("lep_phi", channels, m_sys_lep_phi,
                           100, -M_PI, M_PI, false, extra_flag, extra_weight);
    add_fill_histogram_sys("lep_charge", channels, m_sys_lep_charge,
                           2, -2, 2, true, extra_flag, extra_weight);
    add_fill_histogram_sys("lep_dR_Dmeson", channels, m_sys_lep_dR_D,
                           120, 0, 6, true, extra_flag, extra_weight);
    add_fill_histogram_sys("met_mt", channels, m_sys_met_mt,
                           400, 0, 400, true, extra_flag, extra_weight);
    add_fill_histogram_sys("met_met", channels, m_sys_met,
                           400, 0, 400, true, extra_flag, extra_weight);
    add_fill_histogram_sys("met_dphi", channels, m_sys_met_dphi,
                           100, -M_PI, M_PI, false, extra_flag, extra_weight);
    add_fill_histogram_sys("pileup", channels, m_EventInfo_correctedScaled_averageInteractionsPerCrossing,
                           80, 0, 80, false, extra_flag, extra_weight);
    add_fill_histogram_sys("run_number", channels, m_EventInfo_runNumber,
                          150, 250000, 400000, false, extra_flag, extra_weight);
    if (m_fiducial_selection || m_save_truth_wjets)
    {
        add_fill_histogram_sys("truth_lep_pt", channels, m_truth_lep1.Pt(),
                               400, 0, 400, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_lep_eta", channels, m_truth_lep1.Eta(),
                               100, -3.0, 3.0, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_lep_phi", channels, m_truth_lep1.Phi(),
                               100, -M_PI, M_PI, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_v_pt", channels, m_truth_v.Pt(),
                               400, 0, 400, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_v_eta", channels, m_truth_v.Eta(),
                               100, -3.0, 3.0, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_v_phi", channels, m_truth_v.Phi(),
                               100, -M_PI, M_PI, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_v_m", channels, m_truth_v.M(),
                               400, 0, 400, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_met_met", channels, m_truth_lep2.Pt(),
                               400, 0, 400, false, extra_flag, extra_weight);
        add_fill_histogram_sys("truth_met_mt", channels, m_truth_mt,
                               400, 0, 400, false, extra_flag, extra_weight);
    }
    if (m_jet_selection)
    {
        add_fill_histogram_sys("njets", channels, m_jets.size(),
                               20, -0.5, 19.5, true, extra_flag, extra_weight);
        add_fill_histogram_sys("nbjets", channels, m_sys_nbjets,
                               20, -0.5, 19.5, true, extra_flag, extra_weight);
    }
    if (m_do_extra_histograms)
    {
        add_fill_histogram_sys("lep_d0", channels, m_d0 * Charm::mum,
                               400, -200, 200, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_d0sig", channels, m_d0sig,
                               100, -6.0, 6.0, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_d0_fixed", channels, rescaled_d0(m_d0, m_EventInfo_runNumber, m_is_mc) * Charm::mum,
                               400, -200, 200, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_d0sig_fixed", channels, m_d0sig / m_d0 * rescaled_d0(m_d0, m_EventInfo_runNumber, m_is_mc),
                               100, -6.0, 6.0, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_z0sinTheta", channels, m_z0sinTheta,
                               100, -0.6, 0.6, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_topoetcone20", channels, m_topoetcone20,
                               100, 0.0, 10.0, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_topoetcone20_over_pt", channels, m_topoetcone20_over_pt,
                               100, 0.0, 0.16, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_ptvarcone30_TightTTVA_pt1000", channels, m_ptvarcone30_TightTTVA_pt1000,
                               100, 0.0, 10.0, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_ptvarcone30_TightTTVA_pt1000_over_pt", channels, m_ptvarcone30_TightTTVA_pt1000_over_pt,
                               100, 0.0, 0.1, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_ptvarcone20_TightTTVA_pt1000", channels, m_ptvarcone20_TightTTVA_pt1000,
                               100, 0.0, 10.0, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_ptvarcone20_TightTTVA_pt1000_over_pt", channels, m_ptvarcone20_TightTTVA_pt1000_over_pt,
                               100, 0.0, 0.1, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_newflowisol_over_pt", channels, m_newflowisol,
                               200, 0.0, 0.5, false, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_is_isolated", channels, m_lep_is_isolated,
                               2, 0.0, 2.0, false, extra_flag, extra_weight);
        // add_fill_histogram_sys("lep_EOverP", channels, m_EOverP,
        //                         1100, 0.0, 11.0, false, extra_flag, extra_weight);
    }
    if (m_fake_rate_measurement)
    {
        add_fill_histogram_sys("lep_pt_calo_eta", channels, m_sys_lep_pt, m_sys_lep_calo_eta,
                               400, 0, 400, 5, m_eta_bins_el, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_pt_eta", channels, m_sys_lep_pt, m_sys_lep_eta,
                               400, 0, 400, 3, m_eta_bins_mu, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_pt_calo_eta_plusTopoConeET20", channels, m_sys_lep_pt_plusTopoConeET20, m_sys_lep_calo_eta,
                               400, 0, 400, 5, m_eta_bins_el, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_pt_eta_plusTopoConeET20", channels, m_sys_lep_pt_plusTopoConeET20, m_sys_lep_eta,
                               400, 0, 400, 3, m_eta_bins_mu, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_pt_met", channels, m_sys_lep_pt, m_sys_met,
                               400, 0, 400, 5, m_met_bins_lep, true, extra_flag, extra_weight);
        add_fill_histogram_sys("lep_pt_met_plusTopoConeET20", channels, m_sys_lep_pt_plusTopoConeET20, m_sys_met,
                               400, 0, 400, 5, m_met_bins_lep, true, extra_flag, extra_weight);
    }
}

void WQCDCharmLoopBase::fill_D_plus_histograms(std::string channel)
{
    // number of D mesons
    if (m_fill_wjets && !m_fit_variables_only)
    {
        add_fill_histogram_sys("N_Dplus", channel, m_D_plus_mesons.size(), 20, -0.5, 19.5, true);
    }

    // single entry region with at least one D meson
    if (m_do_single_entry_D)
    {
        std::vector<std::string> single_entry_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_systematics.size() + 1, false);
        std::vector<float> multiplicity = std::vector<float>(m_systematics.size() + 1, 0);
        bool has_D = false;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            // loop over D plus mesons
            for (unsigned int i = 0; i < m_D_plus_mesons.size(); i++)
            {
                // retreive the D meson
                std::pair<unsigned int, int> Dplus_pair = m_D_plus_mesons.at(i);

                // dR cut
                float dRlep = 999;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Dplus_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                }
                if (dRlep >= 0.3)
                {
                    sys_at_least_one_D.at(j) = true;
                    multiplicity.at(j) += 1;
                }
            }
            if (multiplicity.at(j) > 0)
            {
                single_entry_channel.at(j) += "_Dplus";
                has_D = true;
            }
        }

        if (m_do_single_entry_D_only && m_have_one_D)
        {
            return;
        }
        else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
        {
            m_have_one_D = true;
        }

        if (m_do_lep_presel)
        {
            fill_lepton_histograms(single_entry_channel, sys_at_least_one_D);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
        }
        add_fill_histogram_sys("N_Dplus", single_entry_channel, multiplicity, 20, -0.5, 19.5, true, sys_at_least_one_D);
        if (m_do_single_entry_D_only)
        {
            return;
        }
    }

    // counters for D multiplicity
    std::vector<std::unordered_map<std::string, unsigned int>> multiplicity_counter;
    std::set<std::string> used_channels;

    // loop over D plus mesons
    for (unsigned int i = 0; i < m_D_plus_mesons.size(); i++)
    {
        // retreive the D meson
        std::pair<unsigned int, int> Dplus_pair = m_D_plus_mesons.at(i);

        // truth category
        bool matched_signal = false;
        std::string truth_category = "";
        int truthIndex = -1;
        if (m_truth_matchD && m_is_mc)
        {
            truth_category = TruthInfo::get_truth_category(this, Dplus_pair, "Kpipi", m_fiducial_selection);
            truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Dplus_pair.first));
        }
        if (truth_category == "Matched")
        {
            matched_signal = true;
            m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
            m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
            m_truth_D_mass = truth_daughter_mass(truthIndex);
            m_truth_abs_lep_eta = std::abs(m_truth_lep1.Eta());
            if (m_bin_in_truth_D_pt)
            {
                truth_category += "_truth_pt_bin" + std::to_string(m_differential_bins.get_pt_bin(m_truth_D_pt) + 1);
            }
            else if (m_bin_in_truth_lep_eta)
            {
                truth_category += "_truth_eta_bin" + std::to_string(m_differential_bins_eta.get_eta_bin(m_truth_abs_lep_eta) + 1);
            }
        }
        else
        {
            m_truth_D_pt = 0;
            m_truth_D_eta = -999;
            m_truth_D_mass = 0;
            m_truth_abs_lep_eta = -999;
        }

        // signal only
        if (m_signal_only && !matched_signal)
        {
            continue;
        }

        if (m_debug_dmeson_printout && (truth_category == "HardMisMatched" || truth_category == "MisMatched"))
        {
            std::cout << "~~~ " << truth_category << " detected! ~~~" << std::endl;
            debug_dmeson_printout(Dplus_pair.first);
        }

        // dR w.r.t. the lepton depends on systematic
        std::vector<Float_t> sys_dRlep = std::vector<float>(m_systematics.size() + 1, 0.);

        // D meson mass
        m_sys_dmeson_mass = std::vector<float>(m_systematics.size() + 1, m_DMesons_m->at(Dplus_pair.first));

        // OS or SS region
        std::vector<std::string> charged_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> pass_dR_cut = std::vector<bool>(m_systematics.size() + 1, false);
        std::vector<double> extra_weight = std::vector<double>(m_systematics.size() + 1, 1.0);

        // track-jet
        std::vector<int> sys_track_jet_10_index = std::vector<int>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_10_dR = std::vector<float>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_10_pt = std::vector<float>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_10_zt = std::vector<float>(m_systematics.size() + 1, 0.);

        std::vector<int> sys_track_jet_8_index = std::vector<int>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_8_dR = std::vector<float>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_8_pt = std::vector<float>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_8_zt = std::vector<float>(m_systematics.size() + 1, 0.);

        std::vector<int> sys_track_jet_6_index = std::vector<int>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_6_dR = std::vector<float>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_6_pt = std::vector<float>(m_systematics.size() + 1, 0.);
        std::vector<Float_t> sys_track_jet_6_zt = std::vector<float>(m_systematics.size() + 1, 0.);

        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity_counter.push_back({});
            if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0))
            {
                // dR w.r.t. the lepton
                float dRlep = 999;
                float lep_charge = 0;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Dplus_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                    lep_charge = m_sys_lep_charge.at(j);
                }
                sys_dRlep.at(j) = dRlep;

                // dR cut
                if (dRlep < 0.3)
                {
                    continue;
                }

                // track-jet properties
                if (m_track_jet_selection)
                {
                    calculate_track_jet_properties(Dplus_pair.first, j,
                                                sys_track_jet_10_index,
                                                sys_track_jet_10_dR,
                                                sys_track_jet_10_pt,
                                                sys_track_jet_10_zt,
                                                m_AnalysisAntiKt10PV0TrackJets_pt,
                                                m_AnalysisAntiKt10PV0TrackJets_eta,
                                                m_AnalysisAntiKt10PV0TrackJets_phi);

                    // track-jet properties
                    calculate_track_jet_properties(Dplus_pair.first, j,
                                                sys_track_jet_8_index,
                                                sys_track_jet_8_dR,
                                                sys_track_jet_8_pt,
                                                sys_track_jet_8_zt,
                                                m_AnalysisAntiKt8PV0TrackJets_pt,
                                                m_AnalysisAntiKt8PV0TrackJets_eta,
                                                m_AnalysisAntiKt8PV0TrackJets_phi);

                    // track-jet properties
                    calculate_track_jet_properties(Dplus_pair.first, j,
                                                sys_track_jet_6_index,
                                                sys_track_jet_6_dR,
                                                sys_track_jet_6_pt,
                                                sys_track_jet_6_zt,
                                                m_AnalysisAntiKt6PV0TrackJets_pt,
                                                m_AnalysisAntiKt6PV0TrackJets_eta,
                                                m_AnalysisAntiKt6PV0TrackJets_phi);
                }

                // sys string
                std::string sys_string = "";
                if (j > 0)
                {
                    sys_string = m_systematics.at(j - 1);
                }

                // track efficiency sys weight
                double track_weight = m_track_sys_helper.get_track_eff_weight(sys_string, m_DMesons_pt->at(Dplus_pair.first) * Charm::GeV, m_DMesons_eta->at(Dplus_pair.first), m_DMesons_pdgId->at(Dplus_pair.first));
                extra_weight.at(j) *= track_weight;

                // smear mass
                double smear_mass = m_mass_smear_helper.get_smeared_mass(sys_string, m_differential_bins.get_pt_bin(m_DMesons_pt->at(Dplus_pair.first) * Charm::GeV), m_DMesons_eta->at(Dplus_pair.first), m_DMesons_pdgId->at(Dplus_pair.first), m_DMesons_m->at(Dplus_pair.first)) * Charm::GeV;
                m_sys_dmeson_mass.at(j) = smear_mass;

                // modeling uncertainty
                if (matched_signal)
                {
                    double fid_eff_weight = 1.0;
                    if (m_bin_in_D_pt)
                    {
                        fid_eff_weight = m_modeling_uncertainty_helper.get_weight_pt(sys_string, m_truth_D_pt, "Dplus");
                    }
                    else if (m_bin_in_lep_eta)
                    {
                        fid_eff_weight = m_modeling_uncertainty_helper.get_weight_eta(sys_string, m_truth_abs_lep_eta, "Dplus");
                    }
                    extra_weight.at(j) *= fid_eff_weight;
                }

                // replace lepton charge with the truth D charge for SPG samples
                //  - OS if they have the same charge
                //  - SS if they have the opposite charge
                if (m_data_period == "ForcedDecay")
                {
                    lep_charge = -1 * m_TruthParticles_Selected_pdgId->at(0) / fabs(m_TruthParticles_Selected_pdgId->at(0));
                }

                // D+ branching ratio background uncertainty
                if (truth_category == "411MisMatched" || truth_category == "421MisMatched" || truth_category == "431MisMatched")
                {
                    double br_unc_weight = m_dplus_bkg_br_uncertainty.get_weight(sys_string, m_DMesons_m->at(Dplus_pair.first) * Charm::GeV, truth_category, get_charge_prefix(lep_charge, Dplus_pair.first));
                    extra_weight.at(j) *= br_unc_weight;
                }

                // Charge: OS or SS
                pass_dR_cut.at(j) = true;
                charged_channel.at(j) += "_Dplus" + get_charge_prefix(lep_charge, Dplus_pair.first);
                multiplicity_counter.at(j)[charged_channel.at(j)]++;
                used_channels.insert(charged_channel.at(j));

                // weight for SPG forced decay sample
                if (matched_signal && m_reweight_spg && m_data_period == "ForcedDecay")
                {
                    extra_weight.at(j) *= m_spg_forced_decay_helper.get_weight(m_DMesons_m->at(Dplus_pair.first) * Charm::GeV,
                                                                               m_differential_bins.get_pt_bin(m_truth_D_pt), "Dplus");
                }
            }
        }

        // lepton histograms for this channel
        if (m_do_lep_presel && Dplus_pair.second == 0)
        {
            fill_lepton_histograms(charged_channel, pass_dR_cut, extra_weight);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(charged_channel, pass_dR_cut, extra_weight);
        }

        // D plus meson specific histograms
        if (m_do_cutScan == 0)
        {
            if (!m_fit_variables_only)
            {
                add_fill_histogram_sys("Dmeson_dRlep", charged_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut, extra_weight);
                if (m_track_jet_selection)
                {
                    add_fill_histogram_sys("Dmeson_track_jet_10_dR", charged_channel, sys_track_jet_10_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_10_pt", charged_channel, sys_track_jet_10_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_10_zt", charged_channel, sys_track_jet_10_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_8_dR", charged_channel, sys_track_jet_8_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_8_pt", charged_channel, sys_track_jet_8_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_8_zt", charged_channel, sys_track_jet_8_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_6_dR", charged_channel, sys_track_jet_6_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_6_pt", charged_channel, sys_track_jet_6_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_track_jet_6_zt", charged_channel, sys_track_jet_6_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                    if (fabs(m_DMesons_m->at(Dplus_pair.first) - Charm::DP_MASS) < 70)
                    {
                        add_fill_histogram_sys("Dmeson_peak_m", charged_channel, m_sys_dmeson_mass, 200, 1.4, 2.4, false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_pt", charged_channel, m_DMesons_pt->at(Dplus_pair.first) * Charm::GeV, 150, 0, 150., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_10_dR", charged_channel, sys_track_jet_10_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_10_pt", charged_channel, sys_track_jet_10_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_10_zt", charged_channel, sys_track_jet_10_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_8_dR", charged_channel, sys_track_jet_8_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_8_pt", charged_channel, sys_track_jet_8_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_8_zt", charged_channel, sys_track_jet_8_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_6_dR", charged_channel, sys_track_jet_6_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_6_pt", charged_channel, sys_track_jet_6_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_peak_track_jet_6_zt", charged_channel, sys_track_jet_6_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                    }
                }
            }
            DMesonRec::fill_D_plus_histograms(this, charged_channel, Dplus_pair, pass_dR_cut, extra_weight);
        }
        else
        {
            DMesonRec::fill_D_plus_histograms_scan(this, charged_channel, Dplus_pair, pass_dR_cut, extra_weight);
        }

        // D meson truth match
        if (m_is_mc && m_truth_matchD)
        {
            std::vector<std::string> truth_channel = charged_channel;
            std::vector<std::string> truth_channel_fid = std::vector<std::string>(m_systematics.size() + 1, channel);

            for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
            {
                // don't save AntiTight truth categories
                if (m_do_lep_presel && m_sys_anti_tight.at(j))
                {
                    pass_dR_cut.at(j) = false;
                }

                if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 && pass_dR_cut.at(j)))
                {
                    truth_channel.at(j) += "_" + truth_category;
                    multiplicity_counter.at(j)[truth_channel.at(j)]++;
                    used_channels.insert(truth_channel.at(j));
                }
            }
            if (m_do_cutScan == 0)
            {
                if (m_do_lep_presel && Dplus_pair.second == 0)
                {
                    fill_lepton_histograms(truth_channel, pass_dR_cut, extra_weight);
                }
                if (m_do_PV)
                {
                    fill_PV_histograms(truth_channel, pass_dR_cut, extra_weight);
                }
                if (!m_fit_variables_only)
                {
                    add_fill_histogram_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100, -3.0, 3.0, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_mass", truth_channel, m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_lep_eta", truth_channel, m_truth_abs_lep_eta, 25, 0, 2.5, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_dRlep", truth_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut, extra_weight);
                    if (m_track_jet_selection)
                    {
                        add_fill_histogram_sys("Dmeson_track_jet_10_dR", truth_channel, sys_track_jet_10_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_10_pt", truth_channel, sys_track_jet_10_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_10_zt", truth_channel, sys_track_jet_10_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_8_dR", truth_channel, sys_track_jet_8_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_8_pt", truth_channel, sys_track_jet_8_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_8_zt", truth_channel, sys_track_jet_8_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_6_dR", truth_channel, sys_track_jet_6_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_6_pt", truth_channel, sys_track_jet_6_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                        add_fill_histogram_sys("Dmeson_track_jet_6_zt", truth_channel, sys_track_jet_6_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                        if (fabs(m_DMesons_m->at(Dplus_pair.first) - Charm::DP_MASS) < 70)
                        {
                            add_fill_histogram_sys("Dmeson_peak_m", truth_channel, m_sys_dmeson_mass, 200, 1.4, 2.4, false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_pt", truth_channel, m_DMesons_pt->at(Dplus_pair.first) * Charm::GeV, 150, 0, 150., false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_10_dR", truth_channel, sys_track_jet_10_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_10_pt", truth_channel, sys_track_jet_10_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_10_zt", truth_channel, sys_track_jet_10_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_8_dR", truth_channel, sys_track_jet_8_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_8_pt", truth_channel, sys_track_jet_8_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_8_zt", truth_channel, sys_track_jet_8_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_6_dR", truth_channel, sys_track_jet_6_dR, 120, 0., 6., false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_6_pt", truth_channel, sys_track_jet_6_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                            add_fill_histogram_sys("Dmeson_peak_track_jet_6_zt", truth_channel, sys_track_jet_6_zt, 120, 0., 1.2, false, pass_dR_cut, extra_weight);
                        }
                    }
                }
                if (m_unfolding_variables)
                {
                    // transfer matrix for pT(D)
                    add_fill_histogram_sys("Dmeson_transfer_matrix", truth_channel, m_DMesons_pt->at(Dplus_pair.first) * Charm::GeV,
                                           m_truth_D_pt,
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_truth_differential_pt", truth_channel, m_truth_D_pt,
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_differential_pt", truth_channel, m_DMesons_pt->at(Dplus_pair.first) * Charm::GeV,
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);

                    // transfer matrix for eta(lepton)
                    add_fill_histogram_sys("Dmeson_transfer_matrix_eta", truth_channel, m_sys_lep_abs_eta,
                                           m_truth_abs_lep_eta,
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_truth_differential_lep_eta", truth_channel, m_sys_lep_abs_eta,
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_differential_lep_eta", truth_channel, m_truth_abs_lep_eta,
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                }
                DMesonRec::fill_D_plus_histograms(this, truth_channel, Dplus_pair, pass_dR_cut, extra_weight);
            }
            else
            {
                DMesonRec::fill_D_plus_histograms_scan(this, truth_channel, Dplus_pair, pass_dR_cut, extra_weight);
            }

            // Add truth matched hist if requested
            if (m_truth_D && matched_signal && m_is_mc)
            {
                int truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Dplus_pair.first));
                std::string truth_channel;
                // accounting for presence of neutrals in truth D+ decay mode (RM = Right Mode)
                if (fabs(truth_daughter_mass(truthIndex) - DP_MASS) < 40)
                {
                    truth_channel = channel + "_recoMatch_truth_Dplus_RM" + std::to_string(Dplus_pair.second);
                    TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
                }
                truth_channel = channel + "_Dplus_recoMatch_truth" + std::to_string(Dplus_pair.second);
                TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
            }
        }
    }

    for (auto channel : used_channels)
    {
        std::vector<float> multiplicity;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity.push_back(multiplicity_counter.at(j)[channel]);
        }
        if (!m_fit_variables_only)
        {
            add_fill_histogram_sys("N_Dplus", channel, multiplicity, 20, -0.5, 19.5, true);
        }
    }

    // pure D meson truth
    if (m_is_mc && m_truth_D)
    {
        for (unsigned int i = 0; i < m_truth_D_plus_mesons.size(); i++)
        {
            unsigned int index = m_truth_D_plus_mesons.at(i);
            std::string truth_channel;
            // accounting for presence of neutrals in truth D+ decay mode (RM = Right Mode)
            if (fabs(truth_daughter_mass(index) - DP_MASS) < 40)
            {
                truth_channel = channel + "_truth_Dplus_RM";
                TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
            }
            truth_channel = channel + "_Dplus_truth";
            TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
            if (fabs(truth_daughter_mass(index) - DP_MASS) < 40)  // && pass_track_truth(index))
                TruthInfo::fill_truth_D_histograms(this, channel + "_truth_Dplus_RM_passTrackTruth", index);
        }
    }
}

void WQCDCharmLoopBase::fill_D_star_histograms(std::string channel)
{
    // number of D mesons
    if (m_fill_wjets && !m_fit_variables_only)
    {
        add_fill_histogram_sys("N_Dstar", channel, m_D_star_mesons.size(), 20, -0.5, 19.5, true);
    }

    // single entry region with at least one D meson
    if (m_do_single_entry_D && m_D_star_mesons.size())
    {
        std::vector<std::string> single_entry_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_systematics.size() + 1, false);
        std::vector<float> multiplicity = std::vector<float>(m_systematics.size() + 1, 0);
        bool has_D = false;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            // loop over D star mesons
            for (unsigned int i = 0; i < m_D_star_mesons.size(); i++)
            {
                // retreive the D meson
                std::pair<unsigned int, int> Dstar_pair = m_D_star_mesons.at(i);

                // dR cut
                float dRlep = 999;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Dstar_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                }
                if (dRlep >= 0.3)
                {
                    sys_at_least_one_D.at(j) = true;
                    multiplicity.at(j) += 1;
                }
            }
            if (multiplicity.at(j) > 0)
            {
                single_entry_channel.at(j) += "_Dstar";
                has_D = true;
            }
        }

        if (m_do_single_entry_D_only && m_have_one_D)
        {
            return;
        }
        else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
        {
            m_have_one_D = true;
        }

        if (m_do_lep_presel)
        {
            fill_lepton_histograms(single_entry_channel, sys_at_least_one_D);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
        }
        add_fill_histogram_sys("N_Dstar", single_entry_channel, multiplicity, 20, -0.5, 19.5, true, sys_at_least_one_D);
        if (m_do_single_entry_D_only)
        {
            return;
        }
    }

    // counters for D multiplicity
    std::vector<std::unordered_map<std::string, unsigned int>> multiplicity_counter;
    std::set<std::string> used_channels;

    // loop over D star mesons
    for (unsigned int i = 0; i < m_D_star_mesons.size(); i++)
    {
        // retreive the D meson
        std::pair<unsigned int, int> Dstar_pair = m_D_star_mesons.at(i);

        // truth category
        bool matched_signal = false;
        std::string truth_category = "";
        int truthIndex = -1;
        if (m_truth_matchD && m_is_mc)
        {
            truth_category = TruthInfo::get_truth_category(this, Dstar_pair, "Kpipi", m_fiducial_selection);
            truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Dstar_pair.first));
        }
        if (truth_category == "Matched")
        {
            matched_signal = true;
            m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
            m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
            m_truth_D_mass = truth_daughter_mass(truthIndex);
            m_truth_abs_lep_eta = std::abs(m_truth_lep1.Eta());
            if (m_bin_in_truth_D_pt)
            {
                truth_category += "_truth_pt_bin" + std::to_string(m_differential_bins.get_pt_bin(m_truth_D_pt) + 1);
            }
            else if (m_bin_in_truth_lep_eta)
            {
                truth_category += "_truth_eta_bin" + std::to_string(m_differential_bins_eta.get_eta_bin(m_truth_abs_lep_eta) + 1);
            }
        }
        else
        {
            m_truth_D_pt = 0;
            m_truth_D_eta = -999;
            m_truth_D_mass = 0;
            m_truth_abs_lep_eta = -999;
        }

        // signal only
        if (m_signal_only && !matched_signal)
        {
            continue;
        }

        // dR w.r.t. the lepton depends on systematic
        std::vector<Float_t> sys_dRlep = std::vector<float>(m_systematics.size() + 1, 0.);

        // dR w.r.t. the b-jet depends on systematic
        std::vector<Float_t> sys_dRbjet = std::vector<float>(m_systematics.size() + 1, 0.);

        // D meson mass
        m_sys_dmeson_mass = std::vector<float>(m_systematics.size() + 1, m_DMesons_DeltaMass->at(Dstar_pair.first));

        // OS or SS region
        std::vector<std::string> charged_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> pass_dR_cut = std::vector<bool>(m_systematics.size() + 1, false);
        std::vector<double> extra_weight = std::vector<double>(m_systematics.size() + 1, 1.0);

        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity_counter.push_back({});
            if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0))
            {
                // dR w.r.t. the lepton
                float dRlep = 999;
                float lep_charge = 0;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Dstar_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                    lep_charge = m_sys_lep_charge.at(j);
                }
                sys_dRlep.at(j) = dRlep;

                // dR cut
                if (dRlep < 0.3)
                {
                    continue;
                }

                // dR w.r.t. b-jets
                float dRbjet = 999;

                if (m_jet_selection && m_do_lep_presel)
                {
                    auto jets = m_jets_sys.at(j);
                    for (auto &jet : jets)
                    {
                        if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet))
                        {
                            float dR = get_dr_bjet_Dmeson(jet, Dstar_pair.first);
                            if (dR < dRbjet)
                            {
                                dRbjet = dR;
                            }
                        }
                    }
                    sys_dRbjet.at(j) = dRbjet;
                }

                // sys string
                std::string sys_string = "";
                if (j > 0)
                {
                    sys_string = m_systematics.at(j - 1);
                }

                // track efficiency sys weight
                double track_weight = m_track_sys_helper.get_track_eff_weight(sys_string, m_DMesons_pt->at(Dstar_pair.first) * Charm::GeV, m_DMesons_eta->at(Dstar_pair.first), m_DMesons_pdgId->at(Dstar_pair.first));
                extra_weight.at(j) *= track_weight;

                // smear mass
                double smear_mdiff = m_mass_smear_helper.get_smeared_mass(sys_string, m_differential_bins.get_pt_bin(m_DMesons_pt->at(Dstar_pair.first) * Charm::GeV), m_DMesons_eta->at(Dstar_pair.first), m_DMesons_pdgId->at(Dstar_pair.first), m_DMesons_DeltaMass->at(Dstar_pair.first));
                m_sys_dmeson_mass.at(j) = smear_mdiff;

                // modeling uncertainty
                if (matched_signal)
                {
                    double fid_eff_weight = 1.0;
                    if (m_bin_in_D_pt)
                    {
                        fid_eff_weight = m_modeling_uncertainty_helper.get_weight_pt(sys_string, m_truth_D_pt, "Dstar");
                    }
                    else if (m_bin_in_lep_eta)
                    {
                        fid_eff_weight = m_modeling_uncertainty_helper.get_weight_eta(sys_string, m_truth_abs_lep_eta, "Dstar");
                    }
                    extra_weight.at(j) *= fid_eff_weight;
                }
                // weight for SPG forced decay sample
                if (matched_signal && m_reweight_spg && m_data_period == "ForcedDecay")
                {
                    extra_weight.at(j) *= m_spg_forced_decay_helper.get_weight(m_DMesons_DeltaMass->at(Dstar_pair.first) * Charm::MeV,
                                                                               m_differential_bins.get_pt_bin(m_truth_D_pt), "Dstar");
                }

                // replace lepton charge with the truth D charge for SPG samples
                //  - OS if they have the same charge
                //  - SS if they have the opposite charge
                if (m_data_period == "ForcedDecay")
                {
                    lep_charge = -1 * m_TruthParticles_Selected_pdgId->at(0) / fabs(m_TruthParticles_Selected_pdgId->at(0));
                }
                pass_dR_cut.at(j) = true;
                charged_channel.at(j) += "_Dstar" + get_charge_prefix(lep_charge, Dstar_pair.first);
                multiplicity_counter.at(j)[charged_channel.at(j)]++;
                used_channels.insert(charged_channel.at(j));
            }
        }

        // lepton histograms for this channel
        if (m_do_lep_presel && Dstar_pair.second == 0)
        {
            fill_lepton_histograms(charged_channel, pass_dR_cut, extra_weight);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(charged_channel, pass_dR_cut, extra_weight);
        }

        // D star meson specific histograms
        if (m_do_cutScan == 0)
        {
            if (!m_fit_variables_only)
            {
                add_fill_histogram_sys("Dmeson_dRlep", charged_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut, extra_weight);
            }
            DMesonRec::fill_D_star_histograms(this, charged_channel, Dstar_pair, pass_dR_cut, extra_weight);
        }
        else
        {
            DMesonRec::fill_D_star_histograms_scan(this, charged_channel, Dstar_pair, pass_dR_cut, extra_weight);
        }

        // D meson truth match
        if (m_is_mc && m_truth_matchD)
        {
            std::vector<std::string> truth_channel = charged_channel;
            std::vector<std::string> truth_channel_fid = std::vector<std::string>(m_systematics.size() + 1, channel);

            for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
            {
                if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 && pass_dR_cut.at(j)))
                {
                    truth_channel.at(j) += "_" + truth_category;
                    multiplicity_counter.at(j)[truth_channel.at(j)]++;
                    used_channels.insert(truth_channel.at(j));
                }
            }

            if (m_do_cutScan == 0)
            {
                if (m_do_lep_presel && Dstar_pair.second == 0)
                {
                    fill_lepton_histograms(truth_channel, pass_dR_cut);
                }
                if (!m_fit_variables_only)
                {
                    add_fill_histogram_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100, -3.0, 3.0, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_mass", truth_channel, m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_lep_eta", truth_channel, m_truth_abs_lep_eta, 25, 0, 2.5, false, pass_dR_cut, extra_weight);
                    //add_fill_histogram_sys("Dmeson_dRlep", truth_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut, extra_weight);
                    //add_fill_histogram_sys("Dmeson_dRbjet", truth_channel, sys_dRbjet, 120, 0., 6., false, pass_dR_cut, extra_weight);
                }
                if (m_unfolding_variables)
                {
                    // transfer matrix
                    add_fill_histogram_sys("Dmeson_transfer_matrix", truth_channel, m_DMesons_pt->at(Dstar_pair.first) * Charm::GeV,
                                           m_truth_D_pt,
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_truth_differential_pt", truth_channel, m_truth_D_pt,
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_differential_pt", truth_channel, m_DMesons_pt->at(Dstar_pair.first) * Charm::GeV,
                                           m_differential_bins.get_n_differential(), m_differential_bins.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);

                    // transfer matrix for eta(lepton)
                    add_fill_histogram_sys("Dmeson_transfer_matrix_eta", truth_channel, m_sys_lep_abs_eta,
                                           m_truth_abs_lep_eta,
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_truth_differential_lep_eta", truth_channel, m_sys_lep_abs_eta,
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("Dmeson_differential_lep_eta", truth_channel, m_truth_abs_lep_eta,
                                           m_differential_bins_eta.get_n_differential(), m_differential_bins_eta.get_differential_bins(),
                                           true, pass_dR_cut, extra_weight);
                }
                DMesonRec::fill_D_star_histograms(this, truth_channel, Dstar_pair, pass_dR_cut, extra_weight);
            }
            else
            {
                DMesonRec::fill_D_star_histograms_scan(this, truth_channel, Dstar_pair, pass_dR_cut, extra_weight);
            }
            // Add truth matched hist if requested
            if (m_truth_D && matched_signal && m_is_mc)
            {
                int truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Dstar_pair.first));
                std::string truth_channel;
                // accounting for presence of neutrals in truth D+ decay mode (RM = Right Mode)
                if (fabs(truth_daughter_mass(truthIndex) - DSTAR_MASS) < 40)
                    truth_channel = channel + "_recoMatch_truth_Dstar_RM" + std::to_string(Dstar_pair.second);
                else
                    truth_channel = channel + "_Dstar_recoMatch_truth" + std::to_string(Dstar_pair.second);
                TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
            }
        }
    }

    // number of D meson candidates
    for (auto channel : used_channels)
    {
        std::vector<float> multiplicity;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity.push_back(multiplicity_counter.at(j)[channel]);
        }
        if (!m_fit_variables_only)
        {
            add_fill_histogram_sys("N_Dstar", channel, multiplicity, 20, -0.5, 19.5, true);
        }
    }

    // pure D meson truth
    if (m_is_mc && m_truth_D)
    {
        for (unsigned int i = 0; i < m_truth_D_star_mesons.size(); i++)
        {
            unsigned int index = m_truth_D_star_mesons.at(i);
            std::string truth_channel;
            if (fabs(truth_daughter_mass(index) - DSTAR_MASS) < 40)
            {
                truth_channel = channel + "_truth_Dstar_RM";
                TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
            }
            truth_channel = channel + "_truth_Dstar";
            TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
        }
    }
}

void WQCDCharmLoopBase::fill_D_star_pi0_histograms(std::string channel)
{
    // number of D mesons
    if (m_fill_wjets && !m_fit_variables_only)
    {
        add_fill_histogram_sys("N_DstarKPiPi0", channel, m_D_star_pi0_mesons.size(), 20, -0.5, 19.5, true);
    }

    // single entry region with at least one D meson
    if (m_do_single_entry_D && m_D_star_pi0_mesons.size())
    {
        std::vector<std::string> single_entry_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_systematics.size() + 1, false);
        std::vector<float> multiplicity = std::vector<float>(m_systematics.size() + 1, 0);
        bool has_D = false;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            // loop over D star D*pi0 mesons
            for (unsigned int i = 0; i < m_D_star_pi0_mesons.size(); i++)
            {
                // retreive the D meson
                std::pair<unsigned int, int> Dstar_pair = m_D_star_pi0_mesons.at(i);

                // dR cut
                float dRlep = 999;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Dstar_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                }
                if (dRlep >= 0.3)
                {
                    sys_at_least_one_D.at(j) = true;
                    multiplicity.at(j) += 1;
                }
            }
            if (multiplicity.at(j) > 0)
            {
                single_entry_channel.at(j) += "_DstarKPiPi0";
                has_D = true;
            }
        }
        if (m_do_single_entry_D_only && m_have_one_D)
            return;
        else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
            m_have_one_D = true;

        if (m_do_lep_presel)
        {
            fill_lepton_histograms(single_entry_channel, sys_at_least_one_D);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
        }
        add_fill_histogram_sys("N_DstarKPiPi0", single_entry_channel, multiplicity, 20, -0.5, 19.5, true, sys_at_least_one_D);
        if (m_do_single_entry_D_only)
        {
            return;
        }
    }

    // counters for D multiplicity
    std::vector<std::unordered_map<std::string, unsigned int>> multiplicity_counter;
    std::set<std::string> used_channels;

    // loop over D star mesons
    for (unsigned int i = 0; i < m_D_star_pi0_mesons.size(); i++)
    {
        // retreive the D meson
        std::pair<unsigned int, int> Dstar_pair = m_D_star_pi0_mesons.at(i);

        // truth category
        bool matched_signal = false;
        std::string truth_category = "";
        int truthIndex = -1;
        if (m_truth_matchD && m_is_mc)
        {
            truth_category = TruthInfo::get_truth_category(this, Dstar_pair, "Kpipipi0", m_fiducial_selection);
            truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Dstar_pair.first));
        }
        if (truth_category == "Matched")
        {
            m_truth_D_pt = m_TruthParticles_Selected_pt->at(truthIndex) * Charm::GeV;
            m_truth_D_eta = m_TruthParticles_Selected_eta->at(truthIndex);
            m_truth_D_mass = truth_daughter_mass(truthIndex);
            m_truth_abs_lep_eta = std::abs(m_truth_lep1.Eta());
            if (m_bin_in_truth_D_pt)
            {
                truth_category += "_truth_pt_bin" + std::to_string(m_differential_bins.get_pt_bin(m_truth_D_pt) + 1);
            }
            else if (m_bin_in_truth_lep_eta)
            {
                truth_category += "_truth_eta_bin" + std::to_string(m_differential_bins_eta.get_eta_bin(m_truth_abs_lep_eta) + 1);
            }
        }
        else
        {
            m_truth_D_pt = 0;
            m_truth_D_eta = -999;
            m_truth_D_mass = 0;
            m_truth_abs_lep_eta = -999;
        }

        // signal only
        if (m_signal_only && !matched_signal)
        {
            continue;
        }

        // dR w.r.t. the lepton depends on systematic
        std::vector<Float_t> sys_dRlep = std::vector<float>(m_systematics.size() + 1, 0.);

        // dR w.r.t. the b-jet depends on systematic
        std::vector<Float_t> sys_dRbjet = std::vector<float>(m_systematics.size() + 1, 0.);

        // OS or SS region
        std::vector<std::string> charged_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> pass_dR_cut = std::vector<bool>(m_systematics.size() + 1, false);
        std::vector<double> extra_weight = std::vector<double>(m_systematics.size() + 1, 1.0);

        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity_counter.push_back({});
            if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0))
            {
                // dR w.r.t. the lepton
                float dRlep = 999;
                float lep_charge = 0;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Dstar_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                    lep_charge = m_sys_lep_charge.at(j);
                }
                sys_dRlep.at(j) = dRlep;

                // dR cut
                if (dRlep < 0.3)
                {
                    continue;
                }

                // dR w.r.t. b-jets
                float dRbjet = 999;

                if (m_jet_selection && m_do_lep_presel)
                {
                    auto jets = m_jets_sys.at(j);
                    for (auto &jet : jets)
                    {
                        if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet))
                        {
                            float dR = get_dr_bjet_Dmeson(jet, Dstar_pair.first);
                            if (dR < dRbjet)
                            {
                                dRbjet = dR;
                            }
                        }
                    }
                    sys_dRbjet.at(j) = dRbjet;
                }

                // sys string
                std::string sys_string = "";
                if (j > 0)
                {
                    sys_string = m_systematics.at(j - 1);
                }

                // track efficiency sys weight
                double track_weight = m_track_sys_helper.get_track_eff_weight(sys_string, m_DMesons_pt->at(Dstar_pair.first) * Charm::GeV, m_DMesons_eta->at(Dstar_pair.first), m_DMesons_pdgId->at(Dstar_pair.first));
                extra_weight.at(j) *= track_weight;

                // modeling uncertainty
                if (matched_signal)
                {
                    double fid_eff_weight = 1.0;
                    if (m_bin_in_D_pt)
                    {
                        fid_eff_weight = m_modeling_uncertainty_helper.get_weight_pt(sys_string, m_truth_D_pt, "Dstar");
                    }
                    else if (m_bin_in_lep_eta)
                    {
                        fid_eff_weight = m_modeling_uncertainty_helper.get_weight_eta(sys_string, m_truth_abs_lep_eta, "Dstar");
                    }
                    extra_weight.at(j) *= fid_eff_weight;
                }
                // weight for SPG forced decay sample
                if (matched_signal && m_reweight_spg && m_data_period == "ForcedDecay")
                {
                    extra_weight.at(j) *= m_spg_forced_decay_helper.get_weight(m_DMesons_m->at(Dstar_pair.first) * Charm::GeV,
                                                                               m_differential_bins.get_pt_bin(m_truth_D_pt), "Dstar");
                }

                // replace lepton charge with the truth D charge for SPG samples
                //  - OS if they have the same charge
                //  - SS if they have the opposite charge
                if (m_data_period == "ForcedDecay")
                {
                    lep_charge = -1 * m_TruthParticles_Selected_pdgId->at(0) / fabs(m_TruthParticles_Selected_pdgId->at(0));
                }
                pass_dR_cut.at(j) = true;
                charged_channel.at(j) += "_DstarKPiPi0" + get_charge_prefix(lep_charge, Dstar_pair.first);
                multiplicity_counter.at(j)[charged_channel.at(j)]++;
                used_channels.insert(charged_channel.at(j));
            }
        }

        // lepton histograms for this channel
        if (m_do_lep_presel && Dstar_pair.second == 0)
        {
            fill_lepton_histograms(charged_channel, pass_dR_cut, extra_weight);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(charged_channel, pass_dR_cut, extra_weight);
        }

        // D star pi0 meson specific histograms
        if (m_do_cutScan == 0)
        {
            if (!m_fit_variables_only)
            {
                add_fill_histogram_sys("Dmeson_dRlep", charged_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut, extra_weight);
            }
            DMesonRec::fill_D_star_pi0_histograms(this, charged_channel, Dstar_pair, pass_dR_cut, extra_weight);
        }
        else
        {
            DMesonRec::fill_D_star_histograms_scan(this, charged_channel, Dstar_pair, pass_dR_cut, extra_weight);
        }

        // D meson truth match
        if (m_is_mc && m_truth_matchD)
        {
            std::vector<std::string> truth_channel = charged_channel;
            std::vector<std::string> truth_channel_fid = std::vector<std::string>(m_systematics.size() + 1, channel);

            for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
            {
                if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 && pass_dR_cut.at(j)))
                {
                    truth_channel.at(j) += "_" + truth_category;
                    multiplicity_counter.at(j)[truth_channel.at(j)]++;
                    used_channels.insert(truth_channel.at(j));
                }
            }

            if (m_do_cutScan == 0)
            {
                if (m_do_lep_presel && Dstar_pair.second == 0)
                {
                    fill_lepton_histograms(truth_channel, pass_dR_cut, extra_weight);
                }
                if (!m_fit_variables_only)
                {
                    add_fill_histogram_sys("truth_Dmeson_pt", truth_channel, m_truth_D_pt, 150, 0, 150., false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_eta", truth_channel, m_truth_D_eta, 100, -3.0, 3.0, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_mass", truth_channel, m_truth_D_mass * Charm::GeV, 200, 1.4, 2.4, false, pass_dR_cut, extra_weight);
                    add_fill_histogram_sys("truth_Dmeson_lep_eta", truth_channel, m_truth_abs_lep_eta, 25, 0, 2.5, false, pass_dR_cut, extra_weight);
                    //add_fill_histogram_sys("Dmeson_dRlep", truth_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut, extra_weight);
                    //add_fill_histogram_sys("Dmeson_dRbjet", truth_channel, sys_dRbjet, 120, 0., 6., false, pass_dR_cut, extra_weight);
                }
                DMesonRec::fill_D_star_pi0_histograms(this, truth_channel, Dstar_pair, pass_dR_cut, extra_weight);
            }
            else
            {
                DMesonRec::fill_D_star_histograms_scan(this, truth_channel, Dstar_pair, pass_dR_cut, extra_weight);
            }
            // Add truth matched hist if requested
            if (m_truth_D && truth_category == "Matched" && m_is_mc)
            {
                int truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Dstar_pair.first));
                std::string truth_channel;
                // accounting for presence of neutrals in truth D*pi0 decay mode (RM = Right Mode)
                if (fabs(truth_daughter_mass(truthIndex) - DSTAR_MASS) < 40)
                    truth_channel = channel + "_recoMatch_truth_DstarKPiPi0_RM" + std::to_string(Dstar_pair.second);
                else
                    truth_channel = channel + "_DstarKPiPi0_recoMatch_truth" + std::to_string(Dstar_pair.second);
                TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
            }
        }
    }

    // number of D meson candidates
    for (auto channel : used_channels)
    {
        std::vector<float> multiplicity;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity.push_back(multiplicity_counter.at(j)[channel]);
        }
        if (!m_fit_variables_only)
        {
            add_fill_histogram_sys("N_DstarKPiPi0", channel, multiplicity, 20, -0.5, 19.5, true);
        }
    }

    // pure D meson truth
    if (m_is_mc && m_truth_D)
    {
        for (unsigned int i = 0; i < m_truth_D_star_pi0_mesons.size(); i++)
        {
            unsigned int index = m_truth_D_star_pi0_mesons.at(i);
            std::string truth_channel;
            if (fabs(truth_daughter_mass(index) - DSTAR_MASS) < 40)
            {
                truth_channel = channel + "_truth_DstarKPiPi0_RM";
                TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
            }
            truth_channel = channel + "_truth_DstarKPiPi0";
            TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
        }
    }
}

void WQCDCharmLoopBase::fill_D_s_histograms(std::string channel)
{
    // number of D mesons
    if (m_fill_wjets && !m_fit_variables_only)
    {
        add_fill_histogram_sys("N_Ds", channel, m_D_s_mesons.size(), 20, -0.5, 19.5, true);
    }

    // single entry region with at least one D meson
    if (m_do_single_entry_D)
    {
        std::vector<std::string> single_entry_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> sys_at_least_one_D = std::vector<bool>(m_systematics.size() + 1, false);
        std::vector<float> multiplicity = std::vector<float>(m_systematics.size() + 1, 0);
        bool has_D = true;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            // loop over D s mesons
            for (unsigned int i = 0; i < m_D_s_mesons.size(); i++)
            {
                // retreive the D meson
                std::pair<unsigned int, int> Ds_pair = m_D_s_mesons.at(i);

                // dR cut
                float dRlep = 999;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Ds_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                }
                if (dRlep >= 0.3)
                {
                    sys_at_least_one_D.at(j) = true;
                    multiplicity.at(j) += 1;
                }
            }
            if (multiplicity.at(j) > 0)
            {
                single_entry_channel.at(j) += "_Ds";
                has_D = true;
            }
        }
        if (m_do_single_entry_D_only && m_have_one_D)
            return;
        else if (m_do_single_entry_D_only && !m_have_one_D && has_D)
            m_have_one_D = true;

        if (m_do_lep_presel)
        {
            fill_lepton_histograms(single_entry_channel, sys_at_least_one_D);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(single_entry_channel, sys_at_least_one_D);
        }
        add_fill_histogram_sys("N_Ds", single_entry_channel, multiplicity, 20, -0.5, 19.5, true, sys_at_least_one_D);
        if (m_do_single_entry_D_only)
        {
            return;
        }
    }

    // counters for D multiplicity
    std::vector<std::unordered_map<std::string, unsigned int>> multiplicity_counter;
    std::set<std::string> used_channels;

    // loop over D s mesons
    for (unsigned int i = 0; i < m_D_s_mesons.size(); i++)
    {
        // retreive the D meson
        std::pair<unsigned int, int> Ds_pair = m_D_s_mesons.at(i);

        // dR w.r.t. the lepton depends on systematic
        std::vector<Float_t> sys_dRlep = std::vector<float>(m_systematics.size() + 1, 0.);

        // OS or SS region
        std::vector<std::string> charged_channel = std::vector<std::string>(m_systematics.size() + 1, channel);
        std::vector<bool> pass_dR_cut = std::vector<bool>(m_systematics.size() + 1, false);
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity_counter.push_back({});
            if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0))
            {
                // dR w.r.t. the lepton
                float dRlep = 999;
                float lep_charge = 0;
                if (m_do_lep_presel)
                {
                    dRlep = DMesonRec::D_meson_lepton_dR(Ds_pair.first, m_sys_lep_eta.at(j), m_sys_lep_phi.at(j));
                    lep_charge = m_sys_lep_charge.at(j);
                }
                sys_dRlep.at(j) = dRlep;

                // dR cut
                if (dRlep < 0.3)
                {
                    continue;
                }

                pass_dR_cut.at(j) = true;
                charged_channel.at(j) += "_Ds" + get_charge_prefix(lep_charge, Ds_pair.first);
                multiplicity_counter.at(j)[charged_channel.at(j)]++;
                used_channels.insert(charged_channel.at(j));
            }
        }

        // // Sideband or central region
        // if (m_split_into_SB) {
        //     std::string region_name = Charm::get_D_s_mass_region(m_DMesons_m->at(Ds_pair.first));
        //     if (region_name == "") {
        //         continue;
        //     }
        //     charged_channel += "_" + region_name;
        // }

        // D s meson specific histograms
        // lepton histograms for this channel
        if (m_do_lep_presel && Ds_pair.second == 0)
        {
            fill_lepton_histograms(charged_channel, pass_dR_cut);
        }
        if (m_do_PV)
        {
            fill_PV_histograms(charged_channel, pass_dR_cut);
        }

        if (m_do_cutScan == 0)
        {
            if (!m_fit_variables_only)
            {
                add_fill_histogram_sys("Dmeson_dRlep", charged_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut);
            }
            DMesonRec::fill_D_s_histograms(this, charged_channel, Ds_pair, pass_dR_cut);
        }
        else
        {
            DMesonRec::fill_D_s_histograms_scan(this, charged_channel, Ds_pair, pass_dR_cut);
        }

        // D meson truth match
        if (m_is_mc && m_truth_matchD)
        {
            std::string truth_category = TruthInfo::get_truth_category(this, Ds_pair);
            std::vector<std::string> truth_channel = charged_channel;
            for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
            {
                if (!m_do_lep_presel || (event_pass_sys(j) && m_sys_lep_charge.at(j) != 0 && pass_dR_cut.at(j)))
                {
                    truth_channel.at(j) += "_" + truth_category;
                    multiplicity_counter.at(j)[truth_channel.at(j)]++;
                    used_channels.insert(truth_channel.at(j));
                }
            }
            if (m_do_cutScan == 0)
            {
                if (m_do_lep_presel && Ds_pair.second == 0)
                {
                    fill_lepton_histograms(truth_channel, pass_dR_cut);
                }
                if (!m_fit_variables_only)
                {
                    add_fill_histogram_sys("Dmeson_dRlep", truth_channel, sys_dRlep, 120, 0., 6., false, pass_dR_cut);
                }
                DMesonRec::fill_D_s_histograms(this, truth_channel, Ds_pair, pass_dR_cut);
            }
            else
                DMesonRec::fill_D_plus_histograms_scan(this, truth_channel, Ds_pair, pass_dR_cut);
            // Add truth matched hist if requested
            if (m_truth_D && truth_category == "Matched")
            {
                int truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Ds_pair.first));
                std::string truth_channel;
                // accounting for presence of neutrals in truth D+ decay mode (RM = Right Mode)
                if (fabs(truth_daughter_mass(truthIndex) - DS_MASS) < 40)
                {
                    truth_channel = channel + "_recoMatch_truth_Ds_RM" + std::to_string(Ds_pair.second);
                    TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
                }
                truth_channel = channel + "_Ds_recoMatch_truth" + std::to_string(Ds_pair.second);
                TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
            }
            // Add truth matched hist if requested
            // if(m_truth_D && truth_category == "Matched"){
            //     int truthIndex = TruthInfo::getMatchedBarcode(m_DMesons_truthBarcode->at(Ds_pair.first));
            //     std::string truth_channel;
            //     // accounting for presence of neutrals in truth D+ decay mode (RM = Right Mode)
            //     if(fabs(truth_daughter_mass(truthIndex) - DP_MASS) < 40) truth_channel = channel + "_recoMatch_truth_Ds_RM"+ std::to_string(Ds_pair.second);
            //     else truth_channel = channel + "_Ds_recoMatch_truth" + std::to_string(Ds_pair.second);
            //     TruthInfo::fill_truth_D_histograms(this, truth_channel, truthIndex);
            // }
        }
    }

    //counting D multiplicity
    for (auto channel : used_channels)
    {
        std::vector<float> multiplicity;
        for (unsigned int j = 0; j < m_systematics.size() + 1; j++)
        {
            multiplicity.push_back(multiplicity_counter.at(j)[channel]);
        }
        if (!m_fit_variables_only)
        {
            add_fill_histogram_sys("N_Ds", channel, multiplicity, 20, -0.5, 19.5, true);
        }
    }

    // // pure D meson truth
    if (m_is_mc && m_truth_D)
    {
        for (unsigned int i = 0; i < m_truth_D_s_mesons.size(); i++)
        {
            unsigned int index = m_truth_D_s_mesons.at(i);
            std::string truth_channel;
            // accounting for presence of neutrals in truth D+ decay mode (RM = Right Mode)
            if (fabs(truth_daughter_mass(index) - DS_MASS) < 40)
            {
                truth_channel = channel + "_truth_Ds_RM";
                TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
            }
            truth_channel = channel + "_truth_Ds";
            TruthInfo::fill_truth_D_histograms(this, truth_channel, index);
        }
    }
}

void WQCDCharmLoopBase::fill_histograms_with_matrix_method()
{
    if (!m_do_lep_presel)
    {
        fill_histograms();
        return;
    }
    if (m_fake_rate_measurement || m_fake_factor_method || (!m_is_mc && m_eval_fake_rate))
    {
        // calculate weight for anti-tight events
        for (unsigned int i = 0; i < m_systematics.size() + 1; i++)
        {
            if (m_channel_sys.at(i) == "")
            {
                continue;
            }
            if (m_sys_anti_tight.at(i))
            {
                m_channel_sys.at(i) = "AntiTight_" + m_channel_sys.at(i);
                if (m_eval_fake_rate)
                {
                    matrix_method_for_sys(i, true);
                }
            }
            else
            {
                if (!m_eval_fake_rate && m_fake_rate_measurement)
                {
                    m_channel_sys.at(i) = "Tight_" + m_channel_sys.at(i);
                }
            }
        }

        // fill histograms (anti-tight and regular SR)
        fill_histograms();

        // exit here if only doing the fake rate measurement
        if (m_fake_rate_measurement || m_fake_factor_method)
        {
            return;
        }

        // blank out anti-tight regions so it doesn't get filled again
        for (unsigned int i = 0; i < m_systematics.size() + 1; i++)
        {
            if (m_sys_anti_tight.at(i))
            {
                m_channel_sys.at(i) = "";
            }
        }

        // calculate weight for tight events
        for (unsigned int i = 0; i < m_systematics.size() + 1; i++)
        {
            if (m_channel_sys.at(i) == "")
            {
                continue;
            }
            if (!m_sys_anti_tight.at(i))
            {
                m_channel_sys.at(i) = "Tight_" + m_channel_sys.at(i);
                if (m_eval_fake_rate)
                {
                    matrix_method_for_sys(i, false);
                }
            }
        }

        // fill histograms (only tight)
        fill_histograms();
    }
    else
    {
        // not using the matrix method
        fill_histograms();
    }
}

float WQCDCharmLoopBase::get_dr_bjet_Dmeson(unsigned int jet, unsigned int i)
{
    float deta = m_DMesons_eta->at(i) - m_AnalysisJets_eta->at(jet);
    float dphi = TVector2::Phi_mpi_pi(m_DMesons_phi->at(i) - m_AnalysisJets_phi->at(jet));
    return TMath::Sqrt(deta * deta + dphi * dphi);
}

void WQCDCharmLoopBase::calculate_track_jet_properties(unsigned int DMeson_index,
                                                       unsigned int sys_index,
                                                       std::vector<int> &sys_track_jet_index,
                                                       std::vector<Float_t> &sys_track_jet_dR,
                                                       std::vector<Float_t> &sys_track_jet_pt,
                                                       std::vector<Float_t> &sys_track_jet_zt,
                                                       const std::vector<float> *jet_pt,
                                                       const std::vector<float> *jet_eta,
                                                       const std::vector<float> *jet_phi)
{
    int track_jet_index = -1;
    float track_jet_dR = 999;
    float track_jet_pt = 0.0;
    float track_jet_zt = 0.0;
    for (unsigned int jet = 0; jet < jet_pt->size(); jet++)
    {
        float deta = m_DMesons_eta->at(DMeson_index) - jet_eta->at(jet);
        float dphi = TVector2::Phi_mpi_pi(m_DMesons_phi->at(DMeson_index) - jet_phi->at(jet));
        float dR = TMath::Sqrt(deta * deta + dphi * dphi);
        if (dR < track_jet_dR)
        {
            track_jet_dR = dR;
            track_jet_index = jet;
        }
    }
    sys_track_jet_index.at(sys_index) = track_jet_index;
    sys_track_jet_dR.at(sys_index) = track_jet_dR;
    if (track_jet_index >= 0)
    {
        track_jet_pt = jet_pt->at(track_jet_index);
        track_jet_zt = m_DMesons_pt->at(DMeson_index) / jet_pt->at(track_jet_index);
    }
    sys_track_jet_pt.at(sys_index) = track_jet_pt * Charm::GeV;
    sys_track_jet_zt.at(sys_index) = track_jet_zt;
}

void WQCDCharmLoopBase::jet_selection_for_sys()
{
    if (!m_do_lep_presel)
    {
        return;
    }
    for (unsigned int i = 0; i < m_systematics.size() + 1; i++)
    {
        if (m_channel_sys.at(i) == "")
        {
            continue;
        }
        auto jets = m_jets_sys.at(i);
        for (auto &jet : jets)
        {
            // ftag SF
            if (m_is_mc)
            {
                if (fabs(m_AnalysisJets_eta->at(jet)) <= 2.5)
                {
                    multiply_event_weight(m_sys_jet_ftag_eff.at(i)->at(jet), i);
                }
            }
            if (m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70->at(jet))
            {
                m_sys_nbjets.at(i) += 1;
            }
        }
        if (m_sys_nbjets.at(i) == 0)
        {
            m_channel_sys.at(i) += "_0tag";
        }
        else if (m_sys_nbjets.at(i) == 1)
        {
            m_channel_sys.at(i) += "_1tag";
        }
        else if (m_two_tag_region && m_sys_nbjets.at(i) >= 2)
        {
            m_channel_sys.at(i) += "_2tag";
        }
    }
}

void WQCDCharmLoopBase::matrix_method_for_sys(unsigned int i, bool anti_tight)
{
    float pt = m_sys_lep_pt.at(i);
    if (m_do_el_topocone_param)
    {
        pt = m_sys_lep_pt_plusTopoConeET20.at(i);
    }
    float eta = fabs(m_sys_lep_eta.at(i));
    float met = m_sys_met.at(i);
    float mt = m_sys_met_mt.at(i);
    int nbjet = m_sys_nbjets.at(i);
    std::string channel = "mu";
    if (m_loose_electrons_sys.at(i).size())
    {
        channel = "el";
        eta = fabs(m_sys_lep_calo_eta.at(i));
    }
    float w = 0;
    float w2 = 1;
    if (m_inclusive_fake_rate)
    {
        w = matrix_method_weight_inclusive(i, pt, eta, mt, anti_tight, channel);
    }
    else
    {
        w = matrix_method_weight(i, pt, eta, met, mt, nbjet, anti_tight, channel);
    }

    if (m_rw_mm_closure)
    {
        w2 = matrix_method_closure_reweight(i, met, nbjet, channel);
    }

    multiply_event_weight(w * w2, i);
}

float WQCDCharmLoopBase::matrix_method_weight(unsigned int i, float pt, float eta, float met, float mt, int nbjet, bool anti_tight, std::string channel)
{
    std::string sys_string = "";
    if (i > 0)
    {
        sys_string = m_systematics.at(i - 1);
    }

    // Get fake and real rates
    std::string DspeciesMM = "Dstar";
    if (m_D_plus_meson && !m_D_star_meson)
    {
        DspeciesMM = "Dplus";
    }
    bool fake_rate_one = false;
    float fake_rate = m_matrix_method_helper.get_fake_rate(sys_string, channel, "", DspeciesMM, nbjet, pt, eta, met, fake_rate_one);
    float real_rate = 1.0;
    if (!m_fake_factor_method)
    {
        real_rate = m_matrix_method_helper.get_real_rate(sys_string, channel, "", DspeciesMM, nbjet, pt, eta, mt);
    }

    // Non closure sys is the baseline case, w/o non closure reweighting
    // 2015 is given 100% uncertainty
    // FR uncertainty is handled here as well for simplicity, if the FR in the bin exceeds 100%
    float non_closure_w = 1.0;
    if (fake_rate_one)
    {
        non_closure_w *= 2.0;
    }
    non_closure_w *= m_matrix_method_helper.get_non_closure_sys(sys_string, channel, "", DspeciesMM, nbjet, met);

    // Calculate weight
    if (anti_tight)
    {
        if (!m_is_mc)
        {
            return real_rate * fake_rate / (real_rate - fake_rate) * non_closure_w;
        }
        else
        {
            return -1.0 * real_rate * fake_rate / (real_rate - fake_rate) * non_closure_w;
        }
    }
    else
    {
        if (m_fake_factor_method)
        {
            throw std::runtime_error("should not be calculating the Tight region for the Fake Factor method");
        }
        // Sign is inverted here, to generate positive Tight distributions which are later subtracted from AT distributions
        return ((1 - real_rate) * fake_rate / (real_rate - fake_rate))* non_closure_w;
    }
}

float WQCDCharmLoopBase::matrix_method_weight_inclusive(unsigned int i, float pt, float eta, float mt, bool anti_tight, std::string channel)
{
    std::string sys_string = "";
    if (i > 0)
    {
        sys_string = m_systematics.at(i - 1);
    }

    bool fake_rate_one = false;
    float fake_rate = m_matrix_method_helper.get_fake_rate_incl(sys_string, channel, "", pt, eta, fake_rate_one);
    float real_rate = m_matrix_method_helper.get_real_rate_incl(sys_string, channel, "", pt, eta, mt);

    if (anti_tight)
    {
        return real_rate * fake_rate / (real_rate - fake_rate);
    }
    else
    {
        return (1 - real_rate) * fake_rate / (real_rate - fake_rate);
    }
}

float WQCDCharmLoopBase::matrix_method_closure_reweight(unsigned int i, float met, int nbjet, std::string channel)
{
    std::string sys_string = "";
    if (i > 0)
    {
        sys_string = m_systematics.at(i - 1);
    }

    std::string DspeciesMM = "Dstar";
    if (m_D_plus_meson && !m_D_star_meson)
    {
        DspeciesMM = "Dplus";
    }

    return m_matrix_method_helper.get_closure_rw(sys_string, channel, "", DspeciesMM, nbjet, met);
}

void WQCDCharmLoopBase::fill_histograms(std::string channel)
{
    //Global, cross D variable used in Real Rate Measurement
    m_have_one_D = false;

    // PV histograms
    if (m_do_PV && m_fill_wjets)
    {
        fill_PV_histograms(channel);
    }

    // fill lepton histograms
    if (m_do_lep_presel && m_fill_wjets)
    {
        fill_lepton_histograms(channel);
    }

    // fill D plus histograms
    if (m_D_plus_meson)
    {
        fill_D_plus_histograms(channel);
    }

    // fill D star histograms
    if (m_D_star_meson)
    {
        fill_D_star_histograms(channel);
    }

    // fill D star pi0 histograms
    if (m_D_star_pi0_meson)
    {
        fill_D_star_pi0_histograms(channel);
    }

    // fill D s histograms
    if (m_D_s_meson)
    {
        fill_D_s_histograms(channel);
    }

    // stand-alone truth
    if (m_truth_D && m_channel_sys.at(0) != "")
    {
        TruthInfo::fill_stand_alone_truth(this);
    }
}

std::string WQCDCharmLoopBase::get_period_string()
{
    return get_period(m_EventInfo_runNumber);
}

void WQCDCharmLoopBase::debug_dmeson_printout(int index, bool printTruth)
{
    if (index >= 0)
    {
        std::cout << "Reco D meson:" << std::endl;
        std::cout << "  pdgId: " << m_DMesons_pdgId->at(index) << std::endl;
        std::cout << "  pT: " << m_DMesons_pt->at(index) * Charm::GeV << std::endl;
        std::cout << "  eta: " << m_DMesons_eta->at(index) << std::endl;
        std::cout << "  phi: " << m_DMesons_phi->at(index) << std::endl;
        std::cout << "  m: " << m_DMesons_m->at(index) * Charm::GeV << std::endl;
        std::cout << "  Lxy: " << m_DMesons_fitOutput__Lxy->at(index) << std::endl;
        std::cout << "  Chi2: " << m_DMesons_fitOutput__Chi2->at(index) << std::endl;
        std::cout << "  sigma 3D: " << m_DMesons_fitOutput__ImpactSignificance->at(index) << std::endl;
        std::cout << "  truthBarcode: " << m_DMesons_truthBarcode->at(index) << std::endl;
        std::cout << "  num daughters: " << m_DMesons_daughterInfo__truthDBarcode->at(index).size() << std::endl;
        for (unsigned int i = 0; i < m_DMesons_daughterInfo__truthDBarcode->at(index).size(); i++)
        {
            std::cout << "  daughter track " << i << std::endl;
            std::cout << "    truthDBarcode: " << m_DMesons_daughterInfo__truthDBarcode->at(index).at(i) << std::endl;
            std::cout << "    truthBarcode: " << m_DMesons_daughterInfo__truthBarcode->at(index).at(i) << std::endl;
            std::cout << "    pT: " << m_DMesons_daughterInfo__pt->at(index).at(i) * Charm::GeV << std::endl;
            std::cout << "    eta: " << m_DMesons_daughterInfo__eta->at(index).at(i) << std::endl;
            std::cout << "    phi: " << m_DMesons_daughterInfo__phi->at(index).at(i) << std::endl;
        }
    }
    if (printTruth)
    {
        std::cout << "Truth Recrod: " << m_TruthParticles_Selected_pdgId->size() << " particles in the ntuple" << std::endl;
        for (unsigned int i = 0; i < m_TruthParticles_Selected_pdgId->size(); i++)
        {
            std::cout << "truth particle " << i << std::endl;
            std::cout << "  pdgId: " << m_TruthParticles_Selected_pdgId->at(i) << std::endl;
            std::cout << "  barcode: " << m_TruthParticles_Selected_barcode->at(i) << std::endl;
            std::cout << "  pT: " << m_TruthParticles_Selected_pt->at(i) * Charm::GeV << std::endl;
            std::cout << "  eta: " << m_TruthParticles_Selected_eta->at(i) << std::endl;
            std::cout << "  phi: " << m_TruthParticles_Selected_phi->at(i) << std::endl;
            std::cout << "  num daughters: " << m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).size() << std::endl;
            for (unsigned int j = 0; j < m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).size(); j++)
            {
                std::cout << "  truth particle " << j << std::endl;
                std::cout << "    pdgId: " << m_TruthParticles_Selected_daughterInfoT__pdgId->at(i).at(j) << std::endl;
                std::cout << "    barcode: " << m_TruthParticles_Selected_daughterInfoT__barcode->at(i).at(j) << std::endl;
                std::cout << "    pT: " << m_TruthParticles_Selected_daughterInfoT__pt->at(i).at(j) * Charm::GeV << std::endl;
                std::cout << "    eta: " << m_TruthParticles_Selected_daughterInfoT__eta->at(i).at(j) << std::endl;
                std::cout << "    phi: " << m_TruthParticles_Selected_daughterInfoT__phi->at(i).at(j) << std::endl;
            }
        }
    }
}

void WQCDCharmLoopBase::connect_branches()
{
    if (m_do_PV)
    {
        // vertex variables
        add_preselection_branch("CharmEventInfo_PV_X", &m_CharmEventInfo_PV_X);
        add_preselection_branch("CharmEventInfo_PV_Y", &m_CharmEventInfo_PV_Y);
        add_preselection_branch("CharmEventInfo_PV_Z", &m_CharmEventInfo_PV_Z);
        // add_preselection_branch("CharmEventInfo_truthVertex_X", &m_CharmEventInfo_truthVertex_X);
        // add_preselection_branch("CharmEventInfo_truthVertex_Y", &m_CharmEventInfo_truthVertex_Y);
        // add_preselection_branch("CharmEventInfo_truthVertex_Z", &m_CharmEventInfo_truthVertex_Z);
    }

    // TODO: make this dirty code nicer
    // need event weight even in some cases without lepton stuff
    if (!m_do_lep_presel)
    {
        if (m_is_mc && !m_no_weights)
        {
            add_preselection_branch("EventInfo_generatorWeight_NOSYS", &m_EventInfo_generatorWeight_NOSYS);
        }
        else if (m_data_period == "ForcedDecay")
        {
            add_preselection_branch("CharmEventInfo_EventWeight", &m_EventInfo_generatorWeight_NOSYS);
        }
        return;
    }

    if (m_do_lep_presel)
    {
        // event info
        add_preselection_branch("METInfo_MET_NOSYS", &m_METInfo_MET_NOSYS);
        add_preselection_branch("METInfo_METPhi_NOSYS", &m_METInfo_METPhi_NOSYS);
        if (m_use_anti_tight_met && ((m_eval_fake_rate || m_fake_rate_measurement) && !m_is_mc))
        {
            add_preselection_branch("METInfoAntiTight_NOSYS_MET", &m_METInfoAntiTight_NOSYS_MET);
            add_preselection_branch("METInfoAntiTight_NOSYS_METPhi", &m_METInfoAntiTight_NOSYS_METPhi);
        }
        add_preselection_branch("EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH", &m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH);
        add_preselection_branch("EventInfo_trigPassed_HLT_e60_lhmedium", &m_EventInfo_trigPassed_HLT_e60_lhmedium);
        add_preselection_branch("EventInfo_trigPassed_HLT_e120_lhloose", &m_EventInfo_trigPassed_HLT_e120_lhloose);
        add_preselection_branch("EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose", &m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose);
        add_preselection_branch("EventInfo_trigPassed_HLT_e60_lhmedium_nod0", &m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0);
        add_preselection_branch("EventInfo_trigPassed_HLT_e140_lhloose_nod0", &m_EventInfo_trigPassed_HLT_e140_lhloose_nod0);
        add_preselection_branch("EventInfo_trigPassed_HLT_mu20_iloose_L1MU15", &m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15);
        add_preselection_branch("EventInfo_trigPassed_HLT_mu26_ivarmedium", &m_EventInfo_trigPassed_HLT_mu26_ivarmedium);
        add_preselection_branch("EventInfo_trigPassed_HLT_mu50", &m_EventInfo_trigPassed_HLT_mu50);
        add_preselection_branch(Charm::get_run_number_string(m_is_mc), &m_EventInfo_runNumber);
        add_preselection_branch(Charm::get_pileup_string(m_is_mc, m_mc_period, m_data_period), &m_EventInfo_correctedScaled_averageInteractionsPerCrossing);

        // muons
        add_preselection_branch("AnalysisMuons_pt_NOSYS", &m_AnalysisMuons_pt_NOSYS);
        add_preselection_branch("AnalysisMuons_eta", &m_AnalysisMuons_eta);
        add_preselection_branch("AnalysisMuons_phi", &m_AnalysisMuons_phi);
        add_preselection_branch("AnalysisMuons_charge", &m_AnalysisMuons_charge);
        add_preselection_branch("AnalysisMuons_mu_selected_NOSYS", &m_AnalysisMuons_mu_selected_NOSYS);
        add_preselection_branch("AnalysisMuons_isQuality_Tight_NOSYS", &m_AnalysisMuons_isQuality_Tight_NOSYS);
        add_preselection_branch("AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS", &m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS);
        add_preselection_branch("AnalysisMuons_matched_HLT_mu20_iloose_L1MU15", &m_AnalysisMuons_matched_HLT_mu20_iloose_L1MU15);
        add_preselection_branch("AnalysisMuons_matched_HLT_mu26_ivarmedium", &m_AnalysisMuons_matched_HLT_mu26_ivarmedium);
        add_preselection_branch("AnalysisMuons_matched_HLT_mu50", &m_AnalysisMuons_matched_HLT_mu50);
        add_preselection_branch("AnalysisMuons_d0", &m_AnalysisMuons_d0);
        add_preselection_branch("AnalysisMuons_d0sig", &m_AnalysisMuons_d0sig);
        add_preselection_branch("AnalysisMuons_is_bad", &m_AnalysisMuons_is_bad);
        add_preselection_branch("AnalysisMuons_ptvarcone30_TightTTVA_pt500", &m_AnalysisMuons_ptvarcone30_TightTTVA_pt500);
        add_preselection_branch("AnalysisMuons_neflowisol20", &m_AnalysisMuons_neflowisol20);
        add_preselection_branch("AnalysisMuons_topoetcone20", &m_AnalysisMuons_topoetcone20);
        if (m_do_extra_histograms)
        {
            add_preselection_branch("AnalysisMuons_z0sinTheta", &m_AnalysisMuons_z0sinTheta);
            add_preselection_branch("AnalysisMuons_ptvarcone30_TightTTVA_pt1000", &m_AnalysisMuons_ptvarcone30_TightTTVA_pt1000);
        }

        // electrons
        add_preselection_branch("AnalysisElectrons_pt_NOSYS", &m_AnalysisElectrons_pt_NOSYS);
        add_preselection_branch("AnalysisElectrons_eta", &m_AnalysisElectrons_eta);
        add_preselection_branch("AnalysisElectrons_caloCluster_eta", &m_AnalysisElectrons_caloCluster_eta);
        add_preselection_branch("AnalysisElectrons_phi", &m_AnalysisElectrons_phi);
        add_preselection_branch("AnalysisElectrons_charge", &m_AnalysisElectrons_charge);
        add_preselection_branch("AnalysisElectrons_likelihood_Tight", &m_AnalysisElectrons_likelihood_Tight);
        add_preselection_branch("AnalysisElectrons_el_selected_NOSYS", &m_AnalysisElectrons_el_selected_NOSYS);
        add_preselection_branch("AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS", &m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS);
        add_preselection_branch("AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH", &m_AnalysisElectrons_matched_HLT_e24_lhmedium_L1EM20VH);
        add_preselection_branch("AnalysisElectrons_matched_HLT_e60_lhmedium", &m_AnalysisElectrons_matched_HLT_e60_lhmedium);
        add_preselection_branch("AnalysisElectrons_matched_HLT_e120_lhloose", &m_AnalysisElectrons_matched_HLT_e120_lhloose);
        add_preselection_branch("AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose", &m_AnalysisElectrons_matched_HLT_e26_lhtight_nod0_ivarloose);
        add_preselection_branch("AnalysisElectrons_matched_HLT_e60_lhmedium_nod0", &m_AnalysisElectrons_matched_HLT_e60_lhmedium_nod0);
        add_preselection_branch("AnalysisElectrons_matched_HLT_e140_lhloose_nod0", &m_AnalysisElectrons_matched_HLT_e140_lhloose_nod0);
        add_preselection_branch("AnalysisElectrons_d0", &m_AnalysisElectrons_d0);
        add_preselection_branch("AnalysisElectrons_d0sig", &m_AnalysisElectrons_d0sig);
        add_preselection_branch("AnalysisElectrons_ptvarcone20_TightTTVA_pt1000", &m_AnalysisElectrons_ptvarcone20_TightTTVA_pt1000);
        add_preselection_branch("AnalysisElectrons_topoetcone20", &m_AnalysisElectrons_topoetcone20);
        if (m_do_extra_histograms)
        {
            add_preselection_branch("AnalysisElectrons_z0sinTheta", &m_AnalysisElectrons_z0sinTheta);
            add_preselection_branch("AnalysisElectrons_ptvarcone30_TightTTVA_pt1000", &m_AnalysisElectrons_ptvarcone30_TightTTVA_pt1000);
            // add_preselection_branch("AnalysisElectrons_EOverP", &m_AnalysisElectrons_EOverP);
        }

        // mc only
        if (m_is_mc)
        {
            add_preselection_branch("EventInfo_generatorWeight_NOSYS", &m_EventInfo_generatorWeight_NOSYS);
            add_preselection_branch("EventInfo_PileupWeight_NOSYS", &m_EventInfo_PileupWeight_NOSYS);
            add_preselection_branch("EventInfo_jvt_effSF_NOSYS", &m_EventInfo_jvt_effSF_NOSYS);
            // add_preselection_branch("EventInfo_fjvt_effSF_NOSYS", &m_EventInfo_fjvt_effSF_NOSYS);
            add_preselection_branch("AnalysisElectrons_effSF_NOSYS", &m_AnalysisElectrons_effSF_NOSYS);
            add_preselection_branch("AnalysisElectrons_effSF_ID_Tight_NOSYS", &m_AnalysisElectrons_effSF_ID_Tight_NOSYS);
            add_preselection_branch("AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS", &m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS);
            add_preselection_branch("AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
            add_preselection_branch("AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS", &m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);
            add_preselection_branch("AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS", &m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS);
            add_preselection_branch("AnalysisElectrons_truthType", &m_AnalysisElectrons_truthType);
            add_preselection_branch("AnalysisElectrons_truthOrigin", &m_AnalysisElectrons_truthOrigin);
            add_preselection_branch("AnalysisElectrons_firstEgMotherTruthType", &m_AnalysisElectrons_firstEgMotherTruthType);
            add_preselection_branch("AnalysisElectrons_firstEgMotherTruthOrigin", &m_AnalysisElectrons_firstEgMotherTruthOrigin);
            add_preselection_branch("AnalysisElectrons_firstEgMotherPdgId", &m_AnalysisElectrons_firstEgMotherPdgId);
            add_preselection_branch("AnalysisMuons_muon_effSF_Quality_Tight_NOSYS", &m_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS);
            add_preselection_branch("AnalysisMuons_muon_effSF_TTVA_NOSYS", &m_AnalysisMuons_muon_effSF_TTVA_NOSYS);
            add_preselection_branch("AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS", &m_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS);
            add_preselection_branch("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
            add_preselection_branch("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);
            add_preselection_branch("AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
            add_preselection_branch("AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS", &m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);
            add_preselection_branch("AnalysisMuons_truthType", &m_AnalysisMuons_truthType);
            add_preselection_branch("AnalysisMuons_truthOrigin", &m_AnalysisMuons_truthOrigin);

            // sys branches
            if (m_do_systematics)
            {
                // event weights
                initialize_sys_branches(&m_sys_branches_GEN, "EventInfo_generatorWeight_%s");
                initialize_sys_branches(&m_sys_branches_PU, "EventInfo_PileupWeight_%s");
                initialize_sys_branches(&m_sys_branches_JVT, "EventInfo_jvt_effSF_%s");
                // initialize_sys_branches(&m_sys_branches_FTAG, "EventInfo_ftag_effSF_DL1r_FixedCutBEff_70_%s");

                // met
                initialize_sys_branches(&m_sys_branches_MET_met, "METInfo_MET_%s");
                initialize_sys_branches(&m_sys_branches_MET_phi, "METInfo_METPhi_%s");

                // electron sys branches
                initialize_sys_branches(&m_sys_branches_electron_pt, "AnalysisElectrons_pt_%s");
                initialize_sys_branches(&m_sys_branches_electron_selected, "AnalysisElectrons_el_selected_%s");
                initialize_sys_branches(&m_sys_branches_electron_isIsolated_Tight_VarRad, "AnalysisElectrons_isIsolated_Tight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_electron_reco_eff, "AnalysisElectrons_effSF_%s");
                initialize_sys_branches(&m_sys_branches_electron_id_eff, "AnalysisElectrons_effSF_ID_Tight_%s");
                initialize_sys_branches(&m_sys_branches_electron_iso_eff, "AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_electron_charge_eff, "AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_electron_trig_sf, "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                                        {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
                initialize_sys_branches(&m_sys_branches_electron_trig_noiso_sf, "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s",
                                        {"EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"});
                initialize_sys_branches(&m_sys_branches_electron_calib_reco_eff, "AnalysisElectrons_effSF_%s");
                initialize_sys_branches(&m_sys_branches_electron_calib_id_eff, "AnalysisElectrons_effSF_ID_Tight_%s");
                initialize_sys_branches(&m_sys_branches_electron_calib_iso_eff, "AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_electron_calib_charge_eff, "AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_electron_calib_trig_sf, "AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s");
                initialize_sys_branches(&m_sys_branches_electron_calib_trig_noiso_sf, "AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_%s");

                // muon sys branches
                initialize_sys_branches(&m_sys_branches_muon_pt, "AnalysisMuons_pt_%s");
                initialize_sys_branches(&m_sys_branches_muon_selected, "AnalysisMuons_mu_selected_%s");
                initialize_sys_branches(&m_sys_branches_muon_isQuality_Tight, "AnalysisMuons_isQuality_Tight_%s");
                initialize_sys_branches(&m_sys_branches_muon_isIsolated_PflowTight_VarRad, "AnalysisMuons_isIsolated_PflowTight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_muon_quality_eff, "AnalysisMuons_muon_effSF_Quality_Tight_%s");
                initialize_sys_branches(&m_sys_branches_muon_ttva_eff, "AnalysisMuons_muon_effSF_TTVA_%s");
                initialize_sys_branches(&m_sys_branches_muon_iso_eff, "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_muon_trig_2015_eff_mc, "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_%s");
                initialize_sys_branches(&m_sys_branches_muon_trig_2015_eff_data, "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_%s");
                initialize_sys_branches(&m_sys_branches_muon_trig_2018_eff_mc, "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_%s");
                initialize_sys_branches(&m_sys_branches_muon_trig_2018_eff_data, "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_%s");
                initialize_sys_branches(&m_sys_branches_muon_calib_quality_eff, "AnalysisMuons_muon_effSF_Quality_Tight_%s");
                initialize_sys_branches(&m_sys_branches_muon_calib_ttva_eff, "AnalysisMuons_muon_effSF_TTVA_%s");
                initialize_sys_branches(&m_sys_branches_muon_calib_iso_eff, "AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_%s");
                initialize_sys_branches(&m_sys_branches_muon_calib_trig_2015_eff_mc, "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_%s");
                initialize_sys_branches(&m_sys_branches_muon_calib_trig_2015_eff_data, "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_%s");
                initialize_sys_branches(&m_sys_branches_muon_calib_trig_2018_eff_mc, "AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_%s");
                initialize_sys_branches(&m_sys_branches_muon_calib_trig_2018_eff_data, "AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_%s");

                // jet sys branches
                initialize_sys_branches(&m_sys_branches_jet_pt, "AnalysisJets_pt_%s");
                initialize_sys_branches(&m_sys_branches_jet_selected, "AnalysisJets_jet_selected_%s");
                initialize_sys_branches(&m_sys_branches_jet_ftag_eff, "AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_%s");
                initialize_sys_branches(&m_sys_branches_jet_calib_ftag_eff, "AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_%s");
            }
        }
    }

    // reweight production fractions
    if (m_is_mc && (m_do_prod_fraction_rw || m_do_wjets_rw) && !(m_truth_matchD || m_truth_D))
    {
        connect_truth_D_meson_branches(this, (m_fiducial_selection || m_save_truth_wjets));
    }

    // jets
    if (m_jet_selection)
    {
        add_preselection_branch("AnalysisJets_pt_NOSYS", &m_AnalysisJets_pt_NOSYS);
        add_preselection_branch("AnalysisJets_eta", &m_AnalysisJets_eta);
        add_preselection_branch("AnalysisJets_phi", &m_AnalysisJets_phi);
        add_preselection_branch("AnalysisJets_m", &m_AnalysisJets_m);
        add_preselection_branch("AnalysisJets_jet_selected_NOSYS", &m_AnalysisJets_jet_selected_NOSYS);
        add_preselection_branch("AnalysisJets_ftag_select_DL1r_FixedCutBEff_70", &m_AnalysisJets_ftag_select_DL1r_FixedCutBEff_70);
        if (m_is_mc)
        {
            add_preselection_branch("AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS", &m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS);
        }
    }

    // track-jets
    if (m_track_jet_selection)
    {
        add_preselection_branch("AnalysisAntiKt10PV0TrackJets_pt", &m_AnalysisAntiKt10PV0TrackJets_pt);
        add_preselection_branch("AnalysisAntiKt10PV0TrackJets_eta", &m_AnalysisAntiKt10PV0TrackJets_eta);
        add_preselection_branch("AnalysisAntiKt10PV0TrackJets_phi", &m_AnalysisAntiKt10PV0TrackJets_phi);
        add_preselection_branch("AnalysisAntiKt10PV0TrackJets_m", &m_AnalysisAntiKt10PV0TrackJets_m);
        add_preselection_branch("AnalysisAntiKt8PV0TrackJets_pt", &m_AnalysisAntiKt8PV0TrackJets_pt);
        add_preselection_branch("AnalysisAntiKt8PV0TrackJets_eta", &m_AnalysisAntiKt8PV0TrackJets_eta);
        add_preselection_branch("AnalysisAntiKt8PV0TrackJets_phi", &m_AnalysisAntiKt8PV0TrackJets_phi);
        add_preselection_branch("AnalysisAntiKt8PV0TrackJets_m", &m_AnalysisAntiKt8PV0TrackJets_m);
        add_preselection_branch("AnalysisAntiKt6PV0TrackJets_pt", &m_AnalysisAntiKt6PV0TrackJets_pt);
        add_preselection_branch("AnalysisAntiKt6PV0TrackJets_eta", &m_AnalysisAntiKt6PV0TrackJets_eta);
        add_preselection_branch("AnalysisAntiKt6PV0TrackJets_phi", &m_AnalysisAntiKt6PV0TrackJets_phi);
        add_preselection_branch("AnalysisAntiKt6PV0TrackJets_m", &m_AnalysisAntiKt6PV0TrackJets_m);
    }
}

void WQCDCharmLoopBase::read_sys_branches()
{
    // read electrons
    m_sys_el_iso = get_val_for_systematics(m_AnalysisElectrons_isIsolated_Tight_VarRad_NOSYS, &m_sys_branches_electron_isIsolated_Tight_VarRad);
    m_sys_el_pt = get_val_for_systematics(m_AnalysisElectrons_pt_NOSYS, &m_sys_branches_electron_pt);
    m_sys_el_reco_eff = get_val_for_systematics(m_AnalysisElectrons_effSF_NOSYS, &m_sys_branches_electron_reco_eff, &m_sys_branches_electron_calib_reco_eff);
    m_sys_el_id_eff = get_val_for_systematics(m_AnalysisElectrons_effSF_ID_Tight_NOSYS, &m_sys_branches_electron_id_eff, &m_sys_branches_electron_calib_id_eff);
    m_sys_el_iso_eff = get_val_for_systematics(m_AnalysisElectrons_effSF_Isol_Tight_Tight_VarRad_NOSYS, &m_sys_branches_electron_iso_eff, &m_sys_branches_electron_calib_iso_eff);
    m_sys_el_trig_sf = get_val_for_systematics(m_AnalysisElectrons_effSF_Trig_Tight_Tight_VarRad_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
                                               &m_sys_branches_electron_trig_sf, &m_sys_branches_electron_calib_trig_sf);
    m_sys_el_trig_noiso_sf = get_val_for_systematics(m_AnalysisElectrons_effSF_Trig_Tight_noIso_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS,
                                                     &m_sys_branches_electron_trig_noiso_sf, &m_sys_branches_electron_calib_trig_noiso_sf);
    m_sys_el_charge_sf = get_val_for_systematics(m_AnalysisElectrons_effSF_Chflip_Tight_Tight_VarRad_NOSYS, &m_sys_branches_electron_charge_eff, &m_sys_branches_electron_calib_charge_eff);

    // read muons
    m_sys_mu_pt = get_val_for_systematics(m_AnalysisMuons_pt_NOSYS, &m_sys_branches_muon_pt);
    m_sys_mu_quality = get_val_for_systematics(m_AnalysisMuons_isQuality_Tight_NOSYS, &m_sys_branches_muon_isQuality_Tight);
    m_sys_mu_iso = get_val_for_systematics(m_AnalysisMuons_isIsolated_PflowTight_VarRad_NOSYS, &m_sys_branches_muon_isIsolated_PflowTight_VarRad);
    m_sys_mu_quality_eff = get_val_for_systematics(m_AnalysisMuons_muon_effSF_Quality_Tight_NOSYS, &m_sys_branches_muon_quality_eff, &m_sys_branches_muon_calib_quality_eff);
    m_sys_mu_ttva_eff = get_val_for_systematics(m_AnalysisMuons_muon_effSF_TTVA_NOSYS, &m_sys_branches_muon_ttva_eff, &m_sys_branches_muon_calib_ttva_eff);
    m_sys_mu_iso_eff = get_val_for_systematics(m_AnalysisMuons_muon_effSF_Isol_PflowTight_VarRad_NOSYS, &m_sys_branches_muon_iso_eff, &m_sys_branches_muon_calib_iso_eff);
    m_sys_mu_trig_2015_eff_mc = get_val_for_systematics(m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
                                                        &m_sys_branches_muon_trig_2015_eff_mc, &m_sys_branches_muon_calib_trig_2015_eff_mc);
    m_sys_mu_trig_2015_eff_data = get_val_for_systematics(m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS,
                                                          &m_sys_branches_muon_trig_2015_eff_data, &m_sys_branches_muon_calib_trig_2015_eff_data);
    m_sys_mu_trig_2018_eff_mc = get_val_for_systematics(m_AnalysisMuons_muon_effMC_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
                                                        &m_sys_branches_muon_trig_2018_eff_mc, &m_sys_branches_muon_calib_trig_2018_eff_mc);
    m_sys_mu_trig_2018_eff_data = get_val_for_systematics(m_AnalysisMuons_muon_effData_Trig_Tight_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS,
                                                          &m_sys_branches_muon_trig_2018_eff_data, &m_sys_branches_muon_calib_trig_2018_eff_data);

    // read jets
    m_sys_jet_pt = get_val_for_systematics(m_AnalysisJets_pt_NOSYS, &m_sys_branches_jet_pt);
    m_sys_jet_ftag_eff = get_val_for_systematics(m_AnalysisJets_ftag_effSF_DL1r_FixedCutBEff_70_NOSYS, &m_sys_branches_jet_ftag_eff, &m_sys_branches_jet_calib_ftag_eff);

    // branches for selecting objects
    m_sys_el_selected = get_val_for_systematics(m_AnalysisElectrons_el_selected_NOSYS, &m_sys_branches_electron_selected);
    m_sys_mu_selected = get_val_for_systematics(m_AnalysisMuons_mu_selected_NOSYS, &m_sys_branches_muon_selected);
    m_sys_jet_selected = get_val_for_systematics(m_AnalysisJets_jet_selected_NOSYS, &m_sys_branches_jet_selected);
}

void WQCDCharmLoopBase::configure()
{
    // set configs
    std::cout << get_config() << std::endl;

    // systematics
    m_do_systematics = get_config()["do_systematics"].as<bool>(false);

    // save only variables needed for the fit
    m_fit_variables_only = get_config()["fit_variables_only"].as<bool>(false);

    // save unfolding varaibles (transfer matrix, etc..)
    m_unfolding_variables = get_config()["unfolding_variables"].as<bool>(false);

    // lepton preseletion
    m_do_lep_presel = get_config()["do_lep_presel"].as<bool>(true);
    m_fill_wjets = get_config()["fill_wjets"].as<bool>(true);

    // truth match leptons
    m_truth_match = get_config()["truth_match"].as<bool>(true);

    // add primary vertex variables
    m_do_PV = get_config()["do_PV"].as<bool>(false);

    // channels
    m_split_by_period = get_config()["split_by_period"].as<bool>(true);
    m_split_by_charge = get_config()["split_by_charge"].as<bool>(false);

    // D-meson reconstruction
    m_D_plus_meson = get_config()["D_plus_meson"].as<bool>(true);
    m_D_star_meson = get_config()["D_star_meson"].as<bool>(false);
    m_D_star_pi0_meson = get_config()["D_star_pi0_meson"].as<bool>(false);
    m_D_s_meson = get_config()["D_s_meson"].as<bool>(false);

    // Enable Tight Primary track selection (Loose is default, ie false)
    m_D_tightPrimary_tracks = get_config()["D_tightPrimary_tracks"].as<bool>(false);

    // D-meson truth
    m_truth_matchD = get_config()["truth_matchD"].as<bool>(true);
    m_truth_D = get_config()["truth_D"].as<bool>(false);

    // Single entry D-meson regions
    m_do_single_entry_D = get_config()["do_single_entry_D"].as<bool>(false);
    m_do_single_entry_D_only = get_config()["do_single_entry_D_only"].as<bool>(false);

    // D-meson N-1
    m_do_Nminus1 = get_config()["do_Nminus1"].as<bool>(false);

    // Keep all matched D's from N-1 even if the fail 2 or more cuts
    m_do_twice_failed = get_config()["do_twice_failed"].as<bool>(false);

    // Scan over cut to find optimum value. Int value corresponds to Dplus cutflow in DMesonRec::GetDPlusMesons
    // 0 is off
    m_do_cutScan = get_config()["do_cutScan"].as<int>(false);

    // Filling SB/SR Regions
    m_split_into_SB = get_config()["split_into_SB"].as<bool>(false);

    // apply truth fiducial cuts
    m_fiducial_selection = get_config()["fiducial_selection"].as<bool>(false);

    // save truth wjets quantities
    m_save_truth_wjets = get_config()["save_truth_wjets"].as<bool>(false);

    // pT(V) reweight
    m_do_pt_v_rw = get_config()["do_pt_v_rw"].as<bool>(false);

    // charm production fraction reweight
    m_do_prod_fraction_rw = get_config()["do_prod_fraction_rw"].as<bool>(true);

    // reweight wjets samples
    m_do_wjets_rw = get_config()["do_wjets_rw"].as<bool>(true);

    // extra lepton histograms
    m_do_extra_histograms = get_config()["do_extra_histograms"].as<bool>(false);

    //parametrize electrons with topoetcone
    m_do_el_topocone_param = get_config()["do_el_topocone_param"].as<bool>(false);

    // jet multiplicity requirement
    m_jet_selection = get_config()["jet_selection"].as<bool>(true);

    // use track-jets
    m_track_jet_selection = get_config()["track_jet_selection"].as<bool>(false);

    // use the 2-tag region separatelly
    m_two_tag_region = get_config()["two_tag_region"].as<bool>(false);

    // Matrix Method
    m_fake_rate_measurement = get_config()["fake_rate_measurement"].as<bool>(false);

    // Matrix Method Eval
    m_eval_fake_rate = get_config()["eval_fake_rate"].as<bool>(true);

    // Fake Factor instead of Matrix Method
    m_fake_factor_method = get_config()["fake_factor_method"].as<bool>(false);

    // Reweight MM from Closure
    m_rw_mm_closure = get_config()["do_rw_mm_closure"].as<bool>(false);

    // Bin D mesons in pT
    m_bin_in_D_pt = get_config()["bin_in_D_pt"].as<bool>(false);

    // Bin D mesons in truth pT
    m_bin_in_truth_D_pt = get_config()["bin_in_truth_D_pt"].as<bool>(false);

    // Bin D mesons in lepton eta
    m_bin_in_lep_eta = get_config()["bin_in_lep_eta"].as<bool>(false);

    // Bin D mesons in truth lepton eta
    m_bin_in_truth_lep_eta = get_config()["bin_in_truth_lep_eta"].as<bool>(false);

    // Signal only (only save the Matched categories)
    m_signal_only = get_config()["signal_only"].as<bool>(false);

    // inclusive fake rates (instead of fake rates for W+D)
    m_inclusive_fake_rate = get_config()["inclusive_fake_rate"].as<bool>(false);

    // split SR into SR and Anti_SR
    m_loose_w_selection = get_config()["loose_w_selection"].as<bool>(false);

    // skip MET, mT, and pT(lep) cuts entierly
    m_loose_w_selection_only = get_config()["loose_w_selection_only"].as<bool>(false);

    // apply luminosity
    m_apply_lumi = get_config()["apply_lumi"].as<bool>(true);

    // reweight spg samples
    m_reweight_spg = get_config()["reweight_spg"].as<bool>(true);

    // debug print-out for D mesons
    m_debug_dmeson_printout = get_config()["debug_dmeson_printout"].as<bool>(false);

    // set into to set Name of D branch for systematic variations
    m_D_branch_sys = get_config()["D_branch_sys"].as<std::string>("");

    // use AntiTight MET
    m_use_anti_tight_met = get_config()["use_anti_tight_met"].as<bool>(false);

    // Run mT validation region in the standard analysis
    m_run_val_region = get_config()["run_val_region"].as<bool>(false);

    // Run appreviated regions in MM Closure
    m_less_closure_region = get_config()["less_closure_region"].as<bool>(false);
}

WQCDCharmLoopBase::WQCDCharmLoopBase(TString input_file, TString out_path, TString tree_name)
    : EventLoopBase(input_file, out_path, tree_name),
      DMesonRec(),
      TruthInfo(),
      m_eta_bins_el{0, 0.70, 1.37, 1.52, 2.01, 2.47},
      m_eta_bins_mu{0, 1.10, 2.01, 2.5},
      m_met_bins_lep{0, 15.0, 30.0, 45.0, 60.0, 200.0}
{
    add_channel("inclusive", {"all", "one_loose_lepton", "one_tight_lepton", "trigger", "trigger_match"});
}

}  // namespace Charm
