#ifndef CHARM_DEFINITIONS
#define CHARM_DEFINITIONS

namespace Charm
{

static const double GeV = 0.001;
static const double MeV = 1.0;

static const double mm = 1.0;
static const double mum = 1000;

static const double ELECTRON_MASS = 0.5109989461;
static const double MUON_MASS = 105.6583745;
static const double PION_MASS = 139.57018;
static const double PION0_MASS = 134.976;
static const double KAON_MASS = 493.677;
static const double PHI_MASS = 1019.461;
static const double ETA_MASS = 547.853;

static const double LUMI_2015 = 3244.54;
static const double LUMI_2016 = 33402.2;
static const double LUMI_2017 = 44630.6;
static const double LUMI_2018 = 58791.6;
static const double LUMI_RUN2 = 140068.94;

static const float D0_MASS = 1864.83;
static const float DP_MASS = 1870.;
static const float DS_MASS = 1968.30;
static const float D0_MASS_SAT = 1600.;
static const float DSTAR_MASS = 2010.27;

}  // namespace Charm

#endif  // CHARM_DEFINITIONS
