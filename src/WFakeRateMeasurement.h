#ifndef W_FAKE_RATE_MEASUREMENT_H
#define W_FAKE_RATE_MEASUREMENT_H

#include "WQCDCharmLoopBase.h"

namespace Charm
{
class WFakeRateMeasurement : public WQCDCharmLoopBase
{
public:
    WFakeRateMeasurement(TString input_file, TString out_path, TString tree_name = "CharmAnalysis");

protected:
    virtual void connect_branches() override;

private:
    virtual int execute() override;
};

}  // namespace Charm

#endif  // W_FAKE_RATE_MEASUREMENT_H
