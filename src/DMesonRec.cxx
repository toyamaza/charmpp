#include "DMesonRec.h"
#include "WQCDCharmLoopBase.h"

#include <iostream>

#include "TMath.h"
#include "TVector2.h"

namespace Charm
{
void DMesonRec::get_D_plus_mesons(EventLoopBase *ELB, bool loose_selection)
{
    get_D_presel(ELB, 4);

    m_D_plus_mesons.clear();

    for (unsigned int index = 0; index < m_presel_Dplus.size(); index++)
    {
        unsigned int i = m_presel_Dplus.at(index);
        int failed_cut = 0;

        // SV χ2 requirement
        if (m_DMesons_fitOutput__Chi2->at(i) > 8.)
        {
            if (cut_fail_condition(1, failed_cut))
                continue;
        }

        // daughter tracks pt
        if (D_meson_min_track_pt(i) * Charm::MeV < 800.)
        {
            //if (cut_fail_condition(2, failed_cut))
                continue;
        }

        // daughter dR separation
        if (D_meson_max_track_dR(i) > 0.6)
        {
            //if (cut_fail_condition(3, failed_cut))
                continue;
        }

        // min Lxy requirement
        if (m_DMesons_pt->at(i) * Charm::GeV < 40.0)
        {
            if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 1.1)
            {
                if (cut_fail_condition(4, failed_cut))
                    continue;
            }
        }
        else
        {
            if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 2.5)
            {
                if (cut_fail_condition(4, failed_cut))
                    continue;
            }
        }

        // max Lxy requirement (set to exclude beampipe @ 24)
        //if (fabs(m_DMesons_fitOutput__Lxy->at(i) - 24) * Charm::mm < 2.0)
        //{
        //    //if (cut_fail_condition(4, failed_cut))
        //        continue;
        //}

        // impact parameter
        float d0_cut = 1.0;
        if (loose_selection)
        {
            d0_cut = 10.0;
        }
        if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut)
        {
            //if (cut_fail_condition(5, failed_cut))
                continue;
        }

        // combinatorial background rejection
        if (m_DMesons_costhetastar->at(i) < -0.8)
        {
            //if (cut_fail_condition(6, failed_cut))
                continue;
        }

        // D*+ rejection
        if ((std::min(m_DMesons_mKpi1->at(i),
                      m_DMesons_mKpi2->at(i))) * Charm::MeV < 160)
        {
            //if (cut_fail_condition(7, failed_cut))
                continue;
        }

        // Ds rejection
        if ((std::min(m_DMesons_mPhi1->at(i),
                      m_DMesons_mPhi2->at(i))) * Charm::MeV < 8.0)
        {
            //if (cut_fail_condition(8, failed_cut))
                continue;
        }

        // Isolation of Dplus (NEW CUT wrt Run 1)
        // No cut when measuring fake rates to increase statistics
        if (!loose_selection)
        {
            if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(i) > 1.)
            {
                if (cut_fail_condition(10, failed_cut))
                    continue;
            }
        }

        // D+ meson eta
        if (fabs(m_DMesons_eta->at(i)) > 2.2)
        {
            //if (cut_fail_condition(11, failed_cut))
            continue;
        }

        // D+ meson pt
        if (m_DMesons_pt->at(i) * Charm::GeV < 8.0 || m_DMesons_pt->at(i) * Charm::GeV > 150.0)
        {
            //if (cut_fail_condition(12, failed_cut))
            continue;
        }

        // D+ 3D Impact Significance Cut
        // No cut when measuring fake rates to increase statistics
        if (!loose_selection)
        {
            if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(i)) > 4.0)
            {
                if (cut_fail_condition(13, failed_cut))
                    continue;
            }
        }

        // invariant mass cut
        if (m_DMesons_m->at(i) * Charm::GeV <= 1.7 ||
            m_DMesons_m->at(i) * Charm::GeV > 2.2)
            continue;

        // save D+ candidate index
        std::pair<unsigned int, int> pair = std::make_pair(i, failed_cut);
        m_D_plus_mesons.push_back(pair);
    }
}

void DMesonRec::get_D_star_mesons(EventLoopBase *ELB, bool loose_selection)
{
    get_D_presel(ELB, 1);
    int D0Index;
    m_D_star_mesons.clear();

    for (unsigned int index = 0; index < m_presel_Dstar.size(); index++)
    {
        unsigned int i = m_presel_Dstar.at(index);
        int failed_cut = 0;
        D0Index = m_DMesons_D0Index->at(i);

        // SV χ2 requirement
        if (m_DMesons_fitOutput__Chi2->at(i) > 10.)
        {
            if (cut_fail_condition(1, failed_cut))
                continue;
        }

        // daughter tracks pt
        if (D_meson_min_track_pt(D0Index) * Charm::MeV < 600.)
        {
            //if (cut_fail_condition(2, failed_cut))
                continue;
        }

        // daughter dR separation
        if (D_meson_max_track_dR(D0Index) > 0.6)
        {
            //if (cut_fail_condition(3, failed_cut))
                continue;
        }

        if (!loose_selection)
        {
            // impact significance
            if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(D0Index)) > 4.0)
            {
                if (cut_fail_condition(14, failed_cut))
                    continue;
            }
        }

        // min Lxy requirement
        if (m_DMesons_fitOutput__Lxy->at(D0Index) * Charm::mm < 0.0)
        {
            if (cut_fail_condition(4, failed_cut))
                continue;
        }

        // max Lxy requirement
        //if (fabs(m_DMesons_fitOutput__Lxy->at(i) - 24) * Charm::mm < 2.0)
        //{
        //    //if (cut_fail_condition(4, failed_cut))
        //        continue;
        //}

        // impact parameter
        float d0_cut = 1.0;
        if (loose_selection)
        {
            d0_cut = 10.0;
        }
        if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut)
        {
            if (cut_fail_condition(5, failed_cut))
                continue;
        }

        // D0 Mass window
        if (fabs(m_DMesons_m->at(D0Index) - D0_MASS) > 40)
        {
            //if (cut_fail_condition(6, failed_cut))
                continue;
        }

        if (!loose_selection)
        {
            // Isolation
            if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(i) > 1.0)
            {
                if (cut_fail_condition(10, failed_cut))
                    continue;
            }
        }

        // soft pion pt
        if (D_meson_min_track_pt(i) * Charm::MeV < 500.)
        {
            //if (cut_fail_condition(8, failed_cut))
                continue;
        }

        // Soft Pion dR
        if (soft_pion_Dstar_separation(i, D0Index) > .3)
        {
            //if (cut_fail_condition(9, failed_cut))
                continue;
        }

        // Soft pion d0
        if (fabs(m_DMesons_SlowPionD0->at(i)) > 1)
        {
            //if (cut_fail_condition(7, failed_cut))
                continue;
        }

        // D*+ meson eta
        if (fabs(m_DMesons_eta->at(i)) > 2.2)
        {
            //if (cut_fail_condition(12, failed_cut))
            continue;
        }

        // D*+ meson pt
        if (m_DMesons_pt->at(i) * Charm::GeV < 8.0 || m_DMesons_pt->at(i) * Charm::GeV > 150.0)
        {
            //if (cut_fail_condition(13, failed_cut))
            continue;
        }

        // invariant mass cut
        if (m_DMesons_m->at(i) <= 1700. ||
            m_DMesons_m->at(i) > 2200.)
            continue;

        // save D* candidate index
        std::pair<unsigned int, int> pair = std::make_pair(i, failed_cut);
        m_D_star_mesons.push_back(pair);
    }
}

void DMesonRec::get_D_star_pi0_mesons(EventLoopBase *ELB, bool loose_selection)
{
    get_D_presel(ELB, 2);
    int D0Index;
    m_D_star_pi0_mesons.clear();

    for (unsigned int index = 0; index < m_presel_Dstar_pi0.size(); index++)
    {
        unsigned int i = m_presel_Dstar_pi0.at(index);

        int failed_cut = 0;

        D0Index = m_DMesons_D0Index->at(i);

        // SV χ2 requirement **CUT
        if (m_DMesons_fitOutput__Chi2->at(i) > 8.)
        {
            if (cut_fail_condition(1, failed_cut))
                continue;
        }

        // daughter tracks pt **CUT
        if (D_meson_min_track_pt(D0Index) * Charm::MeV < 800.)
        {
            if (cut_fail_condition(2, failed_cut))
                continue;
        }

        // daughter dR separation
        if (D_meson_max_track_dR(D0Index) > 0.6)
        {
            if (cut_fail_condition(3, failed_cut))
                continue;
        }

        // min Lxy requirement
        if (m_DMesons_fitOutput__Lxy->at(D0Index) * Charm::mm < 0.3)
        {
            if (cut_fail_condition(4, failed_cut))
                continue;
        }

        // max Lxy requirement
        //if (fabs(m_DMesons_fitOutput__Lxy->at(i) - 24) * Charm::mm < 2.0)
        //{
        //       if (cut_fail_condition(4, failed_cut))
        //        continue;
        //}

        // impact parameter
        if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > 10.0)
        {
            if (cut_fail_condition(5, failed_cut))
                continue;
        }

        // impact significance **CUT
        if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(i)) > 6.0)
        {
            if (cut_fail_condition(14, failed_cut))
                continue;
        }

        // D0 Mass window
        if ((m_DMesons_m->at(D0Index) * Charm::GeV) < 1.5 || (m_DMesons_m->at(D0Index) * Charm::GeV) > 1.7)
        {
            if (cut_fail_condition(6, failed_cut))
                continue;
        }

        // Isolation
        if (!loose_selection)
        {
            if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(D0Index) > 2.0)
            {
                if (cut_fail_condition(7, failed_cut))
                    continue;
            }
        }

        // soft pion pt
        if (D_meson_min_track_pt(i) * Charm::MeV < 500.)
        {
            if (cut_fail_condition(8, failed_cut))
                continue;
        }

        // Soft Pion dR
        if (soft_pion_Dstar_separation(i, D0Index) > .3)
        {
            if (cut_fail_condition(9, failed_cut))
                continue;
        }

        // Soft pion d0
        if (fabs(m_DMesons_SlowPionD0->at(i)) > 1)
        {
            if (cut_fail_condition(11, failed_cut))
                continue;
        }

        // D+ meson eta
        if (fabs(m_DMesons_eta->at(i)) > 2.2)
        {
            //if (cut_fail_condition(12, failed_cut))
            continue;
        }

        // D+ meson pt
        if (m_DMesons_pt->at(i) * Charm::GeV < 8.0)
        {
            //if (cut_fail_condition(13, failed_cut))
            continue;
        }

        //         // invariant mass cut
        //         if (m_DMesons_m->at(i) <= 1700. ||
        //             m_DMesons_m->at(i) > 2200.)
        //             continue;

        // save D* candidate index
        std::pair<unsigned int, int> pair = std::make_pair(i, failed_cut);
        m_D_star_pi0_mesons.push_back(pair);
    }
}

void DMesonRec::get_D_s_mesons(EventLoopBase *ELB, bool loose_selection)
{
    get_D_presel(ELB, 5);

    m_D_s_mesons.clear();

    for (unsigned int index = 0; index < m_presel_Ds.size(); index++)
    {
        unsigned int i = m_presel_Ds.at(index);
        int failed_cut = 0;

        // SV χ2 requirement
        if (m_DMesons_fitOutput__Chi2->at(i) > 6. && (m_do_cutScan != 1))
        {
            if (cut_fail_condition(1, failed_cut))
                continue;
        }

        // daughter tracks pt
        if (D_meson_min_track_pt(i) * Charm::MeV < 800.)
        {
            if (cut_fail_condition(2, failed_cut))
                continue;
        }

        // daughter dR separation
        if (D_meson_max_track_dR(i) > 0.6)
        {
            if (cut_fail_condition(3, failed_cut))
                continue;
        }

        if (!loose_selection)
        {
            // impact significance
            if (fabs(m_DMesons_fitOutput__ImpactSignificance->at(i)) > 6.0)
            {
                if (cut_fail_condition(14, failed_cut))
                    continue;
            }
        }

        // min Lxy requirement
        if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 0.5)
        {
            if (cut_fail_condition(4, failed_cut))
                continue;
        }

        // max Lxy requirement
        //if (fabs(m_DMesons_fitOutput__Lxy->at(i) - 24) * Charm::mm < 2.0)
        //{
        //    //if (cut_fail_condition(4, failed_cut))
        //        continue;
        //}

        // impact parameter
        float d0_cut = 1.0;
        if (loose_selection)
        {
            d0_cut = 10.0;
        }
        if (fabs(m_DMesons_fitOutput__Impact->at(i)) * Charm::mm > d0_cut)
        {
            if (cut_fail_condition(5, failed_cut))
                continue;
        }

        // combinatorial background rejection
        if (m_DMesons_costhetastar->at(i) > 0.8)
        {
            if (cut_fail_condition(6, failed_cut))
                continue;
        }

        // D+ rejection
        if (fabs(m_DMesons_mPhi1->at(i) - PHI_MASS) > 8.0)
        {
            if (cut_fail_condition(8, failed_cut))
                continue;
        }

        // Isolation of Ds (NEW CUT wrt Run 1)
        if (!loose_selection)
        {
            if (m_DMesons_ptcone40->at(i) / m_DMesons_pt->at(i) > 2.)
            {
                if (cut_fail_condition(10, failed_cut))
                    continue;
            }
        }
        
        // D+ meson eta
        if (fabs(m_DMesons_eta->at(i)) > 2.2)
        {
            //if (cut_fail_condition(11, failed_cut))
            continue;
        }

        // D+ meson pt
        if (m_DMesons_pt->at(i) * Charm::GeV < 8.0)
        {
            //if (cut_fail_condition(12, failed_cut))
            continue;
        }

        // invariant mass cut
        if (m_DMesons_m->at(i) * Charm::GeV <= 1.7 ||
            m_DMesons_m->at(i) * Charm::GeV > 2.2)
            continue;

        // save Ds candidate index
        std::pair<unsigned int, int> pair = std::make_pair(i, failed_cut);
        m_D_s_mesons.push_back(pair);
    }
}

void DMesonRec::get_D_presel(EventLoopBase *ELB, int decayType)
{
    if (decayType == 4)
        m_presel_Dplus.clear();
    else if (decayType == 1)
        m_presel_Dstar.clear();
    else if (decayType == 5)
        m_presel_Ds.clear();
    else if (decayType == 2)
        m_presel_Dstar_pi0.clear();

    TruthInfo *TRI = dynamic_cast<TruthInfo *>(ELB);

    for (unsigned int i = 0; i < m_DMesons_decayType->size(); i++)
    {
        // Correct decay type
        if (m_DMesons_decayType->at(i) != decayType)
            continue;

        if (m_D_tightPrimary_tracks && !D_pass_tight(i))
            continue;

        // Determines if evaluating only matched D's is necessary (eff for example)
        std::pair<unsigned int, int> fakeDpair = std::make_pair(i, 0);
        if (m_do_twice_failed && TRI->TruthInfo::get_truth_category(ELB, fakeDpair) != "Matched")
            continue;

        // Baseline Lxy
        if (m_DMesons_fitOutput__Lxy->at(i) * Charm::mm < 0.0)
            continue;

        if (decayType == 4)
            m_presel_Dplus.push_back(i);
        else if (decayType == 1)
            m_presel_Dstar.push_back(i);
        else if (decayType == 2)
            m_presel_Dstar_pi0.push_back(i);
        else if (decayType == 5)
        {
            m_presel_Ds.push_back(i);
        }
    }
}

std::pair<unsigned int, int> DMesonRec::get_best_D_candidate(std::vector<std::pair<unsigned int, int>> candidates)
{
    unsigned int best_index = 0;
    int best_failed_cut = -1;
    float best_chi2 = -1;
    for (auto &pair : candidates)
    {
        unsigned int index = pair.first;
        int failed_cut = pair.second;
        if (best_chi2 < 0)
        {
            best_chi2 = m_DMesons_fitOutput__Chi2->at(index);
            best_index = index;
            best_failed_cut = failed_cut;
            continue;
        }
        else
        {
            if (m_DMesons_fitOutput__Chi2->at(index) < best_chi2)
            {
                best_chi2 = m_DMesons_fitOutput__Chi2->at(index);
                best_index = index;
                best_failed_cut = failed_cut;
            }
        }
    }
    return std::pair<unsigned int, int>{best_index, best_failed_cut};
}

float DMesonRec::D_meson_min_track_pt(unsigned int index)
{
    return *std::min_element(
        m_DMesons_daughterInfo__pt->at(index).begin(),
        m_DMesons_daughterInfo__pt->at(index).end());
}

float DMesonRec::D_meson_max_track_dR(unsigned int index)
{
    float dRmax = 0;
    std::vector<float> *daughterInfo__eta = &m_DMesons_daughterInfo__eta->at(index);
    std::vector<float> *daughterInfo__phi = &m_DMesons_daughterInfo__phi->at(index);
    for (unsigned int i = 0; i < daughterInfo__eta->size(); i++)
    {
        for (unsigned int j = i + 1; j < daughterInfo__eta->size(); j++)
        {
            float deta = daughterInfo__eta->at(i) - daughterInfo__eta->at(j);
            float dphi = TVector2::Phi_mpi_pi(daughterInfo__phi->at(i) - daughterInfo__phi->at(j));
            float dR = TMath::Sqrt(deta * deta + dphi * dphi);
            if (dR > dRmax)
                dRmax = dR;
        }
    }
    return dRmax;
}

float DMesonRec::D_meson_lepton_dR(unsigned int index)
{
    float deta = m_DMesons_eta->at(index) - m_lep->Eta();
    float dphi = TVector2::Phi_mpi_pi(m_DMesons_phi->at(index) - m_lep->Phi());
    float dR = TMath::Sqrt(deta * deta + dphi * dphi);
    return dR;
}

float DMesonRec::D_meson_lepton_dR(unsigned int index, float lep_eta, float lep_phi)
{
    float deta = m_DMesons_eta->at(index) - lep_eta;
    float dphi = TVector2::Phi_mpi_pi(m_DMesons_phi->at(index) - lep_phi);
    float dR = TMath::Sqrt(deta * deta + dphi * dphi);
    return dR;
}

float DMesonRec::soft_pion_Dstar_separation(unsigned int index, unsigned int D0Index)
{
    float eta[2] = {m_DMesons_daughterInfo__eta->at(index).back(), m_DMesons_eta->at(D0Index)};
    float phi[2] = {m_DMesons_daughterInfo__phi->at(index).back(), m_DMesons_phi->at(D0Index)};
    float deta = eta[0] - eta[1];
    float dphi = TVector2::Phi_mpi_pi(phi[0] - phi[1]);
    return TMath::Sqrt(deta * deta + dphi * dphi);
}

bool DMesonRec::D_pass_tight(unsigned int index)
{
    if (std::count(m_DMesons_daughterInfo__passTight->at(index).begin(),
                   m_DMesons_daughterInfo__passTight->at(index).end(), false))
        return false;
    else
        return true;
}

bool DMesonRec::cut_fail_condition(int cut_number, int &failed_cut)
{
    //Do not cut on variables if we are scanning different values
    if (cut_number == m_do_cutScan)
        return false;
    if (m_do_Nminus1 && failed_cut == 0)
    {
        failed_cut = cut_number;
        return false;
    }
    else if (m_do_Nminus1 && failed_cut > 0 && m_do_twice_failed)
    {
        failed_cut = 999;
        return false;
    }

    //If other specific analyses fail, normal cutflow rule take precedence
    return true;
}

bool DMesonRec::pass_ith_cut(unsigned int i, unsigned int index)
{
    if (m_do_cutScan == 4)
    {
        if (m_DMesons_fitOutput__Lxy->at(index) > m_cut_vals_Lxy[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 11)
    {
        if ((m_DMesons_fitOutput__Lxy->at(index) * Charm::mm *
             m_DMesons_m->at(index) / m_DMesons_pt->at(index)) > m_cut_vals_Ctau[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 1)
    {
        if (m_DMesons_fitOutput__Chi2->at(index) < m_cut_vals_Chi2[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 2)
    {
        if (D_meson_min_track_pt(index) * Charm::MeV > m_cut_vals_TrPt[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 6)
    {
        if (m_DMesons_costhetastar->at(index) > m_cut_vals_CosThetaStar[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 8)
    {
        if (m_DMesons_mPhi1->at(index) < m_cut_vals_Dplus_Ds_test[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 10)
    {
        if (m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index) < m_cut_vals_Iso[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 12)
    {
        if (m_DMesons_pt->at(index) * Charm::GeV > m_cut_vals_Pt[i])
            return true;
        else
            return false;
    }
    if (m_do_cutScan == 14)
    {
        if (m_DMesons_fitOutput__ImpactSignificance->at(index) < m_cut_vals_3DimpactSig[i])
            return true;
        else
            return false;
    }
    else
        return false;
}

std::string DMesonRec::get_charge_prefix(float lep_charge, unsigned int index)
{
    if (lep_charge == 0)
    {
        if (m_DMesons_pdgId->at(index) > 0)
        {
            return "_per_D";
        }
        else
        {
            return "_per_antiD";
        }
    }
    int charge;
    if (m_DMesons_pdgId->at(index) > 0)
    {
        charge = 1;
    }
    else
    {
        charge = -1;
    }
    if (charge * lep_charge < 0)
    {
        return "_OS";
    }
    else
    {
        return "_SS";
    }
}

std::string DMesonRec::D_meson_pt_bin(EventLoopBase* ELB, unsigned int index)
{
    float D_pt = m_DMesons_pt->at(index) * Charm::GeV;
    if (D_pt < ELB->m_differential_bins.get_differential_bins()[0])
    {
        return "_pt_bin0";
    }
    else if (D_pt >= ELB->m_differential_bins.get_differential_bins()[0] && D_pt < ELB->m_differential_bins.get_differential_bins()[1])
    {
        return "_pt_bin1";
    }
    else if (D_pt >= ELB->m_differential_bins.get_differential_bins()[1] && D_pt < ELB->m_differential_bins.get_differential_bins()[2])
    {
        return "_pt_bin2";
    }
    else if (D_pt >= ELB->m_differential_bins.get_differential_bins()[2] && D_pt < ELB->m_differential_bins.get_differential_bins()[3])
    {
        return "_pt_bin3";
    }
    else if (D_pt >= ELB->m_differential_bins.get_differential_bins()[3] && D_pt < ELB->m_differential_bins.get_differential_bins()[4])
    {
        return "_pt_bin4";
    }
    else
    {
        return "_pt_bin5";
    }
}

std::string DMesonRec::D_meson_eta_bin(EventLoopBase *ELB, int i)
{
    WQCDCharmLoopBase* WQCD = dynamic_cast<WQCDCharmLoopBase*>(ELB);
    int eta_bin = ELB->m_differential_bins_eta.get_eta_bin(WQCD->get_sys_lep_abs_eta().at(i)) + 1;
    if (eta_bin > 0)
    {
        return std::string("_eta_bin") + std::to_string(eta_bin);
    }
    return "_eta_bin0";
}

bool DMesonRec::fill_D_plus_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dplus_index, std::vector<bool> extra_flag, std::vector<double> extra_weight, bool diff_bins_override)
{
    unsigned int index = Dplus_index.first;
    int failed_cut = Dplus_index.second;
    std::vector<std::string> suffixes = channels;
    if (!diff_bins_override)
    {
        if (m_bin_in_D_pt)
        {
            for (unsigned int i = 0; i < channels.size(); i++)
            {
                suffixes.at(i) += D_meson_pt_bin(ELB, index);
            }
        }
        else if (m_bin_in_lep_eta)
        {
            for (unsigned int i = 0; i < channels.size(); i++)
            {
                suffixes.at(i) += D_meson_eta_bin(ELB, i);
            }
        }
    }

    if (failed_cut == 0)
    {
        ELB->add_fill_histogram_sys("Dmeson_m", suffixes, m_sys_dmeson_mass, 200, 1.4, 2.4, true, extra_flag, extra_weight);
        if (!ELB->m_fit_variables_only)
        {
            ELB->add_fill_histogram_sys("Dmeson_pt", suffixes, m_DMesons_pt->at(index) * Charm::GeV, 150, 0, 150., true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index), 100, -3.0, 3.0, true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index), 100, -M_PI, M_PI, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_SVr", suffixes,
                                        sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) +
                                            pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)),
                                        1000, 0, 100, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_max_trackZ0", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinTheta->at(index).begin(), m_DMesons_daughterInfo__z0SinTheta->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
                                        150, -150, 150., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_max_trackZ0PV", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(), m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })),
                                        360, -6, 6., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_cTau", suffixes, m_DMesons_fitOutput__Lxy->at(index) * m_DMesons_m->at(index) / m_DMesons_pt->at(index), 200, 0, 2.0, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_z0sinTheta", suffixes, m_DMesons_fitOutput__ImpactZ0SinTheta->at(index), 200, -2, 2, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_kaonPassTight", suffixes, m_DMesons_daughterInfo__passTight->at(index).at(0), 2, 0, 2, false, extra_flag, extra_weight);
        }
    }

    if (ELB->m_fit_variables_only)
    {
        if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override)
        {
            fill_D_plus_histograms(ELB, channels, Dplus_index, extra_flag, extra_weight, true);
        }
        return true;
    }

    if (failed_cut == 0 || failed_cut == 1)
    {
        ELB->add_fill_histogram_sys("Dmeson_chi2", suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.0, true, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_chi2", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
            if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
                ELB->add_fill_histogram_sys("Dmeson_chi2_inSB", suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.5, false, extra_flag, extra_weight);
            else if(m_DMesons_m->at(index) - DP_MASS > 70)
                ELB->add_fill_histogram_sys("Dmeson_chi2_outSB_R", suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.5, false, extra_flag, extra_weight);
            else
                ELB->add_fill_histogram_sys("Dmeson_chi2_outSB_L", suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.5, false, extra_flag, extra_weight);
            
        }
    }
    if (failed_cut == 0 || failed_cut == 2)
    {
        ELB->add_fill_histogram_sys("Dmeson_min_trackPt", suffixes, D_meson_min_track_pt(index) * Charm::GeV, 100, 0, 10., true, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_min_trackPt", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 3)
    {
        ELB->add_fill_histogram_sys("Dmeson_max_trackdR", suffixes, D_meson_max_track_dR(index), 100, 0, 1.2, true, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_max_trackdR", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 13)
    {
        ELB->add_fill_histogram_sys("Dmeson_impact_sig", suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, true, extra_flag, extra_weight);
        if (m_do_Nminus1) {
            ELB->add_fill_histogram_sys("Dmeson_m_impact_sig", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
            if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
                ELB->add_fill_histogram_sys("Dmeson_impact_sig_inSB", suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, false, extra_flag, extra_weight);
            else if(m_DMesons_m->at(index) - DP_MASS > 70)
                ELB->add_fill_histogram_sys("Dmeson_impact_sig_outSB_R", suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, false, extra_flag, extra_weight);
            else
                ELB->add_fill_histogram_sys("Dmeson_impact_sig_outSB_L", suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 4)
    {
        ELB->add_fill_histogram_sys("Dmeson_Lxy", suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., true, extra_flag, extra_weight);
        if (m_do_Nminus1) {
            ELB->add_fill_histogram_sys("Dmeson_m_Lxy", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
            if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
                ELB->add_fill_histogram_sys("Dmeson_Lxy_inSB", suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
            else if(m_DMesons_m->at(index) - DP_MASS > 70)
                ELB->add_fill_histogram_sys("Dmeson_Lxy_outSB_R", suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
            else
                ELB->add_fill_histogram_sys("Dmeson_Lxy_outSB_L", suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 5)
    {
        ELB->add_fill_histogram_sys("Dmeson_d0", suffixes, m_DMesons_fitOutput__Impact->at(index) * Charm::mum, 200, -1100, 1100, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_d0", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 6)
    {
        ELB->add_fill_histogram_sys("Dmeson_cosThetaStar", suffixes, m_DMesons_costhetastar->at(index), 100, -1., 1., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_cosThetaStar", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 7)
    {
        ELB->add_fill_histogram_sys("Dmeson_DstarMin", suffixes, std::min(m_DMesons_mKpi1->at(index), m_DMesons_mKpi2->at(index)) * Charm::GeV, 100, 0, 1.6, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_DstarMin", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 8)
    {
        ELB->add_fill_histogram_sys("Dmeson_DsMin", suffixes, std::min(m_DMesons_mPhi1->at(index), m_DMesons_mPhi2->at(index)) * Charm::GeV, 250, 0., 2., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_DsMin", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
        }
    }
    if (failed_cut == 0 || failed_cut == 10)
    {
        ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt", suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, true, extra_flag, extra_weight);
        if (m_do_Nminus1)
        {
            ELB->add_fill_histogram_sys("Dmeson_m_ptcone40_pt", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
            if (fabs(m_DMesons_m->at(index) - DP_MASS) < 70.)
                ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt_inSB", suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, false, extra_flag, extra_weight);
            else if(m_DMesons_m->at(index) - DP_MASS > 70)
                ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt_outSB_R", suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, false, extra_flag, extra_weight);
            else
                ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt_outSB_L", suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        }
    }

    if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override)
    {
        fill_D_plus_histograms(ELB, channels, Dplus_index, extra_flag, extra_weight, true);
    }
    return true;
}

bool DMesonRec::fill_D_plus_histograms_scan(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dplus_index, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    unsigned int index = Dplus_index.first;
    std::vector<std::string> suffixes = channels;
    if (m_bin_in_D_pt)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_pt_bin(ELB, index);
        }
    }
    else if (m_bin_in_lep_eta)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_eta_bin(ELB, i);
        }
    }

    for (unsigned int i = 0; i < m_NcutVals; i++)
    {
        if (pass_ith_cut(i, index))
        {
            std::string cutStr = std::to_string(i);
            //Pt is always plotted, so an if is not needed for
            ELB->add_fill_histogram_sys("Dmeson_pt" + cutStr, suffixes, m_DMesons_pt->at(index) * Charm::GeV, 200, 0, 150., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_eta" + cutStr, suffixes, m_DMesons_eta->at(index), 100, -3.0, 3.0, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_m" + cutStr, suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
            if (m_do_cutScan == 4)
                ELB->add_fill_histogram_sys("Dmeson_Lxy" + cutStr, suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 11)
                ELB->add_fill_histogram_sys("Dmeson_ctau" + cutStr, suffixes, (m_DMesons_fitOutput__Lxy->at(index) * Charm::mm * m_DMesons_m->at(index) / m_DMesons_pt->at(index)), 100, 0, 2.0, false, extra_flag, extra_weight);
            else if (m_do_cutScan == 2)
                ELB->add_fill_histogram_sys("Dmeson_min_trackPt" + cutStr, suffixes, D_meson_min_track_pt(index) * Charm::GeV, 100, 0, 6., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 1)
            {
                ELB->add_fill_histogram_sys("Dmeson_chi2" + cutStr, suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 21., false, extra_flag, extra_weight);
                //ELB->add_fill_histogram_sys( "Dmeson_chi2_x_m"+cutStr, suffixes, m_DMesons_fitOutput__Chi2->at(index),  m_DMesons_m->at(index) * Charm::GeV, 100, 0, 21., 500, 1.4, 2.4, false, extra_flag, extra_weight);
            }
            else if (m_do_cutScan == 6)
                ELB->add_fill_histogram_sys("Dmeson_cosThetaStar" + cutStr, suffixes, m_DMesons_costhetastar->at(index), 100, -1., 1., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 10)
                ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt" + cutStr, suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, false, extra_flag, extra_weight);
            else if (m_do_cutScan == 14)
                ELB->add_fill_histogram_sys("Dmeson_impact_sig" + cutStr, suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        }
    }
    return true;
}

bool DMesonRec::fill_D_star_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dstar_index, std::vector<bool> extra_flag, std::vector<double> extra_weight, bool diff_bins_override)
{
    unsigned int index = Dstar_index.first;
    int failed_cut = Dstar_index.second;

    std::vector<std::string> suffixes = channels;
    if (!diff_bins_override)
    {
        if (m_bin_in_D_pt)
        {
            for (unsigned int i = 0; i < channels.size(); i++)
            {
                suffixes.at(i) += D_meson_pt_bin(ELB, index);
            }
        }
        else if (m_bin_in_lep_eta)
        {
            for (unsigned int i = 0; i < channels.size(); i++)
            {
                suffixes.at(i) += D_meson_eta_bin(ELB, i);
            }
        }
    }

    int D0Index = m_DMesons_D0Index->at(index);
    
    if (failed_cut == 0)
    {
        ELB->add_fill_histogram_sys("Dmeson_mdiff", suffixes, m_sys_dmeson_mass, 450, 135, 180, true, extra_flag, extra_weight);
        if (!ELB->m_fit_variables_only)
        {
            ELB->add_fill_histogram_sys("Dmeson_pt", suffixes, m_DMesons_pt->at(index) * Charm::GeV, 200, 0, 150., true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index), 100, -3.0, 3.0, true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index), 100, -M_PI, M_PI, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_m", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt", suffixes, m_DMesons_ptcone40->at(index)/m_DMesons_pt->at(index), 100, 0, 10, true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_max_trackZ0", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinTheta->at(index).begin(), m_DMesons_daughterInfo__z0SinTheta->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })), 150, -150, 150., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_max_trackZ0PV", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(), m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })), 360, -6, 6., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_cTau", suffixes, m_DMesons_fitOutput__Lxy->at(index) * m_DMesons_m->at(index) / m_DMesons_pt->at(index), 200, 0, 2.0, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_SVr", suffixes, sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) + pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)), 1000, 0, 100, false, extra_flag, extra_weight);
        }
    }

    if (ELB->m_fit_variables_only)
    {
        if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override)
        {
            fill_D_star_histograms(ELB, channels, Dstar_index, extra_flag, extra_weight, true);
        }
        return true;
    }

    if (failed_cut == 0 || failed_cut == 1)
    {
        ELB->add_fill_histogram_sys("Dmeson_chi2", suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 12.0, true, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_chi2", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 2)
    {
        ELB->add_fill_histogram_sys("Dmeson_min_D0trackPt", suffixes, *(std::min_element(m_DMesons_daughterInfo__pt->at(D0Index).begin(), m_DMesons_daughterInfo__pt->at(D0Index).end())) * Charm::GeV, 100, 0, 10., false, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_D0trackPt", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 3)
    {
        ELB->add_fill_histogram_sys("Dmeson_max_trackdR", suffixes, D_meson_max_track_dR(index), 100, 0, 1.2, true, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_max_trackd", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 14)
    {
        ELB->add_fill_histogram_sys("Dmeson_impact_sig", suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, true, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_m_impact_sig", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 4)
    {
        ELB->add_fill_histogram_sys("Dmeson_Lxy", suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., true, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_Lxy", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 5)
    {
        ELB->add_fill_histogram_sys("Dmeson_d0", suffixes, m_DMesons_fitOutput__Impact->at(index) * Charm::mum, 200, -1100, 1100, false, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_d0", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 6)
    {
        ELB->add_fill_histogram_sys("Dmeson_D0massDiff", suffixes, fabs(m_DMesons_m->at(D0Index) - D0_MASS), 100, 30.0, 70., false, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_D0massDiff", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 10)
    {
        ELB->add_fill_histogram_sys("Dmeson_D0pt_ptcone40", suffixes, m_DMesons_ptcone40->at(D0Index) / m_DMesons_pt->at(D0Index), 100, 0, 2, true, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_D0pt_ptcone40", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 8)
    {
        ELB->add_fill_histogram_sys("Dmeson_min_trackPt", suffixes, *(std::min_element(m_DMesons_daughterInfo__pt->at(index).begin(), m_DMesons_daughterInfo__pt->at(index).end())) * Charm::GeV, 100, 0, 10., true, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_min_trackPt", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 9)
    {
        ELB->add_fill_histogram_sys("Dmeson_softPiDR", suffixes, soft_pion_Dstar_separation(index, D0Index), 100, 0.0, 0.6, false, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_softPiDR", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 11)
    {
        ELB->add_fill_histogram_sys("Dmeson_softPiD0", suffixes, m_DMesons_SlowPionD0->at(index), 100, -1.0, 1.0, false, extra_flag, extra_weight);
        //if (m_do_Nminus1)
        //    ELB->add_fill_histogram_sys("Dmeson_mdiff_softPiD0", suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
    }

    if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override)
    {
        fill_D_star_histograms(ELB, channels, Dstar_index, extra_flag, extra_weight, true);
    }
    return true;
}

bool DMesonRec::fill_D_star_histograms_scan(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dstar_index, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    unsigned int index = Dstar_index.first;
    std::vector<std::string> suffixes = channels;
    if (m_bin_in_D_pt)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_pt_bin(ELB, index);
        }
    }
    else if (m_bin_in_lep_eta)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_eta_bin(ELB, i);
        }
    }

    for (unsigned int i = 0; i < m_NcutVals; i++)
    {
        if (pass_ith_cut(i, index))
        {
            std::string cutStr = std::to_string(i);
            //Pt is always plotted, so an if is not needed for
            ELB->add_fill_histogram_sys("Dmeson_pt" + cutStr, suffixes, m_DMesons_pt->at(index) * Charm::GeV, 200, 0, 150., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_eta" + cutStr, suffixes, m_DMesons_eta->at(index), 100, -3.0, 3.0, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_mdiff" + cutStr, suffixes, m_DMesons_DeltaMass->at(index), 1000, 135, 180, false, extra_flag, extra_weight);
            if (m_do_cutScan == 4)
                ELB->add_fill_histogram_sys("Dmeson_Lxy" + cutStr, suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 11)
                ELB->add_fill_histogram_sys("Dmeson_ctau" + cutStr, suffixes, (m_DMesons_fitOutput__Lxy->at(index) * Charm::mm * m_DMesons_m->at(index) / m_DMesons_pt->at(index)), 100, 0, 2.0, false, extra_flag, extra_weight);
            else if (m_do_cutScan == 2)
                ELB->add_fill_histogram_sys("Dmeson_min_trackPt" + cutStr, suffixes, D_meson_min_track_pt(index) * Charm::GeV, 100, 0, 6., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 1)
            {
                ELB->add_fill_histogram_sys("Dmeson_chi2" + cutStr, suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 21., false, extra_flag, extra_weight);
                //ELB->add_fill_histogram_sys( "Dmeson_chi2_x_m", suffixes, m_DMesons_fitOutput__Chi2->at(index),  m_DMesons_m->at(index) * Charm::GeV, 100, 0, 21., 500, 1.4, 2.4, false, extra_flag, extra_weight);
            }
            else if (m_do_cutScan == 6)
                ELB->add_fill_histogram_sys("Dmeson_cosThetaStar" + cutStr, suffixes, m_DMesons_costhetastar->at(index), 100, -1., 1., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 10)
                ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt" + cutStr, suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, false, extra_flag, extra_weight);
             else if (m_do_cutScan == 14)
                ELB->add_fill_histogram_sys("Dmeson_impact_sig" + cutStr, suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        }
    }
    return true;
}

bool DMesonRec::fill_D_star_pi0_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Dstar_pi0_index, std::vector<bool> extra_flag, std::vector<double> extra_weight, bool diff_bins_override)
{
    unsigned int index = Dstar_pi0_index.first;
    int failed_cut = Dstar_pi0_index.second;

    std::vector<std::string> suffixes = channels;
    if (!diff_bins_override)
    {
        if (m_bin_in_D_pt)
        {
            for (unsigned int i = 0; i < channels.size(); i++)
            {
                suffixes.at(i) += D_meson_pt_bin(ELB, index);
            }
        }
        else if (m_bin_in_lep_eta)
        {
            for (unsigned int i = 0; i < channels.size(); i++)
            {
                suffixes.at(i) += D_meson_eta_bin(ELB, i);
            }
        }
    }

    int D0Index = m_DMesons_D0Index->at(index);

    if (failed_cut == 0)
    {
        ELB->add_fill_histogram_sys("Dmeson_mdiff", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, true, extra_flag, extra_weight);
        if (!ELB->m_fit_variables_only)
        {
            ELB->add_fill_histogram_sys("Dmeson_pt", suffixes, m_DMesons_pt->at(index) * Charm::GeV, 200, 0, 150., true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index), 100, -3.0, 3.0, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index), 100, -M_PI, M_PI, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_m", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, true, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt", suffixes, m_DMesons_pt->at(index) / m_DMesons_ptcone40->at(index), 100, 0, 10, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_max_trackZ0", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinTheta->at(index).begin(), m_DMesons_daughterInfo__z0SinTheta->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })), 150, -150, 150., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_max_trackZ0PV", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(), m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })), 360, -6, 6., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_cTau", suffixes, m_DMesons_fitOutput__Lxy->at(index) * m_DMesons_m->at(index) / m_DMesons_pt->at(index), 200, 0, 2.0, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_SVr", suffixes, sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) + pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)), 1000, 0, 100, false, extra_flag, extra_weight);
        }
    }
    if (ELB->m_fit_variables_only)
    {
        if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override)
        {
            fill_D_star_pi0_histograms(ELB, channels, Dstar_pi0_index, extra_flag, extra_weight, true);
        }
        return true;
    }
    if (failed_cut == 0 || failed_cut == 1)
    {
        ELB->add_fill_histogram_sys("Dmeson_chi2", suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 6.5, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_chi2", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 2)
    {
        ELB->add_fill_histogram_sys("Dmeson_min_D0trackPt", suffixes, *(std::min_element(m_DMesons_daughterInfo__pt->at(D0Index).begin(), m_DMesons_daughterInfo__pt->at(D0Index).end())) * Charm::GeV, 100, 0, 10., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_D0trackPt", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 3)
    {
        ELB->add_fill_histogram_sys("Dmeson_max_trackdR", suffixes, D_meson_max_track_dR(index), 100, 0, 1.2, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_max_trackd", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 14)
    {
        ELB->add_fill_histogram_sys("Dmeson_impact_sig", suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_impact_sig", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 4)
    {
        ELB->add_fill_histogram_sys("Dmeson_Lxy", suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_Lxy", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 5)
    {
        ELB->add_fill_histogram_sys("Dmeson_d0", suffixes, m_DMesons_fitOutput__Impact->at(index) * Charm::mum, 200, -1100, 1100, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_d0", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 6)
    {
        ELB->add_fill_histogram_sys("Dmeson_D0massDiff", suffixes, fabs(m_DMesons_m->at(D0Index) - D0_MASS), 100, 30.0, 70., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_D0massDiff", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 10)
    {
        ELB->add_fill_histogram_sys("Dmeson_D0pt_ptcone40", suffixes, m_DMesons_ptcone40->at(D0Index) / m_DMesons_pt->at(D0Index), 100, 0, 2, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_D0pt_ptcone40", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 8)
    {
        ELB->add_fill_histogram_sys("Dmeson_min_trackPt", suffixes, *(std::min_element(m_DMesons_daughterInfo__pt->at(index).begin(), m_DMesons_daughterInfo__pt->at(index).end())) * Charm::GeV, 100, 0, 10., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_min_trackPt", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 9)
    {
        ELB->add_fill_histogram_sys("Dmeson_softPiDR", suffixes, soft_pion_Dstar_separation(index, D0Index), 100, 0.0, 0.6, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_softPiDR", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 11)
    {
        ELB->add_fill_histogram_sys("Dmeson_softPiD0", suffixes, m_DMesons_SlowPionD0->at(index), 100, -1.0, 1.0, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_mdiff_softPiD0", suffixes, m_DMesons_DeltaMass->at(index), 200, 100, 200, false, extra_flag, extra_weight);
    }

    if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override)
    {
        fill_D_star_pi0_histograms(ELB, channels, Dstar_pi0_index, extra_flag, extra_weight, true);
    }
    return true;
}

bool DMesonRec::fill_D_s_histograms(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Ds_index, std::vector<bool> extra_flag, std::vector<double> extra_weight, bool diff_bins_override)
{
    unsigned int index = Ds_index.first;
    int failed_cut = Ds_index.second;
    std::vector<std::string> suffixes = channels;

    if (m_bin_in_D_pt)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_pt_bin(ELB, index);
        }
    }
    else if (m_bin_in_lep_eta)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_eta_bin(ELB, i);
        }
    }

    if (failed_cut == 0)
    {
        ELB->add_fill_histogram_sys("Dmeson_pt", suffixes, m_DMesons_pt->at(index) * Charm::GeV, 200, 0, 150., true, extra_flag, extra_weight);
        ELB->add_fill_histogram_sys("Dmeson_eta", suffixes, m_DMesons_eta->at(index), 100, -3.0, 3.0, false, extra_flag, extra_weight);
        ELB->add_fill_histogram_sys("Dmeson_phi", suffixes, m_DMesons_phi->at(index), 100, -M_PI, M_PI, false, extra_flag, extra_weight);
        ELB->add_fill_histogram_sys("Dmeson_m", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, true, extra_flag, extra_weight);
        ELB->add_fill_histogram_sys("Dmeson_SVr", suffixes, sqrt(pow(m_DMesons_fitOutput__VertexPosition->at(index)[0], 2) + pow(m_DMesons_fitOutput__VertexPosition->at(index)[1], 2)), 1000, 0, 100, false, extra_flag, extra_weight);
        ELB->add_fill_histogram_sys("Dmeson_max_trackZ0", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinTheta->at(index).begin(), m_DMesons_daughterInfo__z0SinTheta->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })), 150, -150, 150., false, extra_flag, extra_weight);
        ELB->add_fill_histogram_sys("Dmeson_max_trackZ0PV", suffixes, *(std::max_element(m_DMesons_daughterInfo__z0SinThetaPV->at(index).begin(), m_DMesons_daughterInfo__z0SinThetaPV->at(index).end(), [](float z01, float z02) { return fabs(z02) > fabs(z01); })), 360, -6, 6., false, extra_flag, extra_weight);
        ELB->add_fill_histogram_sys("Dmeson_cTau", suffixes, m_DMesons_fitOutput__Lxy->at(index) * m_DMesons_m->at(index) / m_DMesons_pt->at(index), 200, 0, 2.0, false, extra_flag, extra_weight);
        if (m_lep)
        {
            ELB->add_fill_histogram_sys("Dmeson_dPhilep", suffixes, TVector2::Phi_mpi_pi(m_DMesons_phi->at(index) - m_lep->Phi()), 100, -M_PI, M_PI, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_dEtalep", suffixes, m_DMesons_eta->at(index) - m_lep->Eta(), 100, -5.0, 5.0, false, extra_flag, extra_weight);
        }
    }

    if (failed_cut == 0 || failed_cut == 1)
    {
        ELB->add_fill_histogram_sys("Dmeson_chi2", suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 6.5, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_chi2", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 2)
    {
        ELB->add_fill_histogram_sys("Dmeson_min_trackPt", suffixes, *(std::min_element(m_DMesons_daughterInfo__pt->at(index).begin(), m_DMesons_daughterInfo__pt->at(index).end())) * Charm::GeV, 100, 0, 10., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_min_trackPt", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 3)
    {
        ELB->add_fill_histogram_sys("Dmeson_max_trackdR", suffixes, D_meson_max_track_dR(index), 100, 0, 1.2, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_max_trackdR", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 14)
    {
        ELB->add_fill_histogram_sys("Dmeson_impact_sig", suffixes, m_DMesons_fitOutput__ImpactSignificance->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_impact_sig", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 4)
    {
        ELB->add_fill_histogram_sys("Dmeson_Lxy", suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_Lxy", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 5)
    {
        ELB->add_fill_histogram_sys("Dmeson_d0", suffixes, m_DMesons_fitOutput__Impact->at(index) * Charm::mum, 200, -1100, 1100, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_d0", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 6)
    {
        ELB->add_fill_histogram_sys("Dmeson_cosThetaStar", suffixes, m_DMesons_costhetastar->at(index), 100, -1., 1., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_cosThetaStar", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 8)
    {
        ELB->add_fill_histogram_sys("Dmeson_PhiMass", suffixes, m_DMesons_mPhi1->at(index), 100, PHI_MASS - 10., PHI_MASS + 10., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_PhiMass", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (m_lep && (failed_cut == 0 || failed_cut == 9))
    {
        ELB->add_fill_histogram_sys("Dmeson_dRlep", suffixes, D_meson_lepton_dR(index), 100, 0., 6., false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_dRlep", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }
    if (failed_cut == 0 || failed_cut == 10)
    {
        ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt", suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        if (m_do_Nminus1)
            ELB->add_fill_histogram_sys("Dmeson_m_ptcone40_pt", suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
    }

    if ((m_bin_in_D_pt || m_bin_in_lep_eta) && !diff_bins_override)
    {
        fill_D_s_histograms(ELB, channels, Ds_index, extra_flag, extra_weight, true);
    }

    return true;
}

bool DMesonRec::fill_D_s_histograms_scan(EventLoopBase *ELB, std::vector<std::string> channels, std::pair<unsigned int, int> Ds_index, std::vector<bool> extra_flag, std::vector<double> extra_weight)
{
    unsigned int index = Ds_index.first;
    std::vector<std::string> suffixes = channels;
    if (m_bin_in_D_pt)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_pt_bin(ELB, index);
        }
    }
    else if (m_bin_in_lep_eta)
    {
        for (unsigned int i = 0; i < channels.size(); i++)
        {
            suffixes.at(i) += D_meson_eta_bin(ELB, i);
        }
    }

    for (unsigned int i = 0; i < m_NcutVals; i++)
    {
        if (pass_ith_cut(i, index))
        {
            std::string cutStr = std::to_string(i);
            //Pt is always plotted, so an if is not needed for
            ELB->add_fill_histogram_sys("Dmeson_pt" + cutStr, suffixes, m_DMesons_pt->at(index) * Charm::GeV, 200, 0, 150., false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_eta" + cutStr, suffixes, m_DMesons_eta->at(index), 100, -3.0, 3.0, false, extra_flag, extra_weight);
            ELB->add_fill_histogram_sys("Dmeson_m" + cutStr, suffixes, m_DMesons_m->at(index) * Charm::GeV, 1000, 1.4, 2.4, false, extra_flag, extra_weight);
            if (m_do_cutScan == 4)
                ELB->add_fill_histogram_sys("Dmeson_Lxy" + cutStr, suffixes, m_DMesons_fitOutput__Lxy->at(index), 300, 0, 30., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 11)
                ELB->add_fill_histogram_sys("Dmeson_ctau" + cutStr, suffixes, (m_DMesons_fitOutput__Lxy->at(index) * Charm::mm * m_DMesons_m->at(index) / m_DMesons_pt->at(index)), 100, 0, 2.0, false, extra_flag, extra_weight);
            else if (m_do_cutScan == 2)
                ELB->add_fill_histogram_sys("Dmeson_min_trackPt" + cutStr, suffixes, D_meson_min_track_pt(index) * Charm::GeV, 100, 0, 6., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 1)
            {
                ELB->add_fill_histogram_sys("Dmeson_chi2" + cutStr, suffixes, m_DMesons_fitOutput__Chi2->at(index), 100, 0, 21., false, extra_flag, extra_weight);
                //ELB->add_fill_histogram_sys( "Dmeson_chi2_x_m"+cutStr, suffixes, m_DMesons_fitOutput__Chi2->at(index),  m_DMesons_m->at(index) * Charm::GeV, 100, 0, 21., 500, 1.4, 2.4, false, extra_flag, extra_weight);
            }
            else if (m_do_cutScan == 6)
                ELB->add_fill_histogram_sys("Dmeson_cosThetaStar" + cutStr, suffixes, m_DMesons_costhetastar->at(index), 100, -1., 1., false, extra_flag, extra_weight);
            else if (m_do_cutScan == 10)
                ELB->add_fill_histogram_sys("Dmeson_ptcone40_pt" + cutStr, suffixes, m_DMesons_ptcone40->at(index) / m_DMesons_pt->at(index), 100, 0, 10, false, extra_flag, extra_weight);
        }
    }
    return true;
}

void DMesonRec::connect_D_meson_branches(EventLoopBase *ELB)
{
    // Set name of output D branch to allow for systematics
    std::string D_branch_str;
    if (m_D_branch_sys == "")
    {
        D_branch_str = "";
    }
    else
    {
        D_branch_str = m_D_branch_sys;
    }

    ELB->add_preselection_branch("DMesons" + D_branch_str + "_costhetastar", &m_DMesons_costhetastar);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_DeltaMass", &m_DMesons_DeltaMass);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_fitOutput__Chi2", &m_DMesons_fitOutput__Chi2);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_fitOutput__Impact", &m_DMesons_fitOutput__Impact);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_fitOutput__ImpactZ0SinTheta", &m_DMesons_fitOutput__ImpactZ0SinTheta);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_fitOutput__ImpactSignificance", &m_DMesons_fitOutput__ImpactSignificance);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_fitOutput__Lxy", &m_DMesons_fitOutput__Lxy);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_mKpi1", &m_DMesons_mKpi1);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_mKpi2", &m_DMesons_mKpi2);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_mPhi1", &m_DMesons_mPhi1);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_mPhi2", &m_DMesons_mPhi2);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_ptcone40", &m_DMesons_ptcone40);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_SlowPionD0", &m_DMesons_SlowPionD0);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_SlowPionZ0SinTheta", &m_DMesons_SlowPionZ0SinTheta);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_eta", &m_DMesons_eta);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_m", &m_DMesons_m);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_phi", &m_DMesons_phi);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_pt", &m_DMesons_pt);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_D0Index", &m_DMesons_D0Index);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_decayType", &m_DMesons_decayType);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_fitOutput__Charge", &m_DMesons_fitOutput__Charge);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_pdgId", &m_DMesons_pdgId);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_truthBarcode", &m_DMesons_truthBarcode);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__passTight", &m_DMesons_daughterInfo__passTight);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__eta", &m_DMesons_daughterInfo__eta);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__phi", &m_DMesons_daughterInfo__phi);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__pt", &m_DMesons_daughterInfo__pt);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__z0SinTheta", &m_DMesons_daughterInfo__z0SinTheta);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__z0SinThetaPV", &m_DMesons_daughterInfo__z0SinThetaPV);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_fitOutput__VertexPosition", &m_DMesons_fitOutput__VertexPosition);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__pdgId", &m_DMesons_daughterInfo__pdgId);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__truthBarcode", &m_DMesons_daughterInfo__truthBarcode);
    ELB->add_preselection_branch("DMesons" + D_branch_str + "_daughterInfo__truthDBarcode", &m_DMesons_daughterInfo__truthDBarcode);
}

DMesonRec::DMesonRec()
{
}

}  // namespace Charm
