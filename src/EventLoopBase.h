#ifndef EVENT_LOOP_BASE_H
#define EVENT_LOOP_BASE_H

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <TString.h>
#include <TTree.h>
#include <yaml-cpp/yaml.h>

#include <iostream>
#include <set>
#include <unordered_map>

#include "DMesonRec.h"
#include "Definitions.h"
#include "HelperFunctions.h"
#include "TruthInfo.h"

namespace Charm
{
class EventLoopBase
{
    // Allow the DMesonRec class to use EventLoopBase functions
private:
    friend class DMesonRec;
    friend class TruthInfo;

public:
    EventLoopBase();

    EventLoopBase(TString input_file, TString out_path, TString tree_name);

    // execute event loop
    void run(long long start_event = 0, long long end_event = -1);

    // analysis config
    void set_sys_config(std::string name);
    YAML::Node get_sys_config() { return m_sys_config; };

    // config for systematics
    void set_config(std::string name);
    YAML::Node get_config() { return m_config; };

private:
    virtual void read_meta_data();

    virtual void initialize();

    virtual bool preselection();

    virtual int execute();

    virtual void finalize();

protected:
    virtual void connect_branches();

private:
    // config
    YAML::Node m_config{};
    YAML::Node m_sys_config{};
    std::string m_sys_config_name{};

    // systematics to run over
    std::vector<std::string> m_used_systematics{};

    // TTree name
    TString m_tree_name{};

    // out name
    TString m_output_file_name{};

    // histograms
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<TH1 *>>> m_histograms_sys{};
    std::unordered_map<std::string, std::unordered_map<std::string, TH1 *>> m_histograms{};

    // channels
    std::vector<std::string> m_channels{};

    // input root file
    TFile *m_input_file;

    // TTree
    TTree *m_tree;

    // List of branches
    std::vector<TBranch *> m_preselection_branches{};
    std::vector<TBranch *> m_branches{};
    std::vector<TBranch *> m_other_branches{};

    // cutflow map
    std::unordered_map<std::string, TH1D *> m_cutflow_histograms{};

    // output file path
    TString m_out_path{};

    // reweighting histograms
    std::unordered_map<std::string, TH1D *> m_rw_map{};

    // timing
    std::unordered_map<std::string, double> m_timing_map{};

protected:
    // channel
    std::string m_channel{};
    std::vector<std::string> m_channel_sys{};

    // variables
    bool m_is_mc{};
    bool m_no_weights{};
    int m_dataset_id{};
    int m_dataset_id_from_file_name{};
    std::string m_mc_gen{};
    std::string m_mc_shower{};
    std::string m_mc_period{};
    std::string m_data_period{};
    float m_cross_section{};
    float m_lumi{};

    long long m_current_event{};
    long long m_num_events{};

    float m_init_sum_of_w{};
    YAML::Node m_init_sum_of_w_sys{};
    std::vector<float> m_init_sum_of_w_sys_vec{};
    float m_experiment_weight{};

    bool m_enable_loop_timing{};

    bool m_save_nominal{};

    bool m_do_systematics{};

    bool m_fit_variables_only{};

    // histograms created
    bool m_histograms_created{};

    // event weight
    std::vector<float> m_event_weight_sys{};
    float m_event_weight{};

    // list of systematics
    std::vector<std::string> m_systematics{};
    std::map<TString, std::vector<std::string>> m_systematics_groups{};

    // weight associated with sys unc
    std::unordered_map<std::string, float> m_sys_weight{};

    // fake rate histograms
    std::unordered_map<std::string, std::unique_ptr<TH1>> m_fake_rate_map;
    std::unordered_map<std::string, std::unique_ptr<TH1>> m_real_rate_map;
    std::unordered_map<std::string, std::unique_ptr<TH1>> m_rw_hists_map;
    std::unordered_map<std::string, std::unique_ptr<TH1>> m_fake_rate_map_incl;
    std::unordered_map<std::string, std::unique_ptr<TH1>> m_real_rate_map_incl;

    // track efficiency sys
    std::unordered_map<std::string, std::unique_ptr<TObject>> m_track_eff_systematics;

    // mass resolution sys
    std::unordered_map<std::string, std::unique_ptr<TObject>> m_mass_width_systematics;

    // W+jets NLO MG reweighting
    std::unordered_map<std::string, std::unique_ptr<TObject>> m_nlo_mg_weights;

    // // SPG Dplus and Dstar histograms
    std::unordered_map<std::string, std::unique_ptr<TObject>> m_spg_dmeson_weights;

    // Dplus fiducial efficiency histograms
    std::unordered_map<std::string, std::unique_ptr<TObject>> m_dplus_fid_eff_weights;

    // Dplus branching ratio uncertainty histograms
    std::unordered_map<std::string, std::unique_ptr<TObject>> m_dplus_bkg_br_histos;

    // differential bins struct
    differential_bins m_differential_bins{};

    // differential eta bins struct
    differential_bins_eta m_differential_bins_eta{};

    // matrix method helper
    matrix_method_helper m_matrix_method_helper{};

    // track efficiency uncertainties
    track_sys_helper m_track_sys_helper{};

    // mass resolution uncertainties
    mass_smear_helper m_mass_smear_helper{};

    // wjets theory helper
    nlo_mg_sys_helper m_nlo_mg_sys_helper{};

    // spg forced decay weight helper
    spg_forced_decay_helper m_spg_forced_decay_helper{};

    // production fraction weights
    prod_frac_weight_helper m_prod_frac_weight_helper{};

    // modeling uncertainty
    modeling_uncertainty_helper m_modeling_uncertainty_helper{};

    // D+ background branching ratio uncertainty
    dplus_bkg_br_uncertainty m_dplus_bkg_br_uncertainty{};

private:
    void setup_histograms();

    void setup_cache(long long first, long long last);

    void loop(long long first, long long last);

    void save_histograms(TFile &file);

    void save_cutflow_histogram();

    void save_timing_histogram();

    void read_data_file(std::string path, std::unordered_map<std::string, std::unique_ptr<TH1>> *histogram_map);
    void read_data_file(std::string path, std::unordered_map<std::string, std::unique_ptr<TObject>> *histogram_map);

protected:
    void multiply_event_weights(float x);

    void multiply_event_weight(float x, unsigned int i);

    void multiply_nominal_weight(float x);

    void multiply_sys_weight(float x, unsigned int i);

    void multiply_sys_weight(float x, std::string key);

    void multiply_nominal_and_sys_weight(float nominal, sys_branches<Float_t> *sys, bool fix_zero = false);

    void multiply_mc_event_weight(float nominal, sys_branches<Float_t> *sys, bool apply_lumi = true, bool lumi_ratio = false, bool fix_sherpa_weights = false);

    bool event_pass_sys(unsigned int i) { return m_channel_sys.at(i) != ""; }

    // return the tree
    TTree *get_tree() { return m_tree; };

    // return branches
    std::vector<TBranch *> *get_branches() { return &m_branches; };

    // get ptv weight
    float get_ptv_weight(float pt, std::string jet_slice = "incl");

    // get channel name
    inline std::string get_channel() { return m_channel; };

    // check if channel exists for histograms
    void check_channel(std::string channel);

    // fill histogram in channel
    void fill_histogram_in_channel(std::string channel, std::string name, float val, int n, int xmin, int xmax, bool do_sys = false);

    void fill_histogram_in_channel(std::string channel, std::string name, float val, int n, const double *xbins, bool do_sys = false);

    void fill_histogram_in_channel(std::string channel, std::string name, float valx, float valy, int nx, float xmin, float xmax, int ny, float ymin, float ymax, bool do_sys = false);

    void add_fill_histogram_fast(std::string name, float val, int n, float xmin, float xmax, bool histo_exists, bool do_sys = false);

    void add_fill_histogram_fast(std::string name, std::vector<float> val, int n, float xmin, float xmax, bool histo_exists, bool do_sys = false);

    void add_fill_histogram_fast_in_channel(std::string channel, std::string name, float val, int n, float xmin, float xmax, bool histo_exists, bool do_sys = false);

    void add_fill_histogram_fast_in_channel(std::string channel, std::string name, float valx, float valy, int nx, float xmin, float xmax, int ny, float ymin, float ymax, bool histo_exists, bool do_sys = false);

    void add_fill_histogram_fast_in_channel(std::string channel, std::string name, float valx, float valy, int nx, float xmin, float xmax, int ny, const double *ybins, bool histo_exists, bool do_sys = false);

    void add_fill_histogram_sys_fast(std::string name, std::vector<float> val, int n, float xmin, float xmax, bool do_sys = false);

    void add_fill_histogram_sys_fast(std::string name, float val, int n, float xmin, float xmax, bool do_sys = false);

    // TODO: make a single template function for all add_fill_histogram_sys?
    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> val, int n, float xmin, float xmax, bool do_sys = false, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, float val, int n, float xmin, float xmax, bool do_sys = false, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, float val, int n, const double *xbins, bool do_sys = false, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> val, int n, const double *xbins, bool do_sys = false, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_fill_histogram_sys(std::string name, std::string suffix, std::vector<float> val, int n, float xmin, float xmax, bool do_sys = false);

    void add_fill_histogram_sys(std::string name, std::string suffix, float val, int n, float xmin, float xmax, bool do_sys = false);

    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> valx, std::vector<float> valy, int nx,
                                float xmin, float xmax, int ny, float ymin, float ymax, bool do_sys, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> valx, std::vector<float> valy, int nx,
                                float xmin, float xmax, int ny, const double *ybins, bool do_sys, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, float valx, float valy,
                                int nx, const double *xbins, int ny, const double *ybins, bool do_sys, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_fill_histogram_sys(std::string name, std::vector<std::string> suffix, std::vector<float> valx, float valy,
                                int nx, const double *xbins, int ny, const double *ybins, bool do_sys, std::vector<bool> extra_flag = {}, std::vector<double> extra_weight = {});

    void add_histogram(std::string name, bool do_sys, int n, float xmin, float xmax);

    void add_histogram(std::string name, bool do_sys, int n, const double *xbins);

    void add_histogram(std::string name, std::string channel, bool do_sys, int n, const double *xbins);

    void add_histogram(std::string name, std::string channel, bool do_sys, int n, float xmin, float xmax);

    void add_histogram(std::string name, std::string channel, bool do_sys, int nx, float xmin, float xmax, int ny, float ymin, float ymax);

    void add_histogram(std::string name, std::string channel, bool do_sys, int nx, float xmin, float xmax, int ny, const double *ybins);

    void add_histogram(std::string name, std::string channel, bool do_sys, int nx, const double *xbins, int ny, const double *ybins);

    // sys position
    unsigned int get_sys_position(std::string key);

    // get systematics from group
    std::vector<std::string> get_systematics_from_group(std::string group);

    // cutflow counter
    void increment_cutflow(std::string channel, float index);

    // add channel with cutflow
    void add_channel(std::string channel, std::vector<std::string> cuts);

    // set a channel
    void set_channel(std::string channel) { m_channel = channel; }

    // Add preselection branch
    template <typename T>
    void add_preselection_branch(TString name, T var, bool add_to_cache = true)
    {
        unsigned int found{};
        m_tree->SetBranchStatus(name, true, &found);
        if (found == 0)
        {
            throw std::runtime_error(name);
        }

        m_tree->SetBranchAddress(name, var);

        TBranch *branch = m_tree->GetBranch(name);
        if (add_to_cache)
            m_tree->AddBranchToCache(branch, true);

        m_preselection_branches.push_back(branch);
    }

    // Add branch
    template <typename T>
    void add_branch(TString name, T var, bool add_to_cache = true)
    {
        unsigned int found{};
        m_tree->SetBranchStatus(name, true, &found);
        if (found == 0)
        {
            throw std::runtime_error(name);
        }

        m_tree->SetBranchAddress(name, var);

        TBranch *branch = m_tree->GetBranch(name);
        if (add_to_cache)
            m_tree->AddBranchToCache(branch, true);

        m_branches.push_back(branch);
    }

    // Add costum branch
    template <typename T>
    TBranch *add_custom_branch(TString name, T var, bool add_to_cache = true)
    {
        unsigned int found{};
        m_tree->SetBranchStatus(name, true, &found);
        if (found == 0)
        {
            throw std::runtime_error(name);
        }

        m_tree->SetBranchAddress(name, var);

        TBranch *branch = m_tree->GetBranch(name);
        if (add_to_cache)
            m_tree->AddBranchToCache(branch, true);

        m_other_branches.push_back(branch);
        return branch;
    }

    // Add sys branches
    template <typename T>
    void initialize_sys_branches(sys_branches<T> *sys, std::string name, std::vector<std::string> skip = {})
    {
        for (unsigned int i = 0; i < sys->keys.size(); i++)
        {
            if (skip.size() && std::find(skip.begin(), skip.end(), sys->keys.at(i)) != skip.end())
            {
                sys->branches.push_back(nullptr);
            }
            else
            {
                sys->branches.push_back(add_custom_branch(TString::Format(name.c_str(), sys->keys.at(i).c_str()), &sys->values.at(i)));
            }
        }
    }

    template <typename T>
    std::vector<T> get_val_for_systematics(T nominal, sys_branches<T> *sys, const double multiply = 1.0)
    {
        auto out = std::vector<T>(m_systematics.size() + 1, nominal * multiply);
        std::vector<unsigned int> sys_position;
        for (unsigned int j = 0; j < sys->keys.size(); j++)
        {
            if (sys->branches.at(j))
            {
                sys->branches.at(j)->GetEntry(m_current_event);
            }
            sys_position.push_back(get_sys_position(sys->keys.at(j)));
        }
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            unsigned int j = std::find(sys_position.begin(), sys_position.end(), i) - sys_position.begin();
            if (j < sys_position.size())
            {
                if (sys->branches.at(j))
                {
                    out.at(i + 1) = sys->values.at(j) * multiply;
                }
            }
        }
        return out;
    }

    template <typename T>
    std::vector<std::vector<T> *> get_val_for_systematics(std::vector<T> *nominal, sys_branches<std::vector<T> *> *sys)
    {
        auto out = std::vector<std::vector<T> *>(m_systematics.size() + 1, nominal);
        std::vector<unsigned int> sys_position;
        for (unsigned int j = 0; j < sys->keys.size(); j++)
        {
            if (sys->branches.at(j))
            {
                sys->branches.at(j)->GetEntry(m_current_event);
            }
            sys_position.push_back(get_sys_position(sys->keys.at(j)));
        }
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            unsigned int j = std::find(sys_position.begin(), sys_position.end(), i) - sys_position.begin();
            if (j < sys_position.size())
            {
                if (sys->branches.at(j))
                {
                    out.at(i + 1) = sys->values.at(j);
                }
            }
        }
        return out;
    }

    template <typename T>
    std::vector<std::vector<T> *> get_val_for_systematics(std::vector<T> *nominal, sys_branches<std::vector<T> *> *sys_1, sys_branches<std::vector<T> *> *sys_2)
    {
        auto out = std::vector<std::vector<T> *>(m_systematics.size() + 1, nominal);
        std::vector<unsigned int> sys_position_1;
        std::vector<unsigned int> sys_position_2;
        for (unsigned int j = 0; j < sys_1->keys.size(); j++)
        {
            if (sys_1->branches.at(j))
            {
                sys_1->branches.at(j)->GetEntry(m_current_event);
            }
            sys_position_1.push_back(get_sys_position(sys_1->keys.at(j)));
        }
        for (unsigned int j = 0; j < sys_2->keys.size(); j++)
        {
            if (sys_2->branches.at(j))
            {
                sys_2->branches.at(j)->GetEntry(m_current_event);
            }
            sys_position_2.push_back(get_sys_position(sys_2->keys.at(j)));
        }
        for (unsigned int i = 0; i < m_systematics.size(); i++)
        {
            unsigned int j_1 = std::find(sys_position_1.begin(), sys_position_1.end(), i) - sys_position_1.begin();
            unsigned int j_2 = std::find(sys_position_2.begin(), sys_position_2.end(), i) - sys_position_2.begin();
            if (j_1 < sys_position_1.size() && j_2 < sys_position_2.size())
            {
                throw std::runtime_error("Affected by same systematics!");
            }
            else if (j_1 < sys_position_1.size())
            {
                if (sys_1->branches.at(j_1))
                {
                    out.at(i + 1) = sys_1->values.at(j_1);
                }
            }
            else if (j_2 < sys_position_2.size())
            {
                if (sys_2->branches.at(j_2))
                {
                    out.at(i + 1) = sys_2->values.at(j_2);
                }
            }
        }
        return out;
    }
};

}  // namespace Charm

#endif  // EVENT_LOOP_BASE_H
